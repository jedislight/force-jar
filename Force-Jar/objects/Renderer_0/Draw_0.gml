/// @description Insert description here
// You can write your code in this editor

show_debug_message("Rendering");

var renderables = ds_map_create();

for (var curr_layer = DrawLayer.DEFAULT; curr_layer != DrawLayer.END_LAYER; ++curr_layer)
{
	ds_map_add(renderables, curr_layer, ds_list_create());
}

with(all)
{
	if (object_get_visible(object_index))
	{
		continue;	
	}
	
	if (object_get_parent(object_index) != Renderable)
	{
		continue;
	}
	
	ds_list_add(ds_map_find_value(renderables, draw_layer), id);
}

// Now that objects are sorted into layers, we can draw each layer specifically.

// Draw Default Layer
for (var layer_val = DrawLayer.DEFAULT; layer_val != DrawLayer.END_LAYER; ++layer_val)
{
		var layer_id = ds_map_find_value(renderables, layer_val);
		for (var curr_inst = 0; curr_inst < ds_list_size(layer_id); ++curr_inst)
		{
			with (ds_list_find_value(layer_id, curr_inst))
			{
				event_perform(ev_draw, 0);	
			}
		}
}

// We never draw the End Layer (may as well be called invisible), this is by design.