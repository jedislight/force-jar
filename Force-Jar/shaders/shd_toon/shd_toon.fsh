//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
	vec4 sampled = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 out_color = sampled;
	out_color.r = min(floor(sampled.r * 5.0)/5.0, 1.0);
	out_color.g = min(floor(sampled.g * 5.0)/5.0, 1.0);
	out_color.b = min(floor(sampled.b * 5.0)/5.0, 1.0);
    gl_FragColor = out_color;
}
