{
    "id": "b1b7665a-065d-455a-b5de-ede8a6f8d6ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "LevBlock",
    "eventList": [
        {
            "id": "56790a0e-35fc-4d8a-8b2f-33e4db8c31dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1b7665a-065d-455a-b5de-ede8a6f8d6ff"
        },
        {
            "id": "be4c7bb3-69fc-40cf-8009-a73d1bc750df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1b7665a-065d-455a-b5de-ede8a6f8d6ff"
        },
        {
            "id": "7d8ed8ac-fd41-42bc-b5ea-64376d00ca9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1b7665a-065d-455a-b5de-ede8a6f8d6ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a1e55b03-e373-4293-be98-a1325d58bb2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "934cd068-3ff2-428f-b00a-eda1119b3d78",
    "visible": true
}