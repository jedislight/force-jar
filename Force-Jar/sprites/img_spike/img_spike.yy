{
    "id": "db365932-988b-48b6-bf5b-c7796047f569",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_spike",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa2fc4a9-74cb-412a-a6af-08d1863cd3f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db365932-988b-48b6-bf5b-c7796047f569",
            "compositeImage": {
                "id": "7b3cc966-d0f6-4bd1-9daa-f3bc51244a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa2fc4a9-74cb-412a-a6af-08d1863cd3f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82245d00-1bc3-41ec-87ed-bf992199f9dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2fc4a9-74cb-412a-a6af-08d1863cd3f0",
                    "LayerId": "4ceac8af-2845-4018-8084-69e457ad4f47"
                },
                {
                    "id": "2b2c4eac-2772-4e06-a74c-8f30dfd10652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2fc4a9-74cb-412a-a6af-08d1863cd3f0",
                    "LayerId": "1ee205cf-07e1-4c7b-9e04-f14f00905f71"
                },
                {
                    "id": "b52358e7-2506-424a-bcfd-1091a82bcd92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2fc4a9-74cb-412a-a6af-08d1863cd3f0",
                    "LayerId": "eb1e00d9-7b6e-4507-b7b3-afa2831b8715"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4ceac8af-2845-4018-8084-69e457ad4f47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db365932-988b-48b6-bf5b-c7796047f569",
            "blendMode": 0,
            "isLocked": false,
            "name": "Shadow",
            "opacity": 50,
            "visible": true
        },
        {
            "id": "eb1e00d9-7b6e-4507-b7b3-afa2831b8715",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db365932-988b-48b6-bf5b-c7796047f569",
            "blendMode": 3,
            "isLocked": false,
            "name": "Color",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1ee205cf-07e1-4c7b-9e04-f14f00905f71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db365932-988b-48b6-bf5b-c7796047f569",
            "blendMode": 0,
            "isLocked": false,
            "name": "Shape",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}