/// @description Insert description here
// You can write your code in this editor

var dy = 32 // gravity
if(levitating)
{
	dy = -lev_speed;
}

var standers = ds_list_create();
with(PlayerStates)
{
	if(place_meeting(x, y+sign(-dy),other.id))
	{
		ds_list_add(standers, id);
	}
	else
	{
		solid = true;
	}
}
with(Enemy)
{
	if(place_meeting(x, y+sign(-dy),other.id))
	{
		ds_list_add(standers, id);
	}
	else
	{
		solid = true;
	}
}

xstart = x;
ystart = y;
apply_movement(0, dy);

with(Enemy)
{
	solid = false;
}
with(PlayerStates)
{
	solid = false;
}

for(var i = 0; i< ds_list_size(standers); ++i)
{
	var stander = standers[|i];
	stander.x += x - xstart;
	stander.y += y - ystart;
}

ds_list_destroy(standers);