/// @func drops_create(drops_key, drops_amount)
/// @desc Adds the drops facet to the calling object
/// @param {DropTableKeys} drops_key the key to use for the drop table.
/// @param {Real} drops_amount the number of drops to generate

facet_drops_key = argument0;
facet_drops_amount = argument1;
has_facet_drops = true;