/// @description Insert description here
// You can write your code in this editor

++frame;
var player = instance_find(PlayerStates, 0);
var hive = instance_nearest(x,y, Hive);
instance_deactivate_object(id);
nearest_other = instance_nearest(x,y, Swarmer);
instance_activate_object(id);
target = hive
if(instance_exists(player))
{
	if(distance_to_object(player) < 480 and collision_line(x,y, player.x, player.y, Solid, false, false) <= 0)
	{
		target = player;
	}
}
if(target != player and instance_exists(nearest_other))
{
	if(nearest_other.target != hive && nearest_other.target != id)
	{
		target = nearest_other
	}
}

if(not instance_exists(target))
{
	target = id;
}