var tilemap_id = layer_tilemap_get_id(layer_get_id("Tiles"));

var tileset_id = tilemap_get_tileset(tilemap_id);

if (tileset_id != noone && tileset_id != -1)
{
	show_debug_message("Terrain for collision is being set to invisible");
	layer_set_visible(layer_get_id("Undrawn_Terrain"), false);
}
else
{
	layer_set_visible(layer_get_id("Undrawn_Terrain"), true);	
}