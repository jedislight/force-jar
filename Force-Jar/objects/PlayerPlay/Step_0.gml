/// @description Insert description here
// You can write your code in this editor

// locked checks
for(var l = 0; l < ds_list_size(locked_by); ++l)
{
	var locker = locked_by[|l];
	if(!instance_exists(locker))
	{
		ds_list_delete(locked_by, l);
		if(ds_list_empty(locked_by))
		{
			locked = false;
			facing_locked = false;
		}
		break;
	}
}

// powers stat adjust
jump_height = 8;
if(powerStates[? Powers.HI_JUMP] == PowerStates.ON)
{
	jump_height = 12;
}

//weapon selection
if(action_weapon_select_pressed or not instance_exists(active_weapon))
{
	var available_weapons = ds_list_create();
	
	if(powerStates[? Powers.RAPIER] == PowerStates.ON)
	{
		ds_list_add(available_weapons, RapierWeaponData);
	}
	if(powerStates[? Powers.STAFF] == PowerStates.ON)
	{
		ds_list_add(available_weapons, StaffWeaponData);
	}
	
	if(ds_list_size(available_weapons) > 0)
	{
		var current_weapon_index = 0;
		if(instance_exists(active_weapon))
		{
			current_weapon_index = ds_list_find_index(available_weapons, active_weapon.object_index);
		}
	
		var next_weapon_index = (current_weapon_index + 1) mod ds_list_size(available_weapons);
	
		if(instance_exists(active_weapon))
		{
			instance_destroy(active_weapon);
		}
	
		active_weapon = instance_create_depth(0,0,0, available_weapons[| next_weapon_index]);
		active_weapon.owner = id;
		active_weapon.xoffset = 32;
		active_weapon.yoffset = 96;
		active_weapon.persistent = true;
		
		var fx = instance_create_depth(x,y-64, depth, FXFlyout);
		fx.sprite_index = active_weapon.sprite_index;
		fx.anchor = id;
		fx.acceleration_y = 1/25*-fx.vel_y;
	}
	ds_list_destroy(available_weapons);
}
//weapon selection
if(action_offhand_weapon_select_pressed  or not instance_exists(active_offhand_weapon))
{
	var available_weapons = ds_list_create();
	
	if(powerStates[? Powers.LEVITATE_MASS] == PowerStates.ON)
	{
		ds_list_add(available_weapons, LevitateWeaponData);
	}
	
	if(ds_list_size(available_weapons) > 0)
	{
		var current_weapon_index = 0;
		if(instance_exists(active_offhand_weapon))
		{
			current_weapon_index = ds_list_find_index(available_weapons, active_offhand_weapon);
		}
	
		var next_weapon_index = (current_weapon_index + 1) mod ds_list_size(available_weapons);
	
		if(instance_exists(active_offhand_weapon))
		{
			instance_destroy(active_offhand_weapon);
		}
	
		active_offhand_weapon = instance_create_depth(0,0,0, available_weapons[| next_weapon_index]);
		active_offhand_weapon.owner = id;
		active_offhand_weapon.xoffset = 32;
		active_offhand_weapon.yoffset = 96;
		active_offhand_weapon.persistent = true;
		
		var fx = instance_create_depth(x,y-64, depth, FXFlyout);
		fx.sprite_index = active_offhand_weapon.sprite_index;
		fx.anchor = id;
		fx.acceleration_y = 1/25*-fx.vel_y;
	}
	ds_list_destroy(available_weapons);
}

// motion
if(!(facing_locked or locked))
{
	if(horizontal_motion_input > 0)
	{
		if(vertical_motion_input > 0)
		{
			facing = 315;
		}
		else if(vertical_motion_input < 0)
		{
			facing = 45;
		}
		else
		{
			facing = 0;
		}
	}
	else if(horizontal_motion_input < 0)
	{
		if(vertical_motion_input > 0)
		{
			facing = 225;
		}
		else if(vertical_motion_input < 0)
		{
			facing = 135;
		}
		else
		{
			facing = 180;
		}
	}
	else
	{
		if(facing > 90 and facing < 270)
		{
			if(vertical_motion_input > 0)
			{
				facing = 225;
			}
			else if(vertical_motion_input < 0)
			{
				facing = 135
			}
			else
			{
				facing = 180;
			}
		}
		else
		{
			if(vertical_motion_input > 0)
			{
				facing = 315;
			}
			else if(vertical_motion_input < 0)
			{
				facing = 45;
			}
			else
			{
				facing = 0;
			}
		}
	}
}

if(not locked)
{
	dx = horizontal_motion_input * move_speed;
	dy = 32 // gravity

	coyote_time = clamp(coyote_time - (place_free(x,y+1)*2-1), 0, coyote_time_max);
	var can_jump = coyote_time > 0;
	if(not jumping and action_jump_pressed and can_jump)
	{
		jumping = true;
		jump_start = y;
	}

	//fluid jump drag
	var fluid = instance_place(x, y, Fluid)
	if(instance_exists(fluid))
	{
		jump_start += (1.0-fluid.motion_modifier) * 64;
	}

	if(not action_jump)
	{
		if(jumping)
		{
			jump_falloff_current = -32;
		}
	
		jumping = false;
	}

	if(jumping and y >= jump_start - (jump_height*64 - 48))
	{
		dy -= 64
	}
	else
	{
		if(jumping)
		{
			jump_falloff_current = -64;
		}
		jumping = false;
	}

	dy += jump_falloff_current
	jump_falloff_current = min(jump_falloff_current + jump_falloff_deceleration, 0);

	if(instance_exists(grapler) and grapler.locked)
	{
		var dir_to_grapler = point_direction(x+32, y+96, grapler.x, grapler.y);
		dx = lengthdir_x(grapler.velocity, dir_to_grapler);
		dy = lengthdir_y(grapler.velocity, dir_to_grapler);
	}

	//cancel jump on head hit last so you can slide into openings player height
	if(jumping and not place_free(x,y-1))
	{
		jumping = false;
		jump_falloff_current = max(jump_falloff_current, -32);
	}

	var colliding = apply_movement(dx,dy);

	if(!place_free(x,y))
	{
		//TODO handle smashed player better when health
		instance_destroy();
		exit;
	}

	// grapling
	if(colliding and instance_exists(grapler) and grapler.locked)
	{
		instance_destroy(grapler);
		grapler = noone;
	}
	
	if(not instance_exists(grapler))
	{
		grapler = noone;
		facing_locked = false;
	}
	
	if(grapler == noone and action_grapple_pressed and powerStates[? Powers.GRAPPLE] == PowerStates.ON)
	{
		grapler = instance_create_layer(x+32, y+96, "Instances", Grappler);
		grapler.direction = facing;
		grapler.velocity = grapler_velocity;
		grapler.max_extent = grapler_max_extent;
		grapler.owner = id;
		facing_locked = true;
	}
	
	
	if (active_weapon != noone and (action_attack or action_attack_pressed or action_attack_released))
	{
		script_execute(active_weapon.on_attack,
			active_weapon,
			action_attack,
			action_attack_pressed,
			action_attack_released,
			facing);
	}
	
	if (active_offhand_weapon != noone and (action_offhand_attack or action_offhand_attack_pressed or action_offhand_attack_released))
	{
		script_execute(active_offhand_weapon.on_attack,
			active_offhand_weapon,
			action_offhand_attack,
			action_offhand_attack_pressed,
			action_offhand_attack_released,
			facing);
	}
}

if(pause_input_pressed)
{
	map_toggle();
}

event_inherited();