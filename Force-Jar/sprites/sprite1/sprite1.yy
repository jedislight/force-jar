{
    "id": "557d2127-79dc-4adf-b7cc-42c6001362c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "231df5d6-2553-4e9c-9606-463d6db21873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
            "compositeImage": {
                "id": "e0bc567a-e56e-490c-af06-b6e22baf43b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "231df5d6-2553-4e9c-9606-463d6db21873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b2d09f-628c-4bcf-98ba-dcb3b5b7618a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "231df5d6-2553-4e9c-9606-463d6db21873",
                    "LayerId": "f631d0e5-eeae-4832-b84e-08f6500c515a"
                },
                {
                    "id": "ffa351d7-a35d-4458-94a1-c72f1e8039bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "231df5d6-2553-4e9c-9606-463d6db21873",
                    "LayerId": "359c199f-5556-4585-b359-f4f0d8055858"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "f631d0e5-eeae-4832-b84e-08f6500c515a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
            "blendMode": 2,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 45,
            "visible": true
        },
        {
            "id": "359c199f-5556-4585-b359-f4f0d8055858",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}