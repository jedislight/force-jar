
{
    "name": "title_menu",
    "id": "9c6c6100-4094-4ef2-a9af-e670bfddee2d",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "52b6f4dc-5f42-4af3-bb5e-ff6152b0a8bd",
        "5439eb54-7b7c-4de2-88c3-b60a65f4accd",
        "8927177a-b093-4980-a60a-dd492941aa48",
        "59d6ba8c-892c-44d5-a1f7-36dcfb2d96b8",
        "d906b775-f46a-4e8f-88c3-2dbececce792"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "TITLE",
            "id": "e25a7477-8797-433e-9ff7-35493f2485c2",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_63CD8A76","id": "a5bb5cf4-be4a-4327-9a6d-6ef45b26ef0c","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 4.666667,"scaleY": 4.666667,"mvc": "1.0","spriteId": "f459084a-2ede-442c-8c65-4ca4227214db","userdefined_animFPS": false,"x": 1024,"y": 256}
            ],
            "depth": 0,
            "grid_x": 64,
            "grid_y": 64,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "f5d15bfa-14d1-40ac-9a40-d18a4878b21c",
            "depth": 200,
            "grid_x": 64,
            "grid_y": 64,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "START_GAME_ITEM","id": "52b6f4dc-5f42-4af3-bb5e-ff6152b0a8bd","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_START_GAME_ITEM.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "START_GAME_ITEM","objId": "16a628eb-d621-48fc-b47e-7de758f634d2","properties": [{"id": "26a5e386-6886-4b34-8a18-ce6062c5efd0","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "START GAME"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1664,"y": 1408},
{"name": "LOAD_GAME_ITEM","id": "5439eb54-7b7c-4de2-88c3-b60a65f4accd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "LOAD_GAME_ITEM","objId": "ae82b777-fbe4-48cf-9766-8118f3f75409","properties": [{"id": "e07f6e49-8c01-43d8-9e15-5bc3ba58cf76","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "LOAD GAME"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1664,"y": 1536},
{"name": "SETTINGS_ITEM","id": "8927177a-b093-4980-a60a-dd492941aa48","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "SETTINGS_ITEM","objId": "ae82b777-fbe4-48cf-9766-8118f3f75409","properties": [{"id": "3723315f-5309-46f2-bd43-d718db745e19","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "SETTINGS"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1664,"y": 1664},
{"name": "EXIT_ITEM","id": "59d6ba8c-892c-44d5-a1f7-36dcfb2d96b8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "EXIT_ITEM","objId": "529bd6ae-171c-4a1f-866b-edc13f110737","properties": [{"id": "430b9013-00a3-4ba7-99f1-2fb6736af5ca","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "EXIT TO DESKTOP"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1664,"y": 1792},
{"name": "inst_65FCA6B","id": "d906b775-f46a-4e8f-88c3-2dbececce792","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_65FCA6B","objId": "5d9435ea-1393-4015-829f-b7f954bd286b","properties": [{"id": "9e360c12-c390-4bed-b695-e64af7261bea","modelName": "GMOverriddenProperty","objectId": "5d9435ea-1393-4015-829f-b7f954bd286b","propertyId": "b1dd1ae3-2665-4521-ae39-31791463f51d","mvc": "1.0","value": "mus_b"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": true,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "2aa57290-d9b0-4e02-85ff-4e3a0d28f1a1",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 300,
            "grid_x": 64,
            "grid_y": 64,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "58b35d80-b54b-4736-84d5-786afc6bf734",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "ea25a670-72d1-4568-bc3e-a5a267f6a282",
        "Height": 2160,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 3840
    },
    "mvc": "1.0",
    "views": [
{"id": "a4805906-e5c7-473d-8e19-840be524ec25","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "8789e9ed-2a88-4c07-ba51-d88f1d345b85","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "91e92fa3-d9a4-4409-b636-212de1825467","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "72fd7c5f-1617-4441-8940-fddf85152ca8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d199a4e6-4c39-4f0f-9416-36da43acfbb7","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0f6a9162-9f2a-49b1-ac32-0378f3d659cc","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ae25a754-32ba-4404-bfc9-b8763156e2f5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "926cef80-96f0-422f-bb3b-56bd5a2247e9","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "763d1cc2-bdc1-4edf-b47c-1165b9398be7",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}