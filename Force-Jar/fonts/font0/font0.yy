{
    "id": "29622566-d88b-468f-a3e4-0a6e357a29cd",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cfc4d3aa-e29c-4d2c-9cdc-7e34d6d14b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 196,
                "offset": 0,
                "shift": 48,
                "w": 48,
                "x": 441,
                "y": 794
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ea7e2a5b-7337-4f92-8c31-697bc65a9df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 196,
                "offset": 10,
                "shift": 57,
                "w": 51,
                "x": 337,
                "y": 794
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7fa0623d-f432-4a19-80f2-5dd1b33cfd4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 196,
                "offset": 25,
                "shift": 81,
                "w": 62,
                "x": 1968,
                "y": 596
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e937d661-88f4-4184-9f3a-83a58b419cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 196,
                "offset": 8,
                "shift": 95,
                "w": 92,
                "x": 1173,
                "y": 398
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "220b83f5-6967-4d97-906b-2835f59885a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 196,
                "offset": 7,
                "shift": 95,
                "w": 92,
                "x": 1079,
                "y": 398
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "72645736-f744-43ee-8cac-6e33790cdbfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 196,
                "offset": 15,
                "shift": 152,
                "w": 133,
                "x": 766,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bad4ff2f-372e-420d-a8e5-52c96b1a8818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 196,
                "offset": 14,
                "shift": 123,
                "w": 107,
                "x": 1282,
                "y": 200
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c2bdd30a-a593-49c1-bfa7-04da165eee28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 196,
                "offset": 25,
                "shift": 41,
                "w": 32,
                "x": 650,
                "y": 794
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "47ad1729-67e4-41be-bf94-bc160d6ad52d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 196,
                "offset": 11,
                "shift": 57,
                "w": 64,
                "x": 1902,
                "y": 596
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d633e273-6b7c-421f-968e-2eb3d3726e47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 196,
                "offset": -14,
                "shift": 57,
                "w": 64,
                "x": 1836,
                "y": 596
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e76b010d-f614-494b-93c0-d0408d2a4da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 196,
                "offset": 16,
                "shift": 67,
                "w": 62,
                "x": 2,
                "y": 794
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "37d65849-27d1-4918-839e-6ddaf11aea3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 196,
                "offset": 13,
                "shift": 100,
                "w": 87,
                "x": 91,
                "y": 596
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "de8cb36c-79b6-4274-9f84-a2c205334eaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 196,
                "offset": 1,
                "shift": 48,
                "w": 36,
                "x": 575,
                "y": 794
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "df9fce12-2a9d-4f7a-9d5d-01d8349ce5e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 196,
                "offset": 6,
                "shift": 57,
                "w": 52,
                "x": 177,
                "y": 794
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "63f833b7-1fe9-4f53-9f69-ffb1baab795e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 196,
                "offset": 7,
                "shift": 48,
                "w": 30,
                "x": 684,
                "y": 794
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "02e827ad-7c5e-4b0f-99de-1b8b942ff29d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 196,
                "offset": -8,
                "shift": 48,
                "w": 78,
                "x": 1222,
                "y": 596
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c9ccf04e-a024-487a-ad55-778015ffe3fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 196,
                "offset": 11,
                "shift": 95,
                "w": 87,
                "x": 1818,
                "y": 398
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cf21c604-1062-4c5e-9216-fb06c864fd40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 196,
                "offset": 20,
                "shift": 95,
                "w": 68,
                "x": 1766,
                "y": 596
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "86e3a0b3-213a-4c38-ae91-49914234d6bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 196,
                "offset": 10,
                "shift": 95,
                "w": 88,
                "x": 1638,
                "y": 398
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4eddeab9-8f48-4455-90e5-82767e09859c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 196,
                "offset": 8,
                "shift": 95,
                "w": 88,
                "x": 1728,
                "y": 398
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a3d360fd-ea29-4ff4-9a8e-d881c2a9b269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 196,
                "offset": 4,
                "shift": 95,
                "w": 92,
                "x": 1267,
                "y": 398
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a196d58b-8e73-4bdf-b19c-a01e87c58a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 196,
                "offset": 10,
                "shift": 95,
                "w": 89,
                "x": 1547,
                "y": 398
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c3ef8697-eac7-4705-9a3c-e5bf5fb30937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 196,
                "offset": 13,
                "shift": 95,
                "w": 86,
                "x": 622,
                "y": 596
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c09429d2-8eaa-429b-bc2c-74e68f8f1606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 196,
                "offset": 17,
                "shift": 95,
                "w": 87,
                "x": 2,
                "y": 596
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8278fd1c-15ad-42c4-b335-48563b4bb07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 196,
                "offset": 11,
                "shift": 95,
                "w": 86,
                "x": 446,
                "y": 596
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6b70a285-266c-4b1c-aa76-7b5794d0e6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 196,
                "offset": 10,
                "shift": 95,
                "w": 86,
                "x": 358,
                "y": 596
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e8997ff5-953a-4847-bfd1-a2b0648ff895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 196,
                "offset": 12,
                "shift": 57,
                "w": 43,
                "x": 491,
                "y": 794
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a93c37c7-a62a-4657-a6ea-1d65cb30aeed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 196,
                "offset": 6,
                "shift": 57,
                "w": 49,
                "x": 390,
                "y": 794
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "cd1d3958-088d-4043-a05d-6aa67a6522e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 196,
                "offset": 14,
                "shift": 100,
                "w": 85,
                "x": 797,
                "y": 596
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c217c97a-4ca9-4529-8a3a-3ae8e437ce53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 196,
                "offset": 13,
                "shift": 100,
                "w": 87,
                "x": 269,
                "y": 596
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "49a5f659-8fbf-4ca7-a024-bd4337d07d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 196,
                "offset": 14,
                "shift": 100,
                "w": 85,
                "x": 971,
                "y": 596
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9b1a129f-e47a-4de8-9781-980adc7a36bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 196,
                "offset": 21,
                "shift": 104,
                "w": 85,
                "x": 710,
                "y": 596
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3fbb3725-8e85-48b9-80e1-fbc627b0fdba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 196,
                "offset": 11,
                "shift": 167,
                "w": 162,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e0561e6a-aa98-401e-bb68-11beed24b9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 196,
                "offset": -2,
                "shift": 123,
                "w": 118,
                "x": 1911,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3ae8b22e-fc1e-4685-98bb-d4790052c624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 196,
                "offset": 6,
                "shift": 123,
                "w": 116,
                "x": 595,
                "y": 200
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0db7bee2-7215-4b60-93d5-f274ff238c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 196,
                "offset": 16,
                "shift": 123,
                "w": 112,
                "x": 944,
                "y": 200
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b6a06eec-2197-4006-b64c-ff8dedf3cf0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 196,
                "offset": 7,
                "shift": 123,
                "w": 117,
                "x": 2,
                "y": 200
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c0bfcab6-bd92-4d13-b99e-f5739f186a9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 196,
                "offset": 7,
                "shift": 114,
                "w": 117,
                "x": 121,
                "y": 200
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4462f635-5a8c-4937-9c58-5000a1d83b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 196,
                "offset": 6,
                "shift": 104,
                "w": 112,
                "x": 830,
                "y": 200
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c026567d-e2d7-401e-a220-f2c796642d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 196,
                "offset": 15,
                "shift": 133,
                "w": 120,
                "x": 1789,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1fffa7e1-974b-4304-b056-9d911b1a3a1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 196,
                "offset": 7,
                "shift": 123,
                "w": 124,
                "x": 1295,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "de56e412-0683-4ed2-b383-bb00ab5a173d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 196,
                "offset": 5,
                "shift": 48,
                "w": 52,
                "x": 123,
                "y": 794
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "50662845-a49a-4f0a-9ca4-9040ad2aa495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 196,
                "offset": 4,
                "shift": 95,
                "w": 99,
                "x": 104,
                "y": 398
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9bd5d9c1-b4d8-4aff-92cf-7ee132f741ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 196,
                "offset": 6,
                "shift": 123,
                "w": 132,
                "x": 901,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8995c4b9-ee4d-4b80-a787-b2dfde92f281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 196,
                "offset": 7,
                "shift": 104,
                "w": 93,
                "x": 984,
                "y": 398
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "43f43a57-118f-428b-bf0b-77b133f47758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 196,
                "offset": 6,
                "shift": 142,
                "w": 145,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c9718f2f-324d-4574-afd3-0cfc39d68d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 196,
                "offset": 7,
                "shift": 123,
                "w": 124,
                "x": 1169,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a9e3af13-f647-4355-a6dd-03191aa7e638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 196,
                "offset": 14,
                "shift": 133,
                "w": 121,
                "x": 1421,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "48dea7d7-c71d-426c-8dac-dfdbd9589b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 196,
                "offset": 6,
                "shift": 114,
                "w": 115,
                "x": 713,
                "y": 200
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8f4073f6-7d25-4049-98a2-fdc191a49d84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 196,
                "offset": 14,
                "shift": 133,
                "w": 121,
                "x": 1544,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5ad23629-bdb8-4be7-ae0b-547adc414d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 196,
                "offset": 7,
                "shift": 123,
                "w": 120,
                "x": 1667,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1f51f847-956f-4789-8a47-ff17621db950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 196,
                "offset": 10,
                "shift": 114,
                "w": 106,
                "x": 1499,
                "y": 200
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "08dc6771-1063-4afc-b45c-04fec342a271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 196,
                "offset": 20,
                "shift": 104,
                "w": 102,
                "x": 1925,
                "y": 200
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3a9a54c1-7987-4c0f-9f08-092345b21f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 196,
                "offset": 15,
                "shift": 123,
                "w": 116,
                "x": 359,
                "y": 200
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e197efba-3b6b-4cd0-bc39-16d98a9dc3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 196,
                "offset": 19,
                "shift": 114,
                "w": 117,
                "x": 240,
                "y": 200
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "28dd0bd2-8fe7-420f-a42f-94c49784e384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 196,
                "offset": 20,
                "shift": 161,
                "w": 163,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8036dcdb-eea5-4ad9-a53a-6af6c9cf3422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 196,
                "offset": -6,
                "shift": 114,
                "w": 141,
                "x": 623,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c71315ad-0478-4e7a-84fb-5ba823beb360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 196,
                "offset": 19,
                "shift": 114,
                "w": 116,
                "x": 477,
                "y": 200
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3593c3f2-dae0-45e2-93bb-3dc197baf005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 196,
                "offset": 4,
                "shift": 104,
                "w": 111,
                "x": 1058,
                "y": 200
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "67750b5d-4d54-4b80-a2a7-4357e7018175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 196,
                "offset": 1,
                "shift": 57,
                "w": 75,
                "x": 1615,
                "y": 596
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a77bcb4f-0511-4b35-a773-ee7d08a85a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 196,
                "offset": 13,
                "shift": 48,
                "w": 37,
                "x": 536,
                "y": 794
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9e8f3965-c007-437d-bb2d-ca08808344b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 196,
                "offset": -10,
                "shift": 57,
                "w": 75,
                "x": 1538,
                "y": 596
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3a505a8d-56ac-4488-b19e-4e81a2eab011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 196,
                "offset": 17,
                "shift": 100,
                "w": 82,
                "x": 1058,
                "y": 596
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "118601ba-9e89-4f99-baa6-072ac4e69fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 196,
                "offset": -2,
                "shift": 95,
                "w": 98,
                "x": 205,
                "y": 398
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0db32a7d-d882-4072-a0c9-95f05696737b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 196,
                "offset": 22,
                "shift": 57,
                "w": 35,
                "x": 613,
                "y": 794
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f5d6ce2d-db63-4c3f-a2dd-1917c1012ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 196,
                "offset": 7,
                "shift": 95,
                "w": 85,
                "x": 884,
                "y": 596
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "222ae950-62e3-401b-a07f-5ef0797a08e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 196,
                "offset": 6,
                "shift": 104,
                "w": 97,
                "x": 404,
                "y": 398
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "883caee1-646e-4013-85de-815c6ae059ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 196,
                "offset": 10,
                "shift": 95,
                "w": 87,
                "x": 1907,
                "y": 398
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7b818eb0-6408-4ca5-a366-e0413fac0b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 196,
                "offset": 10,
                "shift": 104,
                "w": 105,
                "x": 1607,
                "y": 200
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ffdd67e4-d728-4db5-ba23-55f4914bd12b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 196,
                "offset": 9,
                "shift": 95,
                "w": 86,
                "x": 534,
                "y": 596
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "31b0dfa5-592d-4508-9070-356a0afd25a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 196,
                "offset": 9,
                "shift": 57,
                "w": 72,
                "x": 1692,
                "y": 596
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3d6f1a30-775a-4354-be06-4d966f24439d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 196,
                "offset": 5,
                "shift": 104,
                "w": 102,
                "x": 1821,
                "y": 200
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "332765b5-2649-42b6-ab57-6d4c0955dabc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 196,
                "offset": 7,
                "shift": 104,
                "w": 95,
                "x": 600,
                "y": 398
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2d24ff27-1147-4cf6-84fd-7f8da462a741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 196,
                "offset": 6,
                "shift": 48,
                "w": 51,
                "x": 284,
                "y": 794
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2e6f702c-f5de-4b0d-8203-504fc479b9a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 196,
                "offset": -19,
                "shift": 48,
                "w": 76,
                "x": 1460,
                "y": 596
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a6cf3f85-c6f9-4029-9434-469096ba07f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 196,
                "offset": 6,
                "shift": 95,
                "w": 100,
                "x": 2,
                "y": 398
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "628b18b8-e93d-4cf8-ba66-95709a460308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 196,
                "offset": 6,
                "shift": 48,
                "w": 51,
                "x": 231,
                "y": 794
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "785c0abf-0c69-4efd-8515-ea2b661179b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 196,
                "offset": 6,
                "shift": 152,
                "w": 143,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "74106007-ce13-4fa6-a5ff-bc307436ee34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 196,
                "offset": 7,
                "shift": 104,
                "w": 95,
                "x": 503,
                "y": 398
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "85326530-f1c3-4dd2-86bd-362a0393998e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 196,
                "offset": 10,
                "shift": 104,
                "w": 93,
                "x": 889,
                "y": 398
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d18bd86d-a541-4dab-aaff-b8f374f14711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 196,
                "offset": -1,
                "shift": 104,
                "w": 105,
                "x": 1714,
                "y": 200
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "803c2880-50da-4384-ba6b-a4f97ea92f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 196,
                "offset": 10,
                "shift": 104,
                "w": 97,
                "x": 305,
                "y": 398
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "de34dfb4-9d8f-4bdb-a927-27e802f6ca47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 196,
                "offset": 5,
                "shift": 67,
                "w": 77,
                "x": 1381,
                "y": 596
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e7b4b262-efda-4e87-a886-ae77efa0879e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 196,
                "offset": 3,
                "shift": 95,
                "w": 92,
                "x": 1361,
                "y": 398
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "aae1c266-495c-48d7-99a9-1f2dbe830e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 196,
                "offset": 12,
                "shift": 57,
                "w": 55,
                "x": 66,
                "y": 794
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e896dfb2-8ace-49a7-a6de-b1f408bcf20a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 196,
                "offset": 12,
                "shift": 104,
                "w": 94,
                "x": 697,
                "y": 398
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1e41954b-4e53-494f-a0a3-b465c0b178c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 196,
                "offset": 12,
                "shift": 95,
                "w": 94,
                "x": 793,
                "y": 398
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8a99b91a-f742-4f77-ba18-b19b3c630e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 196,
                "offset": 12,
                "shift": 133,
                "w": 132,
                "x": 1035,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ee118710-367f-4e3c-ba51-d4ca1b51af21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 196,
                "offset": -4,
                "shift": 95,
                "w": 109,
                "x": 1171,
                "y": 200
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5d4521e2-9ebd-4952-a9f0-5e0293ee609e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 196,
                "offset": 1,
                "shift": 95,
                "w": 106,
                "x": 1391,
                "y": 200
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "999957fe-4aab-48f2-985b-d712150d340b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 196,
                "offset": 2,
                "shift": 86,
                "w": 87,
                "x": 180,
                "y": 596
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0bbad689-e2ec-4d35-9975-a5afee39f5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 196,
                "offset": 7,
                "shift": 67,
                "w": 77,
                "x": 1302,
                "y": 596
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cc9addf0-25b7-42be-9c89-5880e52d0179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 196,
                "offset": 14,
                "shift": 48,
                "w": 20,
                "x": 716,
                "y": 794
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7892e474-13e5-45fc-82fa-0781ad748677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 196,
                "offset": -15,
                "shift": 67,
                "w": 78,
                "x": 1142,
                "y": 596
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b2989f53-2d72-4884-a94f-42a5ed3a57cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 196,
                "offset": 11,
                "shift": 100,
                "w": 90,
                "x": 1455,
                "y": 398
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": true,
    "kerningPairs": [
        {
            "id": "4a5734a7-1645-4f8d-a556-78b7c6a1e642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 32,
            "second": 65
        },
        {
            "id": "b8697133-f1c6-4c98-80b2-b7ddabe85aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 89
        },
        {
            "id": "ff0bdf5e-e365-4b0a-8d90-4e253a674b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 32,
            "second": 902
        },
        {
            "id": "62f3cc0c-c334-484b-b6b7-a2d77a888c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 32,
            "second": 913
        },
        {
            "id": "a12bb563-c03c-4e60-87f0-f01432e36ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 32,
            "second": 916
        },
        {
            "id": "b68a6d95-bfb6-489b-91e1-9bc2cdcb8f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 32,
            "second": 923
        },
        {
            "id": "9e840218-cfd0-409a-b12a-133a8e2b8c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 933
        },
        {
            "id": "1c8d8be0-a8fb-4887-9224-b5a9a3971fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 939
        },
        {
            "id": "728c4bfe-06f2-4fc6-82eb-bd4044ee2d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 49,
            "second": 49
        },
        {
            "id": "419922a5-607b-4836-aaa5-82d087afae23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 32
        },
        {
            "id": "c7ca90b5-3e4f-4bd6-87f5-3806f85ee87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 65,
            "second": 84
        },
        {
            "id": "92b5db8c-350a-47f2-a1f7-3fda21e9e574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 65,
            "second": 86
        },
        {
            "id": "12e26316-31cb-4b18-8247-8eb4bd0321d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 87
        },
        {
            "id": "fd3a46c7-f1fb-43cb-b192-900ce684d5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 65,
            "second": 89
        },
        {
            "id": "ed9ff64e-8db0-4a61-b83b-ec25a4d6dde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 160
        },
        {
            "id": "e916c3aa-d9c6-4d34-92ff-69f6a8375d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8ebd030d-087a-46cc-93b7-6e3781709cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 70,
            "second": 44
        },
        {
            "id": "85cd0a9a-5f07-410b-92a7-05511515a913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 70,
            "second": 46
        },
        {
            "id": "e13f10aa-ad8a-4d68-8504-f84aabc3ade4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 65
        },
        {
            "id": "db0d4364-c6e6-41be-b8bf-50aaa4ec191d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 32
        },
        {
            "id": "f4c08fe1-d429-49a0-b468-e76260c732e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 76,
            "second": 84
        },
        {
            "id": "f60ef5b7-cd53-456e-bb0d-2f47adda2d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 76,
            "second": 86
        },
        {
            "id": "8df6caf4-0499-464b-a28e-b7dd37805185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 76,
            "second": 87
        },
        {
            "id": "1f51414a-b538-4f25-a58b-18e41bee0c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 76,
            "second": 89
        },
        {
            "id": "40c965ec-d5b7-453b-a746-a231ca970cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 160
        },
        {
            "id": "48e49599-7d89-48bd-9dcd-f5fdd7a08249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 76,
            "second": 8217
        },
        {
            "id": "d187ff1f-077a-40d9-8dc1-f9dff7484538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 32
        },
        {
            "id": "b8599d31-a5bd-477b-bc71-8fb5429464b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 80,
            "second": 44
        },
        {
            "id": "02dd3250-13bd-470b-942c-513dc43d9ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 80,
            "second": 46
        },
        {
            "id": "b19e66ec-17a1-4f39-b5f2-b76d7eab18d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 80,
            "second": 65
        },
        {
            "id": "d4bfd3d3-eac1-47c8-9b89-9fab20a62853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 160
        },
        {
            "id": "26e88ee6-7501-4a92-ae09-80742e71305c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "61e06935-c685-4d78-9f04-59f4cf7891b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 87
        },
        {
            "id": "dcb49a01-9f24-49e6-b66e-5c19c73789c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "aa937d52-6cac-4b66-aa3b-a14bf69907dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 44
        },
        {
            "id": "b1b2c2c1-f233-4a29-8a21-35643718aacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 45
        },
        {
            "id": "41eaef0b-6661-4861-8214-73835548d4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 46
        },
        {
            "id": "295262d9-8c15-4362-b6dc-5ed8e9275593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 58
        },
        {
            "id": "cccbdfb2-2a73-4108-8d9b-4717eb7f4599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 59
        },
        {
            "id": "518fb180-6cb0-4c36-aac9-760cbac57a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 65
        },
        {
            "id": "0e27c257-d998-4d47-9020-1bcd193dd33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 79
        },
        {
            "id": "e55d0e53-e162-4e16-9009-00e0b8486675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 97
        },
        {
            "id": "4ef58eee-38a7-4d4b-88fc-1eb8cdd00ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 99
        },
        {
            "id": "65058b94-ea98-447d-b4f5-a082b4da3e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 101
        },
        {
            "id": "38d6e0ee-a310-4fbf-ac02-8c32a0c64caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 105
        },
        {
            "id": "048145a0-37dc-43f7-b890-3917ef7c0669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 111
        },
        {
            "id": "287a4e02-f0b3-4eef-8ac3-da59fd69f52e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "48abe325-50f1-4332-9000-599d1388d066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 115
        },
        {
            "id": "9dff15f9-9c6a-44b9-b2f3-b30e0386a7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "71b1998b-6c2a-49bc-b27f-bbde6c6c1e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 119
        },
        {
            "id": "d386a9df-9704-4088-aec1-e5c97dc440a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 121
        },
        {
            "id": "bd444947-87d3-4975-858f-87115af2749d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 173
        },
        {
            "id": "1fffe61e-7de5-4db7-8d6e-b887470a2a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 894
        },
        {
            "id": "e8c6d074-01ee-4d68-8c31-8aa6009c0342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 86,
            "second": 44
        },
        {
            "id": "29e79673-0177-451f-934d-fd7dbcde887c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 45
        },
        {
            "id": "663bb73a-f864-4549-8bc5-d34e87624001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 86,
            "second": 46
        },
        {
            "id": "c88cc101-69aa-443e-b3a5-db6806c07f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 58
        },
        {
            "id": "f69b14c6-7722-46b0-8134-84e0e4961536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 59
        },
        {
            "id": "af027d67-2496-411c-81b4-aea2556e39b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 86,
            "second": 65
        },
        {
            "id": "d09ee625-47f3-4a45-be2a-ab7ac3b9c648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 97
        },
        {
            "id": "a9c42eab-3518-4fa1-b2d9-10012e20ec16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 101
        },
        {
            "id": "6d2a06e8-b033-4f88-b7a9-b4bfacb9b2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 105
        },
        {
            "id": "95f2a03f-2ac3-424a-8751-5c279fef8634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 111
        },
        {
            "id": "b97dcab2-2dac-478d-a928-e562bd25dbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 114
        },
        {
            "id": "84499963-2fa4-4425-9393-21ba544d551f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 117
        },
        {
            "id": "5fb192fc-cba0-4f74-902e-fb54472fca1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 121
        },
        {
            "id": "656d48e2-2a66-47d6-8724-688bad7d1307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 173
        },
        {
            "id": "540e31bd-15d2-4375-921a-2baff5258aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 894
        },
        {
            "id": "49c604d7-439a-4c44-9eb0-3eec2d7440f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 44
        },
        {
            "id": "583a8868-bd46-4d2b-aafc-52e5c92d901f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 45
        },
        {
            "id": "969e9569-f923-4a52-b48f-8bdd74f43817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 46
        },
        {
            "id": "025d3960-4e98-4983-888d-a3ed61e93a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 58
        },
        {
            "id": "d4864c33-c519-4379-88f0-420323267bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 59
        },
        {
            "id": "f142f76e-e73d-47c5-884b-9d6aa58487ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 87,
            "second": 65
        },
        {
            "id": "50665c1d-62a1-4ffb-9b52-6b963ff77e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "3900d74a-acde-47f9-8373-961359c2f02a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "9b3ce977-9b18-4346-aace-25ff3d34bd7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "3dc6e287-a4c6-4d73-9444-9425cdd8293f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "b0d66e91-1aa9-4b8f-8c05-a7a79c7aa820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 114
        },
        {
            "id": "a9c54fd4-2f67-4889-8fc3-fd221c2b28d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 117
        },
        {
            "id": "e81c036a-e784-4a2b-8aa5-73f28e5481b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 121
        },
        {
            "id": "e6683f5a-ab7e-4658-bec7-fba7dc6a32a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 173
        },
        {
            "id": "10f6539a-a2b4-45c4-b7f2-bac079287d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 894
        },
        {
            "id": "9cde6fe4-d969-4a9d-9637-2629b9bbdcd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 32
        },
        {
            "id": "d7770bef-c823-47cf-ab4c-5dd0d2ea4f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 44
        },
        {
            "id": "bea6d914-2155-4743-8d8b-b3a899ae9b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 45
        },
        {
            "id": "a45c920e-0e0e-4baa-a4f9-b79bc46aabac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 46
        },
        {
            "id": "95b98fd4-2e9a-4473-918a-7429e529a247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 58
        },
        {
            "id": "01a023bc-9ef4-4bcd-b63d-8b8d56c76079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 59
        },
        {
            "id": "0ed699ac-b4ba-42ad-8419-e981968259c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 65
        },
        {
            "id": "c422233e-838b-452f-9145-e18b5c38f652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 97
        },
        {
            "id": "2098a89f-5c4a-47d2-ae2d-17a31d3523ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 101
        },
        {
            "id": "bdeb23f9-dabe-4831-a298-c82745b0192a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 105
        },
        {
            "id": "c5be7264-03b5-4cbc-bb83-286e3db17bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "d353bdcd-ec1f-4984-89c4-df4c6aecfeef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 112
        },
        {
            "id": "d20e7048-5234-454d-b989-6fbdccda6b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 113
        },
        {
            "id": "4664abb6-baa8-4160-b1f8-470156120a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 117
        },
        {
            "id": "086d96d7-e539-4fa8-a4b3-6490f1b781c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 118
        },
        {
            "id": "88908adf-25b9-4394-b3ab-2f6286c125fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 160
        },
        {
            "id": "96d450a4-3bfc-4444-bd0e-24debc2bf603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 173
        },
        {
            "id": "66c91d81-fc2a-49a6-9263-6b12c29ecb90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 894
        },
        {
            "id": "a75e0632-cb9f-4b90-9457-1b5c6374c7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 102
        },
        {
            "id": "dca282d3-ffb4-4684-a123-eab95c6678d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8217
        },
        {
            "id": "83f313ae-8356-416b-a7c1-e829bcd29714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 114,
            "second": 44
        },
        {
            "id": "3b37eeed-0daf-4dec-b034-bf829af4b97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 114,
            "second": 46
        },
        {
            "id": "5d1deead-5511-4649-a37d-1d2f05070dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 114,
            "second": 8217
        },
        {
            "id": "ef81fb81-e133-4840-85f8-51b5973411e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 44
        },
        {
            "id": "971170e3-9711-43ea-a5c5-6808c615d503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 46
        },
        {
            "id": "30afb43a-9ef2-4a7e-a2a9-5f2ae996b6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 119,
            "second": 44
        },
        {
            "id": "1431e9ce-9bb8-4466-be0a-7da4a4765dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 119,
            "second": 46
        },
        {
            "id": "19f83710-da76-4057-8e45-4fd4ce7cf2d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 44
        },
        {
            "id": "200cf262-ec63-4baa-91e6-191a39086557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 128,
    "styleName": "Bold Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}