if (!solid)
{
	return;	
}

if (take_damage(other, id, breakable_damage_key) != 0)
{
	solid = false;
	image_speed = 1;
	tilemap_clear_tile(0);
	visible = true;
}