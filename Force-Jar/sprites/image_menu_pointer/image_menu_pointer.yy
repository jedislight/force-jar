{
    "id": "f15cd953-febe-4cdd-a761-6ea656ba5c59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "image_menu_pointer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 4,
    "bbox_right": 125,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a52910b6-9ce5-4f34-b599-64aa70715de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f15cd953-febe-4cdd-a761-6ea656ba5c59",
            "compositeImage": {
                "id": "73a918e9-6059-4fd8-8bc5-7e0321f8fbb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a52910b6-9ce5-4f34-b599-64aa70715de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "349d3ce5-777c-4c57-baff-e4d6a1c07c3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a52910b6-9ce5-4f34-b599-64aa70715de7",
                    "LayerId": "516495a8-8da7-45c3-997b-baab4a989d7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "516495a8-8da7-45c3-997b-baab4a989d7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f15cd953-febe-4cdd-a761-6ea656ba5c59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 125,
    "yorig": 63
}