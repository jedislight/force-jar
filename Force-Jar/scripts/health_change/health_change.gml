/// @function health_add(instance, health_to_add)
/// @desc Change health, a negative value would remove health from current.
/// @param {Real} instance id of the instance to use.
/// @param {Real} health_delta change to health.

var instance = argument0;
var health_delta = argument1;

assert(instance_exists(instance), "Cannot add health, not targetting an instance");
assert_exit

assert(instance_has_health(instance), "Cannot add health, no health data is present");
assert_exit

assert(is_real(health_delta), "Cannot add a health ammount of [" + string(health_delta) + "]: Not a Number.");
assert_exit

instance.health_current = clamp(instance.health_current + health_delta, 0, instance.health_max);

if(instance.health_current <= 0)
{
	instance_destroy(instance);	
}