/// @description Insert description here
// You can write your code in this editor

horizontal_motion_input = global.input_manager.horizontal_motion_input;
vertical_motion_input = global.input_manager.vertical_motion_input;

action_jump = global.input_manager.action_input_1;
action_jump_pressed = global.input_manager.action_input_1_pressed;

action_grapple = global.input_manager.action_input_2;
action_grapple_pressed = global.input_manager.action_input_2_pressed;

action_attack = global.input_manager.action_input_3;
action_attack_pressed = global.input_manager.action_input_3_pressed;
action_attack_released = global.input_manager.action_input_3_released;

action_offhand_attack = global.input_manager.action_input_4;
action_offhand_attack_pressed = global.input_manager.action_input_4_pressed;
action_offhand_attack_released = global.input_manager.action_input_4_released;

action_weapon_select_pressed = global.input_manager.action_input_5_pressed;

action_offhand_weapon_select_pressed = global.input_manager.action_input_6_pressed;

pause_input_pressed = global.input_manager.pause_input_pressed;