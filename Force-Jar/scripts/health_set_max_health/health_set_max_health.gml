/// @function health_set_max_health(instance, health_new_max)
/// @desc Sets new max health to any instance with health_data set.
/// @param {Real} instance id of the instance to adjust.
/// @param {Real} health_new_max new max health value to set.

var instance = argument0;
var health_new_max = argument1;

assert(instance_exists(instance), "Cannot change health_max, instance does not exist");
assert_exit

assert(instance_has_health(instance), "Cannot change health_max, instance has not health set.");
assert_exit

assert(is_real(health_new_max), "Cannot change health_max to value of [" + string(health_new_max) + "]: Not a number.")
assert_exit

instance.health_max = health_new_max;