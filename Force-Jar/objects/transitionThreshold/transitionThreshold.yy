{
    "id": "e0bfbe0a-568c-4008-a6da-3d28538bef97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionThreshold",
    "eventList": [
        {
            "id": "f1b91ed5-0df4-42ef-bfdc-347a7702396f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "380edad1-487c-4192-bac0-65401ddf29b8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0bfbe0a-568c-4008-a6da-3d28538bef97"
        },
        {
            "id": "5cf56893-6b33-4a5b-b004-037fdba59218",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0bfbe0a-568c-4008-a6da-3d28538bef97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "dd8fe835-63b3-49ae-9eae-ded57d8c8563",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 512,
            "value": "room0",
            "varName": "target_room",
            "varType": 5
        },
        {
            "id": "625ea977-7c7f-47b1-b313-8aebf334d8b1",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "door_offset",
            "varType": 1
        },
        {
            "id": "26ea3ebb-e766-454a-8fbf-7234295f2abb",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 4,
            "value": "noone",
            "varName": "music",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "e548521f-3b8c-42b0-84d6-b83c16307be7",
    "visible": false
}