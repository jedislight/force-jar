/// @description Handle being damaged

if (ds_map_find_value(damage_object_ignore, other.id))
{
	exit;
}

var damage_amt = take_damage(other, id, damage_data_key);

if (damage_amt != 0)
{
	health_change(id, -damage_amt);
	
	if (health_current <= 0)
	{
		exit;
	}
}

ds_map_add(damage_object_ignore, other.id, 5);

if(!heavy)
{
	knock_back(id, other.x, other.y);
}

flash_once = true;