{
    "id": "934cd068-3ff2-428f-b00a-eda1119b3d78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_purple_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a91f3d85-7f7d-4c55-808d-6371363180d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "934cd068-3ff2-428f-b00a-eda1119b3d78",
            "compositeImage": {
                "id": "1a53a565-f3af-4315-81da-7c3bb8935a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a91f3d85-7f7d-4c55-808d-6371363180d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d72933f-89e2-4537-b41f-5a508fbc745b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a91f3d85-7f7d-4c55-808d-6371363180d5",
                    "LayerId": "1435a93c-6d58-4f73-b1ec-1db32877cc0a"
                },
                {
                    "id": "6b39d42c-a21a-4d76-8607-4cacf7e84892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a91f3d85-7f7d-4c55-808d-6371363180d5",
                    "LayerId": "c74e0d99-cd56-4a8b-98d1-703b9c285fe4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c74e0d99-cd56-4a8b-98d1-703b9c285fe4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "934cd068-3ff2-428f-b00a-eda1119b3d78",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1435a93c-6d58-4f73-b1ec-1db32877cc0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "934cd068-3ff2-428f-b00a-eda1119b3d78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}