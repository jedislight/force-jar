/// @description Insert description here
// You can write your code in this editor

--duration;
if(duration <= 0)
{
	instance_destroy()
}
if(instance_exists(target))
{
	target.x = target.xprevious
	target.y = target.yprevious
	var dx = lengthdir_x(other.intensity, direction);
	var dy = lengthdir_y(other.intensity, direction);
	
	with(target)
	{
		apply_movement(dx, dy)
	}
}