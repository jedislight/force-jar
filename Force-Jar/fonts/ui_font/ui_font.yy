{
    "id": "ec8f5e2f-4a9d-4b2e-8ad6-b2bb214f5bf8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "ui_font",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "60ba0349-3260-442a-84bd-aa0ccbc0e8ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 37,
                "y": 110
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "eb3b8611-3991-4939-86e1-5c7a90035e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 113,
                "y": 110
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b356c95e-2399-4d6f-b386-3d50bd7744a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "67a0e05e-cca2-4b7c-914d-dc235a542072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 227,
                "y": 29
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d6925584-4d65-4762-ab22-2ed0e84a401a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 119,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "53ed2c69-12d5-49ac-bb61-d0c1ac08f1f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c6b65600-c641-40f9-a759-370a29d0304b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "02ccfed3-72d0-4c30-98d4-a247dd12b3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 125,
                "y": 110
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b260fbbc-3535-43a7-b4cd-47d884805d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 61,
                "y": 110
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "09155a15-639c-4add-8dc5-d9708826ca76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 69,
                "y": 110
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bc6d88aa-fa7f-4674-86e9-1a990b968da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2fd812ce-5546-411c-a3f4-3ce4cad204d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "61972791-6f71-40f2-bf6a-e066caeffb81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 156,
                "y": 110
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6a1c2fc8-03fa-4b0a-8737-0afab36c6765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 110
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b4cf5dba-4e55-488f-9a3e-4555147b5f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 119,
                "y": 110
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cee0f3a6-43f1-4441-9a61-85f4f9998c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 110
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fd04a2c9-517a-4e44-b656-5675972cf975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8e230060-796f-4d77-b616-88fed21c90d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 45,
                "y": 110
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "59455606-d872-4b7a-a82f-98f728558c01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4d871ac9-40d7-4554-8c88-0d1c6d77e43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8ac56d49-25f0-4c71-9751-8f190cf3ca23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 83
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "397b4b83-6f7a-4d07-b503-b7e1ce80be3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 83
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5d2b4b24-8ec2-4fa8-8d16-f185fadefd19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a6c2b2bc-8525-437d-8bf4-cf4c98e2407c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 176,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e203c350-b94f-4e77-949e-f04317620842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 223,
                "y": 56
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "be2ebee1-4640-4d66-af44-04fd19611176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 236,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "00d16e93-5d7c-417c-b544-c427038b4f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 141,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "742cc238-cb82-4381-a6fc-9fc35dfe7f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 146,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "07b5a253-091b-47bb-8c0f-18d145147d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 56
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8338678b-b197-41ce-961a-5533d25c0550",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 241,
                "y": 29
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e82a2a62-bc01-4305-9cd8-62bc37853cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d7e516ef-14fb-4915-97aa-a93f8b53d413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 184,
                "y": 56
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9612eb41-7506-4679-81f3-fe42063803f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d6eec31f-993e-43ed-a973-c160b6cad39b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "283e2220-ec03-4a1e-af68-dde488b6c692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 199,
                "y": 29
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a3246d06-2cef-44b8-9ce2-a21b91138801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "87060d63-191d-4b41-b61e-d4a4fd75a99c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4d331bc8-e1c7-4c92-94ca-2eec3384e17a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 185,
                "y": 29
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9ec10d95-ea0d-4153-a12f-a2d04e14049c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "581369d1-7c06-49f0-876f-1f0a6b56eef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b3a44a07-6acd-4291-8ecf-aad35e24e3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 50,
                "y": 29
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8c767232-6458-4216-879b-b3bbd65cadb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 136,
                "y": 110
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1d3df29a-c9ab-4f1f-a3c1-947cfef95a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 200,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e2bd8542-715f-4cae-bd65-d10c04c32914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 65,
                "y": 29
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "01a587e0-4302-495b-ab07-80ffd176cd3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 164,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8556171e-f9a4-4c17-9ee5-a7fdf50f658d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bde764f1-bfae-4a75-89d3-d37519b243e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 155,
                "y": 29
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e46f2117-5c2c-4508-afde-ddef4d0be137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "13a12169-aa65-47fd-947c-406004f66817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 95,
                "y": 29
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9a2064f7-6089-41e7-aa77-5fc56b01f696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c897e3b0-da59-4f69-863f-e6c7c072cc2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c5895d1d-765f-4a81-8944-9fbfae22617c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 170,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5f5089e4-b8d6-4c76-ab94-f1dc68351e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 125,
                "y": 29
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c54a4c35-486b-4ddb-a213-5ed65cb532c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 140,
                "y": 29
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c8ba43e9-5aa2-4f46-9972-0687287197ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "be7e5a82-c071-4b88-b2b6-f18b87313d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2c5269ca-982a-4f8b-ba5d-0cda287b30a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 34,
                "y": 29
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5d9c0d3f-7b70-4cff-8f1b-02e3c42eb6f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d9c4a8a1-74c0-4c35-854c-b502443274e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 110,
                "y": 29
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3fbf1331-85d4-4ec6-a1e1-033eb4cd3369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 99,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "15b75c25-b5ac-4282-9718-77fcf8c6b372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 110
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4d43c168-84ec-41de-b505-c6ac9a0b65ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 110
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "56a4b0e4-0dc4-4232-8b72-bb3bc76691e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 116,
                "y": 83
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2bbef1ce-0e7d-474f-9498-ae8b3d008929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 80,
                "y": 29
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b249da2c-59c4-4434-b8c9-6c2e2181020a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 92,
                "y": 110
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d54a1498-b87c-495f-8d9b-73604ca37849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 56
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5ebd75f8-6865-4e15-8a2a-080d1735091e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 80,
                "y": 83
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "db0ed4b1-e442-4961-a139-10237d41b13a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 56
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bd16977b-4edb-48f2-9ce4-ef6675257865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 56
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "39998fb6-bc33-4274-8ac1-b958fafb357b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d7b3c15d-fd9f-4b5a-85b9-ee3f5c8996c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3ab34c97-b920-447a-800c-fd01561ab1b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 83
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c276c3bc-2e35-4fbd-9a1d-6f34b643c560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 104,
                "y": 83
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "af6307d0-c237-4576-a401-d211777165fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 131,
                "y": 110
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "218ee485-ec44-48d9-934c-29206d5745b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 106,
                "y": 110
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1cec59d7-95d8-4007-9bb5-ff8a5547f1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 128,
                "y": 83
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "473c1140-2007-4fde-8ddb-ac1a2bab05c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 161,
                "y": 110
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6d161335-9531-42d7-ab0a-47556cac9662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "63a9fa29-252c-44ed-8798-4f9b11cca5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 92,
                "y": 83
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d38dec79-7adf-4e72-94c8-7ed036c4c5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 83
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6c1300b2-1960-4e19-860f-6c4680b88beb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 188,
                "y": 83
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c66be425-37af-4438-81b7-9eddcc1d20fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "58d520c6-55a2-44a3-b84b-0129cce7811c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 221,
                "y": 83
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "27449e34-70a5-4ca6-bbdf-224b88e15e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 140,
                "y": 83
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6d378010-85f1-4337-9908-4b1e33115e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 110
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "78582253-4288-44b1-a324-41f224a0abc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 152,
                "y": 83
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "97f614d4-cc8f-41ab-9005-dc13eafce9dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 56
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c658084f-47bf-4874-aed2-2747e8569ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "19c9ab42-91af-4a7b-a245-dca1f33d02ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 93,
                "y": 56
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "595836c7-5f03-4b32-bfa5-874731d2671c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 158,
                "y": 56
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "988978bf-7b4e-443d-bd70-57f83c0797e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 197,
                "y": 56
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8c962f13-4c8e-4807-bbe6-da4b31772bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 110
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "51881e4b-a5a3-4623-b51d-d7f82b105ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 151,
                "y": 110
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9ef8129e-4e6d-4ab8-93ed-5bcb11119656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 239,
                "y": 83
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "61a07123-67d8-45a8-b694-1196bcf8b840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 213,
                "y": 29
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "5b740bd9-44a1-4a04-b3d9-9b19a6bc4282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "c4408780-e11b-4c46-a925-9b67109ee383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "3d7d9e1a-a9ee-44ce-ada8-a369ecb8837e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "e8c8cd8f-dba1-4b78-90eb-3fbe5feb6dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "48345e06-e531-473a-aff3-033639614c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "dd7692ed-f9bd-405a-b5de-08f922d1f160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "37f180ee-dc26-4a8d-9db3-a4a7fa4565b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "67f30514-e20d-40b9-a6d3-7ea73da3e1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "a40286aa-de12-42fe-b686-8ab659e62a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "58ac4e0a-c317-4bdd-8ec1-6cfe5789bf66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e6c122fc-1b5e-4089-96a7-c34b15a97258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "3e4108c4-d7a3-4932-9612-5c3f8d6c08d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "241ac9e5-acd0-47b5-b201-1670035eb53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "338a6131-427a-4a2f-9d59-18487051fcda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "300cb17b-00d2-4ebc-9507-d378161fddfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "0529c767-90e5-4a9f-b3d4-e45b5b3ed6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "970fec54-40d2-40e6-b846-215778470319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "ad2386da-f622-48a8-8e5e-a7efa88b22d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "95f841cd-8cd9-481a-b771-d27323249f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "4476ed62-091c-454a-a8fa-df00401934b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "3b304183-f80c-4a28-9da2-1814c55bacb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "be432ed7-80f1-40c4-b575-11b93e093f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "e9edd333-619b-4e58-be81-edfbf9091999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "a52d0154-306c-49b5-97de-fa84868ed134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "92c23b88-b5b1-4e77-b118-85e59f6793a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "98fd58dc-8a8a-449d-8a25-ced70f4d9554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "1c9c501a-947e-4314-8bbe-2f8615896765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "0c2cadbc-5d5f-442f-afa6-0808ed7d6cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "627eded3-f7df-461d-b7a4-ae58ad727c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "c89aae33-fb67-4300-9cba-cbde1890fc25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "145b4577-2c80-446f-a30f-d8da70873131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "c0e28bfb-ebc7-49d9-b466-54ca67da2bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "cbec01bc-9abf-44e5-8bbe-92a88385d294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "6f0c9a74-a27e-480d-9c45-40af77d843ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "707837b4-59ea-4fb9-90c2-d0b84eeb34b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "856a7f1e-9423-4504-b24c-6a20042b24a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "5a68a6b8-c260-4c22-beb6-ce71ba20d061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "c23c07c7-4301-44a2-9018-ae59920865a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "c7fa3186-d8be-4555-a746-90e8e84177cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "9dc6ee2c-ba6d-460b-95e2-cb11e6f43203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "ab9a9c47-7f51-4177-8047-76d0a674eab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "162c2264-4474-4a7e-97bb-ef1f37755639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "cee0736a-774d-4b52-b392-815175a77966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "43001141-6390-431a-99d1-abc8d2dabad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "a8f16a76-ecc4-4146-a532-42f0baefcedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "c59a46fe-4184-4329-9c0a-0918c7c3abf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "fdc2e01b-a66f-4447-b005-180cb7f98f78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "85ab8f06-4bbe-4b5c-9d1c-5e775f2e8960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "de4be266-3444-4433-8088-651bf0cb8b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "1f9bb023-7f01-4b87-afc6-6697945a01b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "00ac8ee3-cf06-48e1-bb4e-4a65ea142df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "4e5abe62-59c5-4228-9146-141aa98ad2b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "778a373a-5f1c-42b1-af32-5e8f177730f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "1184d779-6da3-4330-95b6-ee21df446da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "0d88f77d-b8e1-4339-9071-8783e119b51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "e6a230f4-9848-4714-9c3e-54b2e936e8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "4f56da68-8983-41f8-8e24-11cdcbc198e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "ac187edb-f512-4939-bf43-61301c0ff997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "6ce105c2-d803-45ee-bdf4-314ccb0d54af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "6a4ab68e-bb89-411b-9d4b-4bb1217222dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a1de744b-408d-4618-9d3f-02367ab5d785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "aaaedd4e-64bb-4068-8df9-a52bdc79b4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "f7d63925-6027-498d-818e-ecb631261e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "0e50d2ec-3427-4830-9cec-ceb6f6c5e6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "c8534742-c0eb-47f6-ae00-0e757bfaa438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "8cf84e79-5cdd-48dc-aa6e-a6000de3e600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "33aa172c-aa82-4b1d-afee-c6f3df96435b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "117e27a5-d027-4d10-8ffc-4cc6615c9599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "b97656ba-ecc6-415c-ada8-20152cf0a42d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "03b79cdb-235b-4199-b7e5-5db69db87718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "8763a11c-d301-4e9d-ad44-8d090ead7785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "7481284b-f2e7-437c-9e76-3cae3e344d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "d81b6d36-b2b4-4a6d-82fd-9f12fbc6e068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "46a19dce-9a0a-423b-aa0e-6924c190a611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "d167a90e-3df9-4bca-9880-b80b8dfdc65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "ee6ee4cc-941f-4627-9f8e-dd770f5f2871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "b521355a-86dd-4e83-ac46-6c5a6f4c2eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "2f923295-a2c8-4ded-a946-00b3effe2ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "690b6ed7-70d3-4130-afa0-994ed4580704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "a24f9ed6-2ca5-4337-92d0-dba5b7f96439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "2c6d5727-cb43-4782-8f5a-cbf831e73369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "ba501c81-14d6-4c9c-80cf-b6da974215d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "c86c0875-d063-4a92-bc6a-adb63d933706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "94c89429-f028-4884-9001-2acc9d8a61a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "addb4d2f-023e-4252-9255-b3dd8e6f3ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "6db6aad2-c02a-4ad3-b04e-0ea1732639d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "18d9ffb8-db12-4b59-8af4-d4c170c51905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "7ff834be-ffd9-417f-a5a6-ec1e6621940e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}