var drawable = argument0;
var facing_sign = argument1;
draw_sprite_ext(drawable.sprite_index, drawable.image_index, x+32+drawable.anchor_offset_x*facing_sign, y+96+drawable.anchor_offset_y, 1.0*facing_sign, 1.0, drawable.anchor_angle*facing_sign, drawable.image_blend, drawable.image_alpha);