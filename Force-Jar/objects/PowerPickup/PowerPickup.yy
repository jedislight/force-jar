{
    "id": "0fa77905-b3a0-4aae-97fe-da24b75fd572",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PowerPickup",
    "eventList": [
        {
            "id": "b5d7c1f8-bc45-4a58-8b9c-df5b675098d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0fa77905-b3a0-4aae-97fe-da24b75fd572"
        },
        {
            "id": "a4b7e4b7-5eb1-4d27-b0dd-f1207be00cb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0fa77905-b3a0-4aae-97fe-da24b75fd572"
        },
        {
            "id": "3278c807-c849-4609-9ecd-6e275cec8645",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0fa77905-b3a0-4aae-97fe-da24b75fd572"
        },
        {
            "id": "7020677b-d0aa-4171-9ad8-09aaaffdc5e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fce370f7-a038-4188-bfcb-126b004a9dfc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0fa77905-b3a0-4aae-97fe-da24b75fd572"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "08326d2e-83c9-478a-be4e-139d9e976044",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "Powers.POWERS_START",
            "varName": "power_granted",
            "varType": 4
        },
        {
            "id": "d5ea92a1-49b2-4faa-bb50-e80c6c9574b4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 2,
            "value": "sprite6",
            "varName": "power_up_image",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "818a0f22-fc15-447b-adb6-1977adddf408",
    "visible": true
}