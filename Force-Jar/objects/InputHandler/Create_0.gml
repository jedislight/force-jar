/// @description Define core Input Features.

#region Axis and Inputs
horizontal_motion_input = 0;
horizontal_input_pressed = 0;
vertical_motion_input = 0;
vertical_input_pressed = 0;

action_input_1 = false;
action_input_1_pressed = false;

action_input_2 = false;
action_input_2_pressed = false;

action_input_3 = false;
action_input_3_pressed = false;
action_input_3_released = false;

action_input_4 = false;
action_input_4_pressed = false;
action_input_4_released = false;

action_input_5 = false;
action_input_5_pressed = false;

action_input_6 = false;
action_input_6_pressed = false;

pause_input_pressed = false;
cancel_input_pressed = false;
confirm_input_pressed = false;
#endregion

#region Controller Setup

cntr_horizontal_motion = 0;
cntr_horizontal_input = 0;
cntr_vertical_motion = 0;
cntr_vertical_input = 0;

cntr_action_1_btn = gp_face1;
cntr_action_1 = false;
cntr_action_1_pressed = false;

cntr_action_2_btn = gp_face2;
cntr_action_2 = false;
cntr_action_2_pressed = false;

cntr_action_3_btn = gp_face3;
cntr_action_3 = false;
cntr_action_3_pressed = false;
cntr_action_3_released = false;

cntr_action_4_btn = gp_face4;
cntr_action_4 = false;
cntr_action_4_pressed = false;
cntr_action_4_released = false;

cntr_action_5_btn = gp_shoulderrb;
cntr_action_5 = false;
cntr_action_5_pressed = false;

cntr_action_6_btn = gp_shoulderlb;
cntr_action_6 = false;
cntr_action_6_pressed = false;

cntr_pause_btn = gp_start;
cntr_pause_pressed = false;

cntr_cancel_btn = gp_select;
cntr_cancel_pressed = false;

target_gamepad = -1;
#endregion

#region Default Key Bindings

// Using keyboard_set/get_map we can use these real values to change the bindings
// Research needs to be done to determine how to display these changes to the end user.
// Also look into remapping on launch by config?

up_key = vk_up;
down_key = vk_down;
right_key = vk_right;
left_key = vk_left;

action1_key = ord("Z");
action2_key = ord("X");
action3_key = ord("A");
action4_key = ord("S");
action5_key = ord("Q");
action6_key = ord("W");

pause_key = vk_enter;
cancel_key = vk_escape;

#endregion