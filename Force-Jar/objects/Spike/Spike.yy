{
    "id": "ec924d73-d5a5-4c6a-8fcc-62fef3807d65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Spike",
    "eventList": [
        {
            "id": "bf3f1346-23b1-42f2-97a4-8cde3d98a4b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec924d73-d5a5-4c6a-8fcc-62fef3807d65"
        },
        {
            "id": "5af9d043-9b21-4615-8854-262767383cc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ec924d73-d5a5-4c6a-8fcc-62fef3807d65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "231bb874-0476-4220-95b1-89b3c1727f79",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
            "propertyId": "92f9b37e-a3ea-45b9-9477-ede2ba62289e",
            "value": "40"
        }
    ],
    "parentObjectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db365932-988b-48b6-bf5b-c7796047f569",
    "visible": true
}