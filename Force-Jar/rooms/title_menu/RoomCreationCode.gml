var menu_control = instance_create_layer(0, 0, "Instances", MenuControl);

menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "START GAME"));
menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "LOAD GAME"));
menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "SETTINGS"));
menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "EXIT TO DESKTOP"));

menu_init(menu_control);

var background_music = instance_find(BackgroundMusic, 0);
assert_fatal(instance_exists(background_music), "Could not find Title Screen BGM");
music_manager_play(background_music.music);