frame += 1;
var collided_line = collision_line(x,y, owner.x+32, owner.y+96, Solid, false, true);
var collided_with = collision_rectangle(x-5, y-5, x+5, y+5, Solid, false, true);
var collided_any = max(collided_line, collided_with) > 0;
if(collided_any)
{
	locked = true;
	if(collided_line and not collided_with)
	{
		instance_destroy();
	}
	else if(collided_line and pull)
	{
		instance_destroy();
	}
}

if(not locked or pull)
{
	if(frame != 1)
	{
		x += (owner.x - owner.xprevious);
		y += (owner.y - owner.yprevious);
	}
	var dx = 0;
	var dy = 0;
	if(retracting)
	{
		image_angle += velocity;
		dx = lengthdir_x(-velocity, direction);	
		dy = lengthdir_y(-velocity, direction);
	}
	else
	{
		image_angle -= velocity;
		dx = lengthdir_x(velocity, direction);	
		dy = lengthdir_y(velocity, direction);
	}
	var old_distance = point_distance(x,y, owner.x+32, owner.y+96);
	apply_movement(dx,dy);
	var new_distance = point_distance(x,y, owner.x+32, owner.y+96);
	if(retracting and new_distance > old_distance)
	{
		instance_destroy();
	}
	if(point_distance(x,y, owner.x+32, owner.y+96) > max_extent*64)
	{
		retracting = true;
	}
}

if((retracting or locked) and point_distance(x,y, owner.x+32, owner.y+64) < velocity)
{
	instance_destroy();
}

