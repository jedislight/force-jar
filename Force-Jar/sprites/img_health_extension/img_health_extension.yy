{
    "id": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_health_extension",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 61,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b904f6d3-ca25-4731-9919-ff14a3a31dd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
            "compositeImage": {
                "id": "e719235d-ed55-4e19-884e-fe9f5776ec85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b904f6d3-ca25-4731-9919-ff14a3a31dd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d24d166-6623-4911-a4d0-95325dd16722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b904f6d3-ca25-4731-9919-ff14a3a31dd3",
                    "LayerId": "46ce3e9b-14ce-4b10-a223-03ada9d53d93"
                },
                {
                    "id": "d0d96648-6306-4cf0-be92-56d085a7bfc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b904f6d3-ca25-4731-9919-ff14a3a31dd3",
                    "LayerId": "659fe12e-dec9-4a77-98be-e873a0a6a200"
                },
                {
                    "id": "192259ea-d517-430a-a09f-0f49dbdbb445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b904f6d3-ca25-4731-9919-ff14a3a31dd3",
                    "LayerId": "aa8c4bfc-3af8-424a-9829-9290a61e7f04"
                },
                {
                    "id": "8d9e47ee-bc28-48b5-88e3-5f67c8add492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b904f6d3-ca25-4731-9919-ff14a3a31dd3",
                    "LayerId": "a35fd8cd-9066-47be-bd40-483abb0eee01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aa8c4bfc-3af8-424a-9829-9290a61e7f04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a35fd8cd-9066-47be-bd40-483abb0eee01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "659fe12e-dec9-4a77-98be-e873a0a6a200",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
            "blendMode": 3,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "46ce3e9b-14ce-4b10-a223-03ada9d53d93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}