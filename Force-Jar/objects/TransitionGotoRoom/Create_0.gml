/// @description Change Rooms

if (!variable_instance_exists(id, "target_room"))
{
	show_error("No target_room given to TransitionGotoRoom, this operation cannot complete.", true);
	// If we get to here, try to move transition state forward.
	event_user(0);
	exit;
}

room_goto(target_room);
exit;