{
    "id": "7ed22e10-dce3-40fd-a341-e9d43a0f4d1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RapierAttack",
    "eventList": [
        {
            "id": "f1280278-a795-495d-a3cd-be73036e66c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7ed22e10-dce3-40fd-a341-e9d43a0f4d1e"
        },
        {
            "id": "cd19c3c2-0b64-409b-9aa2-48c8a534b45e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7ed22e10-dce3-40fd-a341-e9d43a0f4d1e"
        },
        {
            "id": "b4c432d6-b707-4c49-b390-2478f4105f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "7ed22e10-dce3-40fd-a341-e9d43a0f4d1e"
        },
        {
            "id": "a63aceb3-9b23-46a0-a575-d2effe1d14ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "7ed22e10-dce3-40fd-a341-e9d43a0f4d1e"
        },
        {
            "id": "f785dd83-617a-404a-865c-9da2a92fd7ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7ed22e10-dce3-40fd-a341-e9d43a0f4d1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b337b87-c1e2-4141-9930-cfdc443b15fb",
    "visible": true
}