/// @description Setup All Inputs for this step

#region Get Controller Inputs only if a supported controller is connected

if (gamepad_is_supported())
{
	if (gamepad_is_connected(target_gamepad))
	{	
		var cntr_down_pressed = (gamepad_button_check_pressed(target_gamepad, gp_padd)) ? 1 : 0
		var cntr_up_pressed = (gamepad_button_check_pressed(target_gamepad, gp_padu)) ? 1 : 0;
		var cntr_right_pressed = (gamepad_button_check_pressed(target_gamepad, gp_padr)) ? 1 : 0;
		var cntr_left_pressed = (gamepad_button_check_pressed(target_gamepad, gp_padl)) ? 1 : 0;
			
		cntr_horizontal_motion = gamepad_button_check(target_gamepad, gp_padr) - gamepad_button_check(target_gamepad, gp_padl);
		cntr_horizontal_input = cntr_right_pressed - cntr_left_pressed;
		cntr_vertical_motion = gamepad_button_check(target_gamepad, gp_padd) - gamepad_button_check(target_gamepad, gp_padu);
		cntr_vertical_input = cntr_down_pressed - cntr_up_pressed;
			
		cntr_action_1 = gamepad_button_check(target_gamepad, cntr_action_1_btn);
		cntr_action_1_pressed = gamepad_button_check_pressed(target_gamepad, cntr_action_1_btn);

		cntr_action_2 = gamepad_button_check(target_gamepad, cntr_action_2_btn);
		cntr_action_2_pressed = gamepad_button_check_pressed(target_gamepad, cntr_action_2_btn);

		cntr_action_3 = gamepad_button_check(target_gamepad, cntr_action_3_btn);
		cntr_action_3_pressed = gamepad_button_check_pressed(target_gamepad, cntr_action_3_btn);
		cntr_action_3_released = gamepad_button_check_released(target_gamepad, cntr_action_3_btn);

		cntr_action_4 = gamepad_button_check(target_gamepad, cntr_action_4_btn);
		cntr_action_4_pressed = gamepad_button_check_pressed(target_gamepad, cntr_action_4_btn);
		cntr_action_4_released = gamepad_button_check_released(target_gamepad, cntr_action_4_btn);

		cntr_action_5 = gamepad_button_check(target_gamepad, cntr_action_5_btn);
		cntr_action_5_pressed = gamepad_button_check_pressed(target_gamepad, cntr_action_5_btn);

		cntr_action_6 = gamepad_button_check(target_gamepad, cntr_action_6_btn);
		cntr_action_6_pressed = gamepad_button_check_pressed(target_gamepad, cntr_action_6_btn);

		cntr_pause_pressed = gamepad_button_check_pressed(target_gamepad, cntr_pause_btn);
		cntr_cancel_pressed = gamepad_button_check_pressed(target_gamepad, cntr_cancel_btn);
	}
}
#endregion

var down_pressed = (keyboard_check_pressed(down_key)) ? 1 : 0;
var up_pressed = (keyboard_check_pressed(up_key)) ? 1 : 0;
var right_pressed = (keyboard_check_pressed(right_key)) ? 1 : 0;
var left_pressed = (keyboard_check_pressed(left_key)) ? 1 : 0;

horizontal_motion_input = (cntr_horizontal_motion != 0) ? cntr_horizontal_motion : keyboard_check(right_key) - keyboard_check(left_key);
horizontal_input_pressed = (cntr_horizontal_input != 0) ? cntr_horizontal_input : right_pressed - left_pressed;
vertical_motion_input = (cntr_vertical_motion != 0) ? cntr_vertical_motion : keyboard_check(down_key) - keyboard_check(up_key);
vertical_input_pressed = (cntr_vertical_input != 0) ? cntr_vertical_input : down_pressed - up_pressed;

action_input_1 = keyboard_check(action1_key) || cntr_action_1;
action_input_1_pressed = keyboard_check_pressed(action1_key) || cntr_action_1_pressed;

action_input_2 = keyboard_check(action2_key) || cntr_action_2;
action_input_2_pressed = keyboard_check_pressed(action2_key) || cntr_action_2_pressed;

action_input_3 = keyboard_check(action3_key) || cntr_action_3;
action_input_3_pressed = keyboard_check_pressed(action3_key) || cntr_action_3_pressed;
action_input_3_released = keyboard_check_released(action3_key) || cntr_action_3_released;

action_input_4 = keyboard_check(action4_key) || cntr_action_4;
action_input_4_pressed = keyboard_check_pressed(action4_key) || cntr_action_4_pressed;
action_input_4_released = keyboard_check_released(action4_key) || cntr_action_4_released;

action_input_5 = keyboard_check(action5_key) || cntr_action_5;
action_input_5_pressed = keyboard_check_pressed(action5_key) || cntr_action_5_pressed;

action_input_6 = keyboard_check(action6_key) || cntr_action_6;
action_input_6_pressed = keyboard_check_pressed(action6_key) || cntr_action_6_pressed;

pause_input_pressed = keyboard_check_pressed(pause_key) || cntr_pause_pressed;
cancel_input_pressed = keyboard_check_pressed(cancel_key) || cntr_cancel_pressed;
confirm_input_pressed = pause_input_pressed || cntr_action_1_pressed;