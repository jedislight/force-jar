{
    "id": "3b2d18c4-becd-4747-a1df-cac11b875a86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5199977a-6b1c-4ea3-b307-adfb1c3dc4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b2d18c4-becd-4747-a1df-cac11b875a86",
            "compositeImage": {
                "id": "92f86595-1a68-49d3-b4e9-87952ee6a735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5199977a-6b1c-4ea3-b307-adfb1c3dc4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d851dc46-85b9-4055-8e7a-8c35bb2c2480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5199977a-6b1c-4ea3-b307-adfb1c3dc4b0",
                    "LayerId": "de0bb6a5-c859-462d-8d39-7bdacc396c94"
                }
            ]
        },
        {
            "id": "d20c000c-28b8-4ec4-8cda-6f7c215d9ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b2d18c4-becd-4747-a1df-cac11b875a86",
            "compositeImage": {
                "id": "58e7e657-c8d4-4bb3-9eb2-d43207e27e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d20c000c-28b8-4ec4-8cda-6f7c215d9ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c794c0a6-b79b-4e39-bd6e-f4aabf00cd79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d20c000c-28b8-4ec4-8cda-6f7c215d9ee2",
                    "LayerId": "de0bb6a5-c859-462d-8d39-7bdacc396c94"
                }
            ]
        },
        {
            "id": "a650eb16-2f93-4292-8faf-f8b97b40fa92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b2d18c4-becd-4747-a1df-cac11b875a86",
            "compositeImage": {
                "id": "6e4a8b60-3f4e-4b80-b707-a6a02ee83319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a650eb16-2f93-4292-8faf-f8b97b40fa92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00de58d0-f91c-4411-a0dd-2cace6d9d9db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a650eb16-2f93-4292-8faf-f8b97b40fa92",
                    "LayerId": "de0bb6a5-c859-462d-8d39-7bdacc396c94"
                }
            ]
        },
        {
            "id": "139124c5-210d-49f9-ab17-787d9a0c5eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b2d18c4-becd-4747-a1df-cac11b875a86",
            "compositeImage": {
                "id": "bf0cf8c6-7a4f-403b-b9cc-5e7fc6248f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "139124c5-210d-49f9-ab17-787d9a0c5eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ca98c6-85a2-44a0-b5e6-6ed631571c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "139124c5-210d-49f9-ab17-787d9a0c5eb3",
                    "LayerId": "de0bb6a5-c859-462d-8d39-7bdacc396c94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "de0bb6a5-c859-462d-8d39-7bdacc396c94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b2d18c4-becd-4747-a1df-cac11b875a86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        647115054,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}