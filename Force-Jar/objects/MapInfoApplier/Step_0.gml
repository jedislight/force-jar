/// @description Insert description here
// You can write your code in this editor

if(not variable_instance_exists(id, "frame"))
{
	frame = 0;
}

++frame;

Map.mini = false;

if(frame mod 5 == 0)
{
	if(map_info_pop_apply())
	{
		instance_destroy();
	}
}