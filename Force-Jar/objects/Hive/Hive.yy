{
    "id": "e9e4ab97-aa2d-4201-bbb9-41a48811b360",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Hive",
    "eventList": [
        {
            "id": "1b172643-3f33-4f16-b225-3d82647caa6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9e4ab97-aa2d-4201-bbb9-41a48811b360"
        },
        {
            "id": "23c26897-f01e-487b-8be4-81129ce29052",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9e4ab97-aa2d-4201-bbb9-41a48811b360"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "9d790214-7923-4c66-a0f0-6adc2bf33408",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
            "propertyId": "20aca9f3-ef56-4faf-bb9e-13400362483b",
            "value": "10"
        }
    ],
    "parentObjectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "01f11dae-b555-4864-9d4a-1f3c62e69cb4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "swarm_size",
            "varType": 1
        }
    ],
    "solid": true,
    "spriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
    "visible": true
}