{
    "id": "78a7a96c-f0b4-488a-97c1-c55dcade5dce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "FlyingGoomba",
    "eventList": [
        {
            "id": "8e3208ee-5d09-4f95-a6b4-60f59d82c4f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78a7a96c-f0b4-488a-97c1-c55dcade5dce"
        },
        {
            "id": "8bc209be-2f0d-45bc-914f-ebccdffad811",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78a7a96c-f0b4-488a-97c1-c55dcade5dce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "f581a497-5ecf-4ac7-8ce5-ef97f1979b56",
    "visible": true
}