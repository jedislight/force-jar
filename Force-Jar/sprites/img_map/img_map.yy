{
    "id": "08964883-f522-42a9-9bbe-6ead8275de49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 2,
    "bbox_right": 59,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17b782b0-5432-4b59-af9c-326e6528c06e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08964883-f522-42a9-9bbe-6ead8275de49",
            "compositeImage": {
                "id": "7c1ad285-5413-41d5-88aa-fa0e5612e100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17b782b0-5432-4b59-af9c-326e6528c06e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1c6ec54-575b-4327-bbbe-67cf622dd8d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17b782b0-5432-4b59-af9c-326e6528c06e",
                    "LayerId": "d6874467-a937-42b7-a589-059ddbf7b0f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "d6874467-a937-42b7-a589-059ddbf7b0f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08964883-f522-42a9-9bbe-6ead8275de49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}