/// @description Set Alpha

if (iframe_countdown % 3 != 0)
{
	current_alpha = 0.1;
}

draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, current_alpha);

current_alpha = 1.0;