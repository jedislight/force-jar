{
    "id": "2454cd08-ada9-498e-a9dc-278733433d2e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "assert_font",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "54c78a0c-dfb0-4837-916d-d7a8b14509d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e51d5480-c23f-4536-a7c1-c8be01286cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 163,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7a53f788-ccce-4efe-9602-7ba9a508db85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "46b4e914-97b3-4774-916e-028313181607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d3cbb000-643f-4f80-92c7-6e1d3cee1c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "05a19e6f-ba5f-46e1-bd06-da81f54098d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f2c2eb02-f458-4491-b596-dc4de56fc8c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 152,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9609e1ea-8dc2-42ab-a6eb-bfd60eadd3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 168,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f4892ebc-a06f-4ece-9d1e-ff8b0d0b3348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 5,
                "shift": 10,
                "w": 4,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "269efbd8-ca0d-42bf-99d6-ba98da449726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 3,
                "x": 158,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "580a5135-9ef9-42ce-9a80-ff93386eba83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 179,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f9223a5c-823f-4d51-bf0a-62b1d31e1224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4fe14b35-debf-4b70-911d-8263522cc67c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "380db161-0315-45cc-878b-15eb3696ed05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 22
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6b3c3670-2e0f-457f-bd41-0c9e5c1af68b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 128,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5ef40539-0154-4c7d-bc5f-b394286b01de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 197,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e3d4111a-d55d-4e66-a755-da4af46f3e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 56,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1e0717d9-9e68-48c2-8d55-6139c046867b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cfc39626-5e87-4a7f-b792-fc350818d156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d257c306-7b83-43de-9e0f-cfd2f6f1690f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 226,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "379c9133-d080-4336-986e-6bdeac78e52a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a53c75ec-40ec-40d0-b645-54f703fd033e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "256e2ce4-26d4-4143-8a69-c08c7fb8a0f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "25382b3f-6830-4a89-a27a-d78d0161e46e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 242,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b4646966-a10f-4dae-93bd-47bd3e2d102d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 233,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ab9073f7-b067-4835-9003-c517ed0d0e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 224,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "876572f7-797c-47ce-bf29-c4157309ff21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 122,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ef9703d1-f588-4c53-aa1d-514e33a10ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 140,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "46a757c5-5cd9-4442-9dd1-3cca0190ccee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 145,
                "y": 22
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "662a0f97-eb49-495f-bc13-a20f5cd7ab45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 22
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4aeabf47-8f15-454f-a634-73b4efd77ce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 42
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3b7acd6e-e5de-44e1-af2a-2ab953a0d364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6b45efda-64fd-4426-b1b3-9bce912dbc8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 206,
                "y": 42
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "05aa3fa2-39dc-4c13-a3d0-451237d03028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f338afff-2a83-49ac-a982-5532985d68ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "83f6fc35-5c19-4451-8c2a-d829444fce05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4ba09964-21bf-4c35-bce8-70b251887e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "35175b85-4ee4-4184-9e6c-9364cc611956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9717a811-5e3d-4258-993a-7b1e7d3a2353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a226a331-c94a-4b0e-a984-f560fd995629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "752f8143-094e-44e0-90cf-753cf4cf7b37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4f6855cc-19e5-43f5-b25a-9dc975445709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 215,
                "y": 42
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "afb0baa4-2860-444d-bdf0-6814ee4f3615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0019991b-a180-41be-936f-eb232a34d3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b94017d4-3930-4de1-86ec-b392d4d96260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ed3e1ce5-a053-4f81-9ede-e2c9ecefdb41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "23de1cad-f9a7-4ad1-9b81-bc54c5ff9fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "79cf1c53-b86e-43c3-a69b-47715c3bb75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 123,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ee732e31-f045-465d-94c1-8f380d413d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d93e5c7b-4ff4-4e59-8d9d-443ed1d44f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0fbe639d-b47e-42b7-9589-425af8053fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8ea4a25c-6c74-4014-9db7-554dd7064080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 42
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7ab02cc5-2727-4b37-99f0-b25d8f111d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8574bdff-0f23-4f84-9c08-2326570704eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a0799c25-3089-4502-8a5b-5ead67f90955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3623c126-be15-4ec1-96f7-43a25bf8e950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b3e293a0-bf30-434a-9c49-5362406afe97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 22
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8e25e1b0-175c-458b-af8c-5071fa0951d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 22
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f163782d-1b0f-4590-aeda-959707daa29c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4561cd64-ebc9-4071-89fe-06749159ad42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "25d354bd-0894-417c-89e8-e2a0b95f5d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "76af2333-e5cc-44bb-a100-234e0f4ad9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 152,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "afb90976-dabf-48fb-9a23-3548a11658e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 47,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5eb4f8b0-0495-4ec9-b558-33cfa21af2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f42a9ae8-19fa-48f8-84c5-f75ad81f83f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1293412c-6192-4932-9450-1aa5f1280065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 142,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a78b4dfe-b178-4a95-a4dd-69d3666cd74b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 22
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b47882fc-159e-4cdf-ba8a-b582de91e9fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1d5b375a-e0e2-448f-b5de-0f60aaa9e582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f3817b5d-8e45-4fc5-84f8-c32b1574b07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 176,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bf967b8a-9046-4137-8af5-774dc272c731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 186,
                "y": 22
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e4ecd4fc-0153-4af2-a93d-6c92a432969b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9bb8a540-6cb5-4b29-9778-6582d3be9a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c52d19df-ffed-4f40-987a-472272a7f031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 206,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d33de136-eb91-4ae2-a63f-ca1fd004b795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 74,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d5d9b696-f6a2-4502-b6e7-a37a5ccf8c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "36603c4a-da9d-477b-91ca-d4a4e637768d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 216,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "57803b72-def0-47ef-9285-c19910a636d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ee86693f-f326-4661-b450-d2d45b9a3df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "056800a3-f99b-43b5-b14b-ace6ba0422f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 236,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3099349e-e1f7-455b-a765-a97f9dca1e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e1ea4670-00b3-47c5-bc7a-ebb69caa0911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2bbada03-0919-40a2-8581-e1bf96d49829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "235220db-9220-4e4c-8b29-ba71220881b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8ff0666b-e56e-4e6a-89cd-c4ac04a7e9be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8d001eb8-cba8-40f8-8fe6-a08117099fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fa24bceb-6567-498e-8a35-21194052c495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "664ff62b-81d0-49a1-8f79-a661a0da3461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "403057f5-40d0-4bff-a8c2-de66a0f6320c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a95c86e3-ad3c-48bd-8fcd-d9854b07407f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "de4bfa3c-9c6a-4495-84da-820e6575577d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 188,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "33b34295-9afb-43aa-ac94-192d8ecb7573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0ce0963e-707b-4300-a8a1-08f11318f64d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 173,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a1a89fbf-0d21-471b-98e7-9d15ba515828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 104,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4b8ee54f-5a19-4118-9c30-19a5180d1733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 156,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}