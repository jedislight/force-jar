{
    "id": "a48ca9f1-e4cc-4c73-9dff-a34cb4d3acda",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionCameraSweep",
    "eventList": [
        {
            "id": "dc959b73-4e6c-4973-9d03-8d688450c4e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a48ca9f1-e4cc-4c73-9dff-a34cb4d3acda"
        },
        {
            "id": "f02f3c29-fe4e-4ddd-906c-b1114ecb4399",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a48ca9f1-e4cc-4c73-9dff-a34cb4d3acda"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dd1b089e-db02-48ac-9809-3db0559cd464",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}