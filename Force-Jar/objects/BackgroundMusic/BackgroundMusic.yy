{
    "id": "5d9435ea-1393-4015-829f-b7f954bd286b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "BackgroundMusic",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "b1dd1ae3-2665-4521-ae39-31791463f51d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 4,
            "value": "mus_x",
            "varName": "music",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "d7bc0c5b-dd34-4f3c-98e1-dfcc18f61dc8",
    "visible": false
}