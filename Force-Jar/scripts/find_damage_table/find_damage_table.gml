/// @function find_damage_table(damage_table_key)
/// @desc Finds or Creates global damage table, returns TRUE on found.
/// @param damage_table_key

var damage_table_key = argument0;

if (ds_map_exists(global.damage_data_map, damage_table_key))
{
	return true;	
}

global.damage_data_map[? damage_table_key] = ds_map_create();
return false;