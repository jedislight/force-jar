var value = argument0
var message = argument1
if(not value)
{
	assert(value, message);
	var a = instance_create_depth(0,0,0, AssertObject);
	a.text += "Game: " + game_display_name + "\n";
	a.text += "Game ID: " + string(game_id) + "\n";
	a.text += "Game Project: " + game_project_name + "\n";
	a.text += "Save ID: " + string(game_save_id) + "\n";
	a.text += "Room: " +room_get_name(room) + "\n";
	a.text += "Object: " +object_get_name(object_index) + "\n";
	a.text += "Instance ID: " +string(id) + "\n";
	a.text += "Debug Mode: " +string(debug_mode) + "\n";
	a.text += "Program Directory: " +program_directory + "\n";
	a.text += "Runtime (ms): " +string(current_time) + "\n";
	a.text += "Time : "+ date_datetime_string(date_current_datetime()) + "\n";
	var player = instance_find(PlayerStates, 0);
	if(player)
	{
		a.text += "Player: " + string(player.id) + "\n";
		a.text += "Player.x: " + string(player.x) + "\n";
		a.text += "Player.y: " + string(player.y) + "\n";
	}
	else
	{
		a.text += "Player: noone\n";
	}
	a.text += "Callstack:\n";
	var _a = debug_get_callstack();
	for (var i = 0; i < array_length_1d(_a); ++i)
	{
		a.text += " " + string(_a[i]) + "\n";
	}
	a.text += "Assertion Message:\n";
	a.text += message;
	exit;
}