{
    "id": "bbd5714f-3375-4e21-b5d0-86e8e2c91a2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_cave_0003_color",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3999,
    "bbox_left": 0,
    "bbox_right": 3999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d2f6263-5fa1-4abe-abb9-da6d8ba73c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbd5714f-3375-4e21-b5d0-86e8e2c91a2f",
            "compositeImage": {
                "id": "b37a393b-5f90-42d0-b247-4ecbdc8e4710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2f6263-5fa1-4abe-abb9-da6d8ba73c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823cdeec-719f-40ef-ac24-5d1f55a15e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2f6263-5fa1-4abe-abb9-da6d8ba73c72",
                    "LayerId": "4739734c-8e63-46ae-b690-1e78d3c9b637"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4000,
    "layers": [
        {
            "id": "4739734c-8e63-46ae-b690-1e78d3c9b637",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbd5714f-3375-4e21-b5d0-86e8e2c91a2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4000,
    "xorig": 0,
    "yorig": 0
}