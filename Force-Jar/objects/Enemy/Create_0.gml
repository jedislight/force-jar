/// @description Insert description here
// You can write your code in this editor

locked = false;
heavy = false;
pull = noone;

if (!instance_has_health(id))
{
	health_create(initial_health);
}

if (!has_drops(id))
{
	drops_create(drop_table_key, drop_table_amt);
}

damage_data_map = noone;
damage_map_create = false; // used if an object needs to define it's damage table

// If an object isn't using default enemy key, this value can be used by a child object to decide to created a table for the first time.
damage_table_created = !find_damage_table(damage_data_key);

damage_object_ignore = ds_map_create();

faction = Factions.ENEMIES

flash_once = false;