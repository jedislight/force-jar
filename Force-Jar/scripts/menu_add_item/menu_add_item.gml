var menu_control_target = argument0;
var menu_item_target = argument1;

with (menu_control_target)
{
	ds_list_ensure(menu_item_list, menu_item_target);	
}