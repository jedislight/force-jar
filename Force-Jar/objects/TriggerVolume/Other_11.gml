/// @description Exit

if(script_exists(on_exit))
{
	script_execute(on_exit);
}