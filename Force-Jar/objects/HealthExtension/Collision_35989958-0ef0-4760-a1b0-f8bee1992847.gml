/// @description Advance Health

assert(is_undefined(other.extension_map[? extension_name]), "Extension already collected, should have shut down");
assert_exit

other.extension_map[? extension_name] = true;

health_set_max_health(other, other.health_max + 100);
health_change(other, other.health_max);

instance_destroy();