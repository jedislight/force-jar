{
    "id": "d3e1e231-2562-4619-871d-544e394bd790",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PlayerInvincibleFrames",
    "eventList": [
        {
            "id": "19779859-0a94-41f9-ba7b-c38cf2140478",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d3e1e231-2562-4619-871d-544e394bd790"
        },
        {
            "id": "576205f8-cd86-4039-8218-328218251d8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3e1e231-2562-4619-871d-544e394bd790"
        },
        {
            "id": "b6919073-9498-4f20-8e9a-25fcab42c772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d3e1e231-2562-4619-871d-544e394bd790"
        },
        {
            "id": "c5fe24e1-1863-4d1b-9cff-f93043e2cb56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d3e1e231-2562-4619-871d-544e394bd790"
        },
        {
            "id": "7640955a-fc2b-4d1f-8c47-cf01fe5b1d22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d3e1e231-2562-4619-871d-544e394bd790"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fce370f7-a038-4188-bfcb-126b004a9dfc",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
    "visible": true
}