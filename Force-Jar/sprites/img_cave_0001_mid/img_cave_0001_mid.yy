{
    "id": "ea3fe35d-975d-4d26-adbe-17c30dcb3ecf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_cave_0001_mid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3999,
    "bbox_left": 0,
    "bbox_right": 3999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "747aa32e-1e41-4a65-8c12-544561f4873f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea3fe35d-975d-4d26-adbe-17c30dcb3ecf",
            "compositeImage": {
                "id": "5aed216a-bcf7-4470-a58d-341170f5fb1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747aa32e-1e41-4a65-8c12-544561f4873f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f58fa99-b7ef-424b-9396-a6915c2c51f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747aa32e-1e41-4a65-8c12-544561f4873f",
                    "LayerId": "3ef71d3e-4549-4824-8312-8663a73f6ddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4000,
    "layers": [
        {
            "id": "3ef71d3e-4549-4824-8312-8663a73f6ddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea3fe35d-975d-4d26-adbe-17c30dcb3ecf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4000,
    "xorig": 0,
    "yorig": 0
}