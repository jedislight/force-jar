{
    "id": "92da8943-8904-4472-a1fa-650948749444",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_unknown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b7e99c6-5e27-40ff-9ba1-06d6f70be7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92da8943-8904-4472-a1fa-650948749444",
            "compositeImage": {
                "id": "c8a70028-6e98-4fdf-9c34-112ae5890a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b7e99c6-5e27-40ff-9ba1-06d6f70be7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508668e6-1875-4a11-9116-259936dba233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b7e99c6-5e27-40ff-9ba1-06d6f70be7e8",
                    "LayerId": "c96dc907-a1a2-405e-b5a2-9e64678bf7cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c96dc907-a1a2-405e-b5a2-9e64678bf7cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92da8943-8904-4472-a1fa-650948749444",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}