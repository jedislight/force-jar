{
    "id": "615fc1c2-5b2e-48d1-a166-55cc48b73272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_crumble_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e7f0e57-d2b5-4a53-a760-6ece3654d89e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615fc1c2-5b2e-48d1-a166-55cc48b73272",
            "compositeImage": {
                "id": "3d808ca1-8864-42e2-bb70-e056c54b812e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e7f0e57-d2b5-4a53-a760-6ece3654d89e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47af32e6-b683-4e64-b488-02164d8212b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e7f0e57-d2b5-4a53-a760-6ece3654d89e",
                    "LayerId": "a88f9f08-8ebd-4b3e-bb54-54b152c77def"
                },
                {
                    "id": "cad44728-e4d1-49fd-9c6b-fc54368215b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e7f0e57-d2b5-4a53-a760-6ece3654d89e",
                    "LayerId": "ce384ca6-e91f-4623-9f63-cf6ddeb3b873"
                }
            ]
        },
        {
            "id": "5409b5e9-a61d-487a-a667-45fc0784cc14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615fc1c2-5b2e-48d1-a166-55cc48b73272",
            "compositeImage": {
                "id": "695145bd-1761-4ed1-bd3b-bfd32f66b311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5409b5e9-a61d-487a-a667-45fc0784cc14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9dd867-895b-46b2-81e6-f7472197981f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5409b5e9-a61d-487a-a667-45fc0784cc14",
                    "LayerId": "a88f9f08-8ebd-4b3e-bb54-54b152c77def"
                },
                {
                    "id": "f44fb25b-be6a-4ccf-bde3-70ab43be96de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5409b5e9-a61d-487a-a667-45fc0784cc14",
                    "LayerId": "ce384ca6-e91f-4623-9f63-cf6ddeb3b873"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a88f9f08-8ebd-4b3e-bb54-54b152c77def",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615fc1c2-5b2e-48d1-a166-55cc48b73272",
            "blendMode": 3,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ce384ca6-e91f-4623-9f63-cf6ddeb3b873",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615fc1c2-5b2e-48d1-a166-55cc48b73272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}