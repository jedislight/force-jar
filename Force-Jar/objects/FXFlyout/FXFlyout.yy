{
    "id": "437f6f38-85d0-47d7-bdae-9d98e011209e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "FXFlyout",
    "eventList": [
        {
            "id": "c26ee236-1598-413c-913a-fcfc8918c264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "437f6f38-85d0-47d7-bdae-9d98e011209e"
        },
        {
            "id": "265ee8be-fc46-4c7d-a563-89fedcbfca85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "437f6f38-85d0-47d7-bdae-9d98e011209e"
        },
        {
            "id": "38f76fd1-9c07-44ae-8b57-5f4e0c804913",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "437f6f38-85d0-47d7-bdae-9d98e011209e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}