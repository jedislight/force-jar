{
    "id": "82bb69f1-2702-415a-a0fd-7ac59e614421",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "HealthExtension",
    "eventList": [
        {
            "id": "35989958-0ef0-4760-a1b0-f8bee1992847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "380edad1-487c-4192-bac0-65401ddf29b8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "82bb69f1-2702-415a-a0fd-7ac59e614421"
        },
        {
            "id": "4f6c7823-62b4-4cea-adf8-d047019e67f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "82bb69f1-2702-415a-a0fd-7ac59e614421"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "15319ed1-3e1b-49b9-be8f-162e5504ead5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "override_this_value",
            "varName": "extension_name",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "90c2f66c-c1d3-45e4-ad53-2ceda624e1ea",
    "visible": true
}