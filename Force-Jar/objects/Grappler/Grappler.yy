{
    "id": "40555686-03c6-4ec5-9019-92354d88d370",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Grappler",
    "eventList": [
        {
            "id": "38868fb5-c6f6-4468-b60c-0a568c7a1e57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40555686-03c6-4ec5-9019-92354d88d370"
        },
        {
            "id": "79e7cd1f-550b-4b3f-b35b-e4e1eadf2b54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "40555686-03c6-4ec5-9019-92354d88d370"
        },
        {
            "id": "559664a9-2506-4bef-8ee2-ddf96eaa70c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "40555686-03c6-4ec5-9019-92354d88d370"
        },
        {
            "id": "da6af9ca-5cf6-4de0-b01f-3d8dccf8016c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "40555686-03c6-4ec5-9019-92354d88d370"
        },
        {
            "id": "7acb50d1-d4cd-48a5-bfef-60abece9cf78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "40555686-03c6-4ec5-9019-92354d88d370"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "818a0f22-fc15-447b-adb6-1977adddf408",
    "visible": true
}