breakable_damage_key = "Breakable";
breakable_damage_map = noone;

if (!variable_instance_exists(id, "respawning"))
{
	respawning = false;
}

if (!respawning)
{
	image_speed = 0;
}
else
{
	solid = false;
	image_index = image_number - 1;
	image_speed = -1;
	respawning = false;
}

if (!find_damage_table(breakable_damage_key))
{
	breakable_damage_map = global.damage_data_map[? breakable_damage_key];
	breakable_damage_map[? DM_FACTION] = Factions.TERRAIN;
}

tilemap_set_visible_behind_tile();