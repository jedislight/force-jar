/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(not locked)
{
	++frames_since_last_turn;
	var dx = velocity * sign(image_xscale)

	if(frames_since_last_turn > minimum_frame_to_turn)
	{
		var collision = apply_movement(dx,0);
		if(collision)
		{
			image_xscale *= -1;
			frames_since_last_turn = 0;
		}
	}
}
else
{
	if(instance_number(Grappler) == 0)
	{
		locked = false;
	}
}