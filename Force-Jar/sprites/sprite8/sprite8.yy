{
    "id": "0aacbf14-d271-40d8-8800-94b2efe1737e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "051e55d7-b59d-481b-9db3-63445dc98192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aacbf14-d271-40d8-8800-94b2efe1737e",
            "compositeImage": {
                "id": "1ea62c2c-7ce3-456d-a397-4f43fb2c0fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "051e55d7-b59d-481b-9db3-63445dc98192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45ab4b59-29d2-48d6-831c-c0caa64136e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "051e55d7-b59d-481b-9db3-63445dc98192",
                    "LayerId": "f21815c9-96f1-42f3-b1da-d0aad0058652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "f21815c9-96f1-42f3-b1da-d0aad0058652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aacbf14-d271-40d8-8800-94b2efe1737e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 0
}