{
    "id": "c47633f4-bd22-49b2-9853-af12fcca1944",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "BreakableTerrain",
    "eventList": [
        {
            "id": "541d463b-8874-4713-84ed-7a7d146a0957",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c47633f4-bd22-49b2-9853-af12fcca1944"
        },
        {
            "id": "08e87023-1d57-4349-97cd-28f7ff5c8f6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c47633f4-bd22-49b2-9853-af12fcca1944"
        },
        {
            "id": "2cf4682a-a5e0-4c30-b619-8e5156e7902e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c47633f4-bd22-49b2-9853-af12fcca1944"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a1e55b03-e373-4293-be98-a1325d58bb2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "f1197402-fec6-4541-a3fc-7d6568e8e4b2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "300",
            "varName": "respawn_timeout",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
    "visible": true
}