{
    "id": "6187398b-462f-412e-84e4-7b500f621f67",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "WeaponDataBase",
    "eventList": [
        {
            "id": "4685d968-76ac-4dc5-a4ef-e51aa08047ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6187398b-462f-412e-84e4-7b500f621f67"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "8d8e758a-cdfa-41ef-b0ca-27d0a13a833a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 16,
            "value": "single_attack_on_press",
            "varName": "on_attack",
            "varType": 5
        },
        {
            "id": "9fb5d4cf-6c99-4d37-92f3-b8ba0c2e7774",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 256,
            "value": "noone",
            "varName": "damage_object_data",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "92da8943-8904-4472-a1fa-650948749444",
    "visible": false
}