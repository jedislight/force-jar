/// @description Move to next room

if (other.object_index == PlayerTransitionState)
{
	// Short circuit
	return;	
}

if (transition_start != noone)
{
	return;	
}

room_side = image_angle;

player_instance = other;

jump_offset = player_instance.y - player_instance.jump_start;

transition_start = instance_create_depth(0, 0, layer_get_depth("Instances") - 50, TransitionStart);

transition_start.target_room = target_room;
transition_start.room_side = room_side;
transition_start.door_offset = door_offset;
transition_start.jump_offset = jump_offset;

with (transition_start)
{
	
	ds_queue_enqueue(transition_queue, TransitionPlayerStateToggle);
	ds_queue_enqueue(transition_queue, TransitionFadeOut);
	ds_queue_enqueue(transition_queue, TransitionGotoRoom);
	ds_queue_enqueue(transition_queue, TransitionCameraSweep);
	ds_queue_enqueue(transition_queue, TransitionFadeIn);
	ds_queue_enqueue(transition_queue, TransitionPlayerStateToggle);
	
	// Call this to start the transition.
	event_user(0);
}