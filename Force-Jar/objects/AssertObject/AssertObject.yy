{
    "id": "3d2bee12-3e26-41ad-a075-7dfd143a97db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AssertObject",
    "eventList": [
        {
            "id": "13d350d2-0726-4865-ad10-8a82d0162845",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3d2bee12-3e26-41ad-a075-7dfd143a97db"
        },
        {
            "id": "d48da02e-b580-4954-a18d-4ffcc6fe8a25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d2bee12-3e26-41ad-a075-7dfd143a97db"
        },
        {
            "id": "96fe85b0-fb22-4056-aa0b-2c124e77169b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d2bee12-3e26-41ad-a075-7dfd143a97db"
        },
        {
            "id": "7d9d4cad-7553-4ee5-94c9-ca91ef8cdac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "3d2bee12-3e26-41ad-a075-7dfd143a97db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}