{
    "id": "fce370f7-a038-4188-bfcb-126b004a9dfc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PlayerPlay",
    "eventList": [
        {
            "id": "57fa7665-7049-4b14-b9e6-89ab0225d9c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "fce370f7-a038-4188-bfcb-126b004a9dfc"
        },
        {
            "id": "07da3d15-e372-4cfe-b6ef-c29933e28f0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fce370f7-a038-4188-bfcb-126b004a9dfc"
        },
        {
            "id": "4f68ca05-f6ac-46f2-a1a0-c9d1f36708ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fce370f7-a038-4188-bfcb-126b004a9dfc"
        },
        {
            "id": "9e4b8b56-ecd6-427a-9325-5b2e431720ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fce370f7-a038-4188-bfcb-126b004a9dfc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "380edad1-487c-4192-bac0-65401ddf29b8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
    "visible": true
}