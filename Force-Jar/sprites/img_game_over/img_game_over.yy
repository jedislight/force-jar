{
    "id": "a92cec87-97d3-4789-a340-282ed40b28a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_game_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 166,
    "bbox_left": 16,
    "bbox_right": 285,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "794a6c61-1c1f-4eaa-858c-6181d559cb59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a92cec87-97d3-4789-a340-282ed40b28a2",
            "compositeImage": {
                "id": "dc0d9af5-c26b-4c8f-8a3d-12f897a5678a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "794a6c61-1c1f-4eaa-858c-6181d559cb59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb9ca407-c22c-45e9-866a-54799cf9b8d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "794a6c61-1c1f-4eaa-858c-6181d559cb59",
                    "LayerId": "7fcdef1a-7675-41b4-a259-98aee9a746e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "7fcdef1a-7675-41b4-a259-98aee9a746e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a92cec87-97d3-4789-a340-282ed40b28a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}