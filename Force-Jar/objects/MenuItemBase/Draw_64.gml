/// @description Draw the Menu Item Display Name

draw_set_font(display_font);
draw_set_color((item_selected) ? item_selected_color : item_idle_color);
draw_set_valign(fa_middle);

draw_text((x / room_width) * display_get_width(), (y / room_height) * display_get_height(), display_name);

draw_set_valign(fa_top);
draw_set_color(c_black);
draw_set_font(-1);