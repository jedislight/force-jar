/// @function instance_has_health(instance)
/// @desc Checks if an instance has HealthData available
/// @param {Real} instance the id of the instance to check.

var instance = argument0;

return variable_instance_exists(instance, "health_dirty");