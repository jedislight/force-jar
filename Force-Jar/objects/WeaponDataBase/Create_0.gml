/// @description Define Owner
// Call event_inherited() if you intend to overwrite create.

owner = noone;
// Offset data in case the attack should not come from the origin for the object.
xoffset = 0;
yoffset = 0;
draw_idle = true;

anchor_offset_x = 0;
anchor_offset_y = 0;
anchor_angle = -45;

charge = 0
charge_max = 1.0;