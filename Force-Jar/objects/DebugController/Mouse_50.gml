/// @description Teleport to cursor

if(keyboard_check(vk_control))
{
	var player = instance_find(PlayerStates,0);
	if(instance_exists(player))
	{
		with(player)
		{
			if(place_free(mouse_x, mouse_y))
			{
				player.x = mouse_x;
				player.y = mouse_y;
			}
		}
	}
}
