{
    "id": "b9daf845-8f89-4e70-8b15-c20d026ea2c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ApplicationRenderer",
    "eventList": [
        {
            "id": "e8255027-583d-4f63-857b-910b069c19fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9daf845-8f89-4e70-8b15-c20d026ea2c9"
        },
        {
            "id": "40d1cac0-a21f-4ba2-9c2c-2eacf9768848",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "b9daf845-8f89-4e70-8b15-c20d026ea2c9"
        },
        {
            "id": "a6cb66bc-7785-4698-bdd4-e6f62f571a32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b9daf845-8f89-4e70-8b15-c20d026ea2c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}