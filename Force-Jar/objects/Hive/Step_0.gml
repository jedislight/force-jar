/// @description Insert description here
// You can write your code in this editor
event_inherited();
++frame;
solid = true;

var spawns_needed = swarm_size - instance_number(Swarmer);

if(spawns_needed > 0 && frame % 10 == 0)
{
	instance_create_depth(x+92, y, depth, Swarmer)
	instance_create_depth(x-92, y, depth, Swarmer)
}