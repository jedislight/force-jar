{
    "id": "4b337b87-c1e2-4141-9930-cfdc443b15fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rapier_sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 19,
    "bbox_right": 124,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a45b6c0-0770-4e94-82eb-739ed362a136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b337b87-c1e2-4141-9930-cfdc443b15fb",
            "compositeImage": {
                "id": "1cc9d303-497a-4844-b30c-fd1c6a430ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a45b6c0-0770-4e94-82eb-739ed362a136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43605584-a0c5-4800-9379-1c46e9e58fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a45b6c0-0770-4e94-82eb-739ed362a136",
                    "LayerId": "91c47790-398e-4ffc-8c5e-3ebf859282ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "91c47790-398e-4ffc-8c5e-3ebf859282ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b337b87-c1e2-4141-9930-cfdc443b15fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 25,
    "yorig": 19
}