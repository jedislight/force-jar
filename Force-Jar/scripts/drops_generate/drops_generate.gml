/// @func drops_generate(facetted_instance)
/// @desc Generates a drop at location the instance died if it had a drops facet.
/// @param {Real} facetted_instance the id of the drops facet holder

var facetted_instance = argument0;

assert(has_drops(facetted_instance), "Cannot drop items, no drops facet on " + string(facetted_instance));
assert_exit

// For each potential drop, attempt a generation check.
for (var current_drop = DropTableKeys.FIRST; current_drop < facet_drops_amount; ++current_drop)
{
	for (var current_drop_type = DropTableOptions.FIRST; current_drop_type < DropTableOptions.END; ++current_drop_type)
	{
		// Raw generation for now.
		// TODO: Add scaling random numbers given player resources.
		if (random(100) <= global.drop_tables[# facet_drops_key, current_drop_type])
		{
			instance_create_layer(facetted_instance.x, facetted_instance.y, "Instances", global.drop_objects[| current_drop_type]);
			break;
		}
	}
}