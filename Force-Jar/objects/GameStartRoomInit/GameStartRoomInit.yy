{
    "id": "3322e906-3752-4f4a-89d1-ff6011d1be32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GameStartRoomInit",
    "eventList": [
        {
            "id": "ada6c6cc-4390-4e0b-9848-7b5d59d3856c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "3322e906-3752-4f4a-89d1-ff6011d1be32"
        },
        {
            "id": "f8c8bc8f-8274-4462-8a91-c7fe118d925f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3322e906-3752-4f4a-89d1-ff6011d1be32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bf4f98dd-c42e-45d3-a7cf-ced722ebe4ef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8214a1f-1387-4c06-aded-003268fdd646",
    "visible": true
}