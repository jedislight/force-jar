{
    "id": "56d47bb0-ffff-4e6a-9a30-2674b5c54fd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_levitate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 31,
    "bbox_right": 32,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ed16172-c9f4-4983-b234-3327a3ddca25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56d47bb0-ffff-4e6a-9a30-2674b5c54fd8",
            "compositeImage": {
                "id": "56646dfb-3050-4a64-81b1-9d30aa437c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ed16172-c9f4-4983-b234-3327a3ddca25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6421d05c-9892-4787-8542-b2d2d6042e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ed16172-c9f4-4983-b234-3327a3ddca25",
                    "LayerId": "f4b3f916-bc94-4adb-92ab-96cac7299818"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f4b3f916-bc94-4adb-92ab-96cac7299818",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56d47bb0-ffff-4e6a-9a30-2674b5c54fd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}