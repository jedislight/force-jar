/// @description Insert description here
// You can write your code in this editor

switch (async_load[? "event_type"])
{
	case "gamepad discovered":
		show_debug_message("Gamepad discovered in slot " + string(async_load[? "pad_index"]) + ".  Gamepad is " + gamepad_get_description(async_load[? "pad_index"]));
		if (target_gamepad == -1)
		{
			show_debug_message("Gamepad discovered, assigning");
			target_gamepad = async_load[? "pad_index"];	
			gamepad_set_axis_deadzone(target_gamepad, 0.5);
			gamepad_set_button_threshold(target_gamepad, 0.1);
		}
		break;
	case "gamepad lost":
		if (target_gamepad == async_load[? "pad_index"])
		{
			show_debug_message("Gamepad lost");
			target_gamepad = -1;	
		}
		break;
}