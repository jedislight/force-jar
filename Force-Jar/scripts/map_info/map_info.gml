/// @function map_info(x, y, borders, doors)
/// @desc sets a MapInfoPickups room information
/// @param {Real} x map x cordinate
/// @param {Real} y map y cordinate
/// @param {Real} borders map border flags
/// @param {Real} map door flags

var xx = argument0;
var yy = argument1;
var borders = argument2;
var doors = argument3;

ds_list_add(info, xx, yy, borders, doors);