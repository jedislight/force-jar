// Find all game objects defined in game_init and delete them.
instance_destroy(instance_find(CameraControl, 0));

instance_destroy(instance_find(UiMap, 0));
instance_destroy(instance_find(UiMiniMap, 0));
instance_destroy(instance_find(Map, 0));
instance_destroy(instance_find(UiPlayerHealth, 0));
instance_destroy(instance_find(DebugController, 0));

var menu_control = instance_create_layer(0, 0, "Instances", MenuControl);

menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "RESTART FROM CHECKPOINT"));
menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "RETURN TO MENU"));
menu_add_item(menu_control, ds_map_find_value(global.active_menu_items, "EXIT TO DESKTOP"));

menu_init(menu_control);
