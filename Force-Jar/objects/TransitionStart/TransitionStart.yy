{
    "id": "dd1b089e-db02-48ac-9809-3db0559cd464",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionStart",
    "eventList": [
        {
            "id": "dd0c760d-163e-4d6c-84a7-8030c736a357",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "dd1b089e-db02-48ac-9809-3db0559cd464"
        },
        {
            "id": "d731039b-ec33-45e4-ad50-46fc71501345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd1b089e-db02-48ac-9809-3db0559cd464"
        },
        {
            "id": "7748eaec-bfd0-4df6-bafe-152cc7e7239d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "dd1b089e-db02-48ac-9809-3db0559cd464"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "e1efcf1b-5279-4617-8c36-e12ff816d616",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "201d114f-1383-4886-9290-61266cbf78b0",
            "propertyId": "73d7c9b3-cf07-4c43-9166-ff5355980d55",
            "value": "DrawLayer.TRANSITION_EFFECT"
        }
    ],
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}