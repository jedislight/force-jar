{
    "id": "5dbd5a74-2a97-4298-9ddc-f83cc2d28608",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "362f6613-2245-47ec-934b-368c8229a74d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dbd5a74-2a97-4298-9ddc-f83cc2d28608",
            "compositeImage": {
                "id": "ffd039e0-8ac2-4bc9-920b-676a2b564256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362f6613-2245-47ec-934b-368c8229a74d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a677d15-5e07-49c1-aaf5-22336155027a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362f6613-2245-47ec-934b-368c8229a74d",
                    "LayerId": "314e3b90-c5c7-4ef8-9456-e8cd0eee869c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "314e3b90-c5c7-4ef8-9456-e8cd0eee869c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5dbd5a74-2a97-4298-9ddc-f83cc2d28608",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}