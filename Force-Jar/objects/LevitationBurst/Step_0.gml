/// @description Insert description here
// You can write your code in this editor
if(kill)
{
	instance_destroy();
	exit;
}

radius += velocity;
radius = min(radius, end_radius);
velocity += acceleration;

with(LevBlock)
{
	if(collision_circle(other.x,other.y,other.radius, id, false,false) == id)
	{
		levitating = true;
		image_blend = c_aqua;
	}
}

if(radius == end_radius)
{
	kill = true;
}

color = merge_color(color, c_black, radius / end_radius)