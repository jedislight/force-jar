{
    "id": "35928798-b217-4f00-abd5-476266961574",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite81",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7bd4b03-f69b-4e08-bffb-c98568e7f9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35928798-b217-4f00-abd5-476266961574",
            "compositeImage": {
                "id": "5fe7d94d-0fa0-4991-8177-30d5e675c1db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7bd4b03-f69b-4e08-bffb-c98568e7f9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54861fbc-77fd-4a34-8333-13136fc32af4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7bd4b03-f69b-4e08-bffb-c98568e7f9b8",
                    "LayerId": "412d713f-5df8-42f9-922c-e1ffe7cd98e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "412d713f-5df8-42f9-922c-e1ffe7cd98e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35928798-b217-4f00-abd5-476266961574",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 0
}