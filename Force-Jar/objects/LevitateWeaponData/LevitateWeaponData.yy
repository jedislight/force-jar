{
    "id": "3946c157-5e4a-4e7a-ad56-d9d343bbcc2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "LevitateWeaponData",
    "eventList": [
        {
            "id": "4a17d13b-59ed-478e-b77d-5f76bb6205a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3946c157-5e4a-4e7a-ad56-d9d343bbcc2c"
        },
        {
            "id": "fd47c82b-12b8-416f-a8df-96be23436d09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3946c157-5e4a-4e7a-ad56-d9d343bbcc2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "9f1071cb-739e-4ce8-b735-e83b0920264e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6187398b-462f-412e-84e4-7b500f621f67",
            "propertyId": "9fb5d4cf-6c99-4d37-92f3-b8ba0c2e7774",
            "value": "noone"
        },
        {
            "id": "5848afdd-79ab-4e43-a575-7282105dc1a4",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6187398b-462f-412e-84e4-7b500f621f67",
            "propertyId": "8d8e758a-cdfa-41ef-b0ca-27d0a13a833a",
            "value": "mass_levitate_on_press"
        }
    ],
    "parentObjectId": "6187398b-462f-412e-84e4-7b500f621f67",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56d47bb0-ffff-4e6a-9a30-2674b5c54fd8",
    "visible": false
}