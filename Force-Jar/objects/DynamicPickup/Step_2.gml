/// @description push out of walls to player or center
if(!place_free(x,y))
{
	var move_to_x = room_width/2;
	var move_to_y = room_height/2;
	
	var player = instance_find(PlayerStates, 0);
	if(instance_exists(player))
	{
		move_to_x = player.x;
		move_to_y = player.y;
	}
	
	var dir = point_direction(x,y, move_to_x, move_to_y);
	
	var dx = lengthdir_x(push_speed, dir);
	var dy = lengthdir_y(push_speed, dir);
	
	x += dx;
	y += dy;
	
	ttl += 1; // don't count pickup time against player when they can't get it
}