{
    "id": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_staff_twirl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 190,
    "bbox_left": 0,
    "bbox_right": 190,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ebcbbbf-9b1d-4f49-8bda-a24bd3663979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
            "compositeImage": {
                "id": "9a77375b-23b3-45c9-87aa-91c0341f0162",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ebcbbbf-9b1d-4f49-8bda-a24bd3663979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a5c448a-1df5-4ca4-a376-42fb16a8d271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ebcbbbf-9b1d-4f49-8bda-a24bd3663979",
                    "LayerId": "49b3b199-7367-4efd-8e0b-080b8d1e5b3c"
                }
            ]
        },
        {
            "id": "9b905b6e-87c1-4605-9f15-ba9fc3d62553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
            "compositeImage": {
                "id": "d99a3306-d5cb-44f4-b7aa-e6a487bda7fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b905b6e-87c1-4605-9f15-ba9fc3d62553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14b7676d-66c7-47d8-b4cf-3c2f7dc4aea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b905b6e-87c1-4605-9f15-ba9fc3d62553",
                    "LayerId": "49b3b199-7367-4efd-8e0b-080b8d1e5b3c"
                }
            ]
        },
        {
            "id": "bb9c8602-5ee2-47ab-89cf-625281bfde0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
            "compositeImage": {
                "id": "2133d826-4cf9-405a-a826-1cb050596968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9c8602-5ee2-47ab-89cf-625281bfde0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3e02fa-cfc0-4d8a-a86b-745220dd54e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9c8602-5ee2-47ab-89cf-625281bfde0b",
                    "LayerId": "49b3b199-7367-4efd-8e0b-080b8d1e5b3c"
                }
            ]
        },
        {
            "id": "c01b5b80-b090-48db-b3a8-b06ef0e736ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
            "compositeImage": {
                "id": "76445289-f804-4f3e-a5a5-8b7bd0ba64a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01b5b80-b090-48db-b3a8-b06ef0e736ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a355c43-729f-49d4-92e6-d131f6a38bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01b5b80-b090-48db-b3a8-b06ef0e736ee",
                    "LayerId": "49b3b199-7367-4efd-8e0b-080b8d1e5b3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "49b3b199-7367-4efd-8e0b-080b8d1e5b3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        3103784959,
        1475144940,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4281534720,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}