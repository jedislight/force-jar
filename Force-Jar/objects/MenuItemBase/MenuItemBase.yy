{
    "id": "ae82b777-fbe4-48cf-9766-8118f3f75409",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MenuItemBase",
    "eventList": [
        {
            "id": "327890e1-d547-4c15-8437-3a7dfc879f0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ae82b777-fbe4-48cf-9766-8118f3f75409"
        },
        {
            "id": "49ef8da4-26cc-4a22-99dc-70a824665eda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ae82b777-fbe4-48cf-9766-8118f3f75409"
        },
        {
            "id": "d677c0ff-f475-4539-88c8-b07beb2b891d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae82b777-fbe4-48cf-9766-8118f3f75409"
        },
        {
            "id": "42d32d2f-0d7f-400b-b3c5-5097c6dc4513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ae82b777-fbe4-48cf-9766-8118f3f75409"
        },
        {
            "id": "800843db-30a1-4de6-b892-16036c27c7f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ae82b777-fbe4-48cf-9766-8118f3f75409"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "5f7935d0-6f95-44dc-a4a3-09662c20db83",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "<default>",
            "varName": "display_name",
            "varType": 2
        },
        {
            "id": "b76de70a-38f8-436b-8fdf-c452bd42294e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 64,
            "value": "ui_font",
            "varName": "display_font",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}