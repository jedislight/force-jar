/// @description Insert description here
// You can write your code in this editor
event_inherited()
if(not locked)
{
	++frames_since_last_turn;
	var dx = velocity * sign(image_xscale)
	var dy = 32 // gravity

	if(frames_since_last_turn > minimum_frame_to_turn)
	{
		if(place_free(x+dx+sprite_width,y+dy+16))
		{
			image_xscale *= -1;
			dx *= -1;
			frames_since_last_turn = 0;
		}

		var collision = apply_movement(dx,0);
		if(collision)
		{
			image_xscale *= -1;
			frames_since_last_turn = 0;
		}
	}

	apply_movement(0, dy);
}
else
{
	if(instance_number(Grappler) == 0)
	{
		locked = false;
	}
}