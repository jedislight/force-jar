/// @description draw application surface

var camera = camera_get_active();
var xscale = camera_get_view_width(camera) / surface_get_width(application_surface);
var yscale = camera_get_view_height(camera) / surface_get_height(application_surface);

assert_fatal(surface_exists(application_surface), "NO APPLICATION SURFACE!");


shader_set(post_effect_shaders[| post_effect_shader_index]);
{
	draw_surface(application_surface,0,0);
} shader_reset();