{
    "id": "000c5d4a-55ed-4409-bb3e-84a97bdd44db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "InputHandler",
    "eventList": [
        {
            "id": "47c1e5f1-f782-486a-9c28-409f2ae0f5ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "000c5d4a-55ed-4409-bb3e-84a97bdd44db"
        },
        {
            "id": "68a880ce-bcb1-4fdf-ac65-5a74ca06250d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "000c5d4a-55ed-4409-bb3e-84a97bdd44db"
        },
        {
            "id": "6d17c050-fe64-4c35-8fe5-5a24de11a550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 7,
            "m_owner": "000c5d4a-55ed-4409-bb3e-84a97bdd44db"
        },
        {
            "id": "cea6c7d4-e195-443a-b4d8-23b60cf0d0c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "000c5d4a-55ed-4409-bb3e-84a97bdd44db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}