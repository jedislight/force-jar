{
    "id": "5d7a5418-a48f-4361-8790-b2c0e8e82521",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RapierWeaponData",
    "eventList": [
        {
            "id": "d0e524b8-732c-4369-9643-111635cf65af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "5d7a5418-a48f-4361-8790-b2c0e8e82521"
        },
        {
            "id": "b1037302-67d8-414f-86da-b9073d98d0d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5d7a5418-a48f-4361-8790-b2c0e8e82521"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "de7d5a0d-d13a-4857-8f95-faab2029ac68",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6187398b-462f-412e-84e4-7b500f621f67",
            "propertyId": "9fb5d4cf-6c99-4d37-92f3-b8ba0c2e7774",
            "value": "RapierAttack"
        },
        {
            "id": "e8342615-04a4-41a7-82b7-05a520af872a",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6187398b-462f-412e-84e4-7b500f621f67",
            "propertyId": "8d8e758a-cdfa-41ef-b0ca-27d0a13a833a",
            "value": "charge_attack_on_release"
        }
    ],
    "parentObjectId": "6187398b-462f-412e-84e4-7b500f621f67",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b337b87-c1e2-4141-9930-cfdc443b15fb",
    "visible": false
}