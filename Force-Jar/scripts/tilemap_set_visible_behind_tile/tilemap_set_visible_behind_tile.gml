var tilemap_id = layer_tilemap_get_id(layer_get_id(LR_TILES));

assert(tilemap_id != noone, "No tilemap found");
assert_exit

var gx = tilemap_get_cell_x_at_pixel(tilemap_id, x, y);
var gy = tilemap_get_cell_y_at_pixel(tilemap_id, x, y);

var tiledata = tilemap_get(tilemap_id, gx, gy);

visible = tile_get_empty(tiledata);