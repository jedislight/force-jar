/// @description Insert description here
// You can write your code in this editor

assert_fatal(ds_exists(info, ds_type_list), "Map Info Pickup missing info list!");

var player_object = instance_find(PlayerStates, 0);
if(instance_exists(player_object))
{
	if (not is_undefined(player_object.map_map[? map_name]))
	{
		instance_destroy();	
	}
}