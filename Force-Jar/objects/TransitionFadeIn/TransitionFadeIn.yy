{
    "id": "aeaa4476-e4b9-4cc3-80e3-5b8554da8942",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionFadeIn",
    "eventList": [
        {
            "id": "a388dc55-e88f-4d3e-8374-892d8acd9516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "aeaa4476-e4b9-4cc3-80e3-5b8554da8942"
        },
        {
            "id": "5990135f-b370-4d7b-8d8b-b30a357ae764",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aeaa4476-e4b9-4cc3-80e3-5b8554da8942"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "dd1b089e-db02-48ac-9809-3db0559cd464",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}