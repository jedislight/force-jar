{
    "id": "ad99be38-cfba-4a0a-8387-38e4b1a7b87a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MenuControl",
    "eventList": [
        {
            "id": "d26ef474-3af0-4b1d-a009-f5064668d55c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad99be38-cfba-4a0a-8387-38e4b1a7b87a"
        },
        {
            "id": "c7ec4d35-e95e-481b-9e09-00ab31c60b3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "ad99be38-cfba-4a0a-8387-38e4b1a7b87a"
        },
        {
            "id": "6b6174eb-0c8e-4d42-bbcc-5d2d77cb9aac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad99be38-cfba-4a0a-8387-38e4b1a7b87a"
        },
        {
            "id": "1d9ef42a-37dc-41f1-8071-597dde95e253",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ad99be38-cfba-4a0a-8387-38e4b1a7b87a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "b0a899a3-edf4-4198-ac7f-89634bc2b43b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "use_horizontal",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}