/// @description Insert description here
// You can write your code in this editor

event_inherited();

music_transition = false;
music = noone;

if(variable_instance_exists(id, "threshold_inst"))
{
	var threshold = threshold_inst;
	if(variable_instance_exists(threshold_inst, "background_music"))
	{
		music = threshold_inst.background_music;
	}
	else
	{
		var background_music_instance = instance_find(BackgroundMusic, 0);
		if(instance_exists(background_music_instance))
		{
			music = background_music_instance.music;
		}
	}	
}

if(audio_exists(music) and !music_manager_is_background_music_playing(music))
{
	music_transition = true;
}

// Set fade to turn off.
fade_overlay.merge_direction = -1;