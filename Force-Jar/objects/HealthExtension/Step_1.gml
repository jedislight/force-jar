/// @description Done during Steps to guarantee the Player exists.

var player_object = instance_find(PlayerStates, 0);

assert_fatal(player_object != noone, "No player exists during HealthExtension [" + extension_name + "] Begin Step");

if (not is_undefined(player_object.extension_map[? extension_name]))
{
	instance_destroy();	
}