{
    "id": "dfc49e78-9103-4a94-bec7-a6a0a7d830f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "UiPlayerHealth",
    "eventList": [
        {
            "id": "837d1698-d198-4714-b32f-2864d6cd4dc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dfc49e78-9103-4a94-bec7-a6a0a7d830f1"
        },
        {
            "id": "05199b89-09be-4d83-896d-f3e76dee7c92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dfc49e78-9103-4a94-bec7-a6a0a7d830f1"
        },
        {
            "id": "4212c0d3-40a7-4f64-b242-2212b0ed8b8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dfc49e78-9103-4a94-bec7-a6a0a7d830f1"
        },
        {
            "id": "9e9c5f85-c2b2-4607-935f-53cc14182b51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "dfc49e78-9103-4a94-bec7-a6a0a7d830f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}