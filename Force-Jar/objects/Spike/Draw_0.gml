/// @description Draw in 1 Row Only

var tile_x_offset = cos(degtorad(image_angle));
var tile_y_offset = -sin(degtorad(image_angle));
for (var tile_offset = 0; tile_offset < image_xscale; ++tile_offset)
{
	var x_pos = x + (tile_x_offset * sprite_get_width(image_index) * tile_offset);
	var y_pos = y + (tile_y_offset * sprite_get_width(image_index) * tile_offset);
	draw_sprite_ext(sprite_index, image_index, x_pos, y_pos, 1.0, 1.0, image_angle, image_blend, image_alpha);	
}