{
    "id": "e548521f-3b8c-42b0-84d6-b83c16307be7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "threshold_door_dummy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 63,
    "bbox_right": 448,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a311b301-3468-4232-ae39-d93a8dd4a67a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e548521f-3b8c-42b0-84d6-b83c16307be7",
            "compositeImage": {
                "id": "4eaab15e-909b-4531-bb66-1c5fc8a35a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a311b301-3468-4232-ae39-d93a8dd4a67a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd742e3-9a47-4f2a-b45b-fcfd0e2a0d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a311b301-3468-4232-ae39-d93a8dd4a67a",
                    "LayerId": "593e08a6-a86e-468e-b6c1-1e97a01ecfd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "593e08a6-a86e-468e-b6c1-1e97a01ecfd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e548521f-3b8c-42b0-84d6-b83c16307be7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 128
}