{
    "id": "b754f75d-1780-4643-9f8c-7e19d8eabfc1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ChangeRoomMenuItem",
    "eventList": [
        {
            "id": "4c0bf187-bd29-4c33-b12a-bc6b74437971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b754f75d-1780-4643-9f8c-7e19d8eabfc1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae82b777-fbe4-48cf-9766-8118f3f75409",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e73b2ccb-0544-434d-9242-34f3433b10da",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 512,
            "value": "title_menu",
            "varName": "target_room",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}