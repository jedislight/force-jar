/// @description Start the Game

// Change the next room to load to the first room of the game.
global.next_room_to_load = room0;

room_goto(game_init);