/// @description Insert description here
// You can write your code in this editor

var flashing_once = flash_once;
flash_once = false;

if(flashing_once)
{
	shader_set(shd_overbright);
}

draw_self();

if(flashing_once)
{
	shader_reset();
}