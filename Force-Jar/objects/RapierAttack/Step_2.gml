/// @description Delete the weapon if needbe

if (timeAlive >= timeToLive)
{
	instance_destroy();	
}

if(charge == 1.0)
{	
	owner.locked = true;
	owner.facing_locked= true;
	
	ds_list_ensure(owner.locked_by, id);

	owner.x = owner.xprevious;
	owner.y = owner.yprevious;
	var collision = false;
	with(owner)
	{
		 collision = apply_movement(lengthdir_x(48, facing), lengthdir_y(48, facing));
	}
	
	if(collision)
	{
		if(collision_last_frame)
		{
			instance_destroy();
		}
		collision_last_frame = true;
	}
	else
	{
		collision_last_frame = false;
	}
}

if(charge != 1.0)
{
	timeAlive += 1;
	if (owner != noone)
	{
		x = owner.x + xoffset + lengthdir_x(thrustDistance * 64 * (timeAlive/timeToLive), direction);
		y = owner.y + yoffset + lengthdir_y(thrustDistance * 64 * (timeAlive/timeToLive), direction);
	}
}
else
{
	mask_index = img_energy_wave;
	x = owner.x + xoffset + lengthdir_x(thrustDistance * 64 * (1.0), direction);
	y = owner.y + yoffset + lengthdir_y(thrustDistance * 64 * (1.0), direction);
}