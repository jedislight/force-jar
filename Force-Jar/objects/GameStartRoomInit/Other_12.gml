/// @description Continuous
// You can write your code in this editor

event_inherited();
var background_music_instance = instance_find(BackgroundMusic, 0);
assert_fatal(instance_exists(background_music_instance), "Game start trigger could not find music instance to start!");
music = background_music_instance.music;
		
music_manager_play(music);
instance_destroy();