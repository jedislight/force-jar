/// @description Set default values here

item_selected = false;

item_selected_color = c_yellow;
item_idle_color = c_white;

if (display_name != "<default>")
{
	menu_key = display_name;
	var number_chaser = 0;
	
	while (not is_undefined(ds_map_find_value(global.active_menu_items, menu_key)))
	{
		menu_key = display_name + " " + string(++number_chaser);
	}
	
	show_debug_message("Setting active_menu_items key to " + menu_key);
	ds_map_add(global.active_menu_items, menu_key, id);
}