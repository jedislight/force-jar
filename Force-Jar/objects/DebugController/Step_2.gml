/// @description Insert description here
// You can write your code in this editor

var lists = 0;
var maps = 0;
var grids = 0;
var queues = 0;
var pqueues = 0;
var stacks = 0;
var paths = 0;

for(var i = 0; i < 200; ++i)
{
	lists += ds_exists(i, ds_type_list);
	maps += ds_exists(i, ds_type_map);
	grids += ds_exists(i, ds_type_grid);
	queues += ds_exists(i, ds_type_queue);
	pqueues += ds_exists(i, ds_type_priority);
	stacks += ds_exists(i, ds_type_stack);
	paths += path_exists(i);
}

fps_wma = (29 * fps_wma + fps_real) / 30;

text = "";

text +=   "Data Structure Counts: L:"+string(lists)+" M:"+string(maps)+" G:"+string(grids)+" Q:"+string(queues)+" S:"+string(stacks);
text += "\nInstances:"+ string(instance_number(all));
text += "\nFPS:" + string(round(fps_wma));
text += "\nMap Pos:" + string(map_x) +", "+ string(map_y);
text += "\nPost Shader:" + string(ApplicationRenderer.post_effect_shader_index);

var camera = view_camera[0]
var xscale = camera_get_view_width(camera) / surface_get_width(application_surface);
var yscale = camera_get_view_height(camera) / surface_get_height(application_surface);
text += "\nActive Camera:" + string(camera);
text += "\nActive Camera Size:" + string(camera_get_view_width(camera)) + " " + string(camera_get_view_height(camera));
text += "\nActive Camera Scale:" + string(xscale) + " " + string(yscale);
text += "\nActive Camera Pos:" + string(camera_get_view_x(camera)) + " " + string(camera_get_view_y(camera));
text += "\nTime:" + timer_get_string();
text += "\nFrames:" + string(timer_get());