{
    "id": "d7bc0c5b-dd34-4f3c-98e1-dfcc18f61dc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 10,
    "bbox_right": 52,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "720264b6-4296-4470-9e02-199c7dd42587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7bc0c5b-dd34-4f3c-98e1-dfcc18f61dc8",
            "compositeImage": {
                "id": "ef79af07-702e-4d06-9d14-99b8d9f751c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720264b6-4296-4470-9e02-199c7dd42587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e735cb06-e324-4201-bd04-46228bc3a8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720264b6-4296-4470-9e02-199c7dd42587",
                    "LayerId": "97932135-62a4-4555-afd6-d3006dc8527e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "97932135-62a4-4555-afd6-d3006dc8527e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7bc0c5b-dd34-4f3c-98e1-dfcc18f61dc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}