/// @description Insert description here
// You can write your code in this editor

var draw_at_x = UiMiniMap.x;
var draw_at_y = UiMiniMap.y;

var draw_cell_size = 32;
var alpha = 0.5;
var border_width = 5;
var door_width = 12;
var draw_cells = 5;
if(not mini)
{
	draw_at_x = UiMap.x;
	draw_at_y = UiMap.y;

	draw_cell_size = 32;
	alpha = 0.9;
	border_width = 5;
	door_width = 12;	
	draw_cells = 25
}

for(var xx = 0; xx < draw_cells; ++xx) for(var yy = 0; yy < draw_cells; ++yy)
{
	if(xx < 0 or yy < 0 or xx >= ds_grid_width(map_state_grid) or yy >= ds_grid_width(map_state_grid))
	{
		continue;
	}
	var color = c_black;
	var cell_x = x+xx-(draw_cells div 2);
	var cell_y = y+yy-(draw_cells div 2);
	if(map_state_grid[# cell_x, cell_y] == MapCellStates.EXPLORED)
	{
		color = c_yellow;
	}
	else if(map_state_grid[# cell_x, cell_y] == MapCellStates.UNEXPLORED)
	{
		color = c_purple;
	}
	
	var left = draw_at_x+xx*draw_cell_size;
	var right = draw_at_x+(xx+1)*draw_cell_size-2;
	var up = draw_at_y+yy*draw_cell_size;
	var down = draw_at_y+(yy+1)*draw_cell_size-1;
	draw_rectangle_color(left, up , right, down, color, color, color, color, false)
	
	if(map_state_grid[# cell_x, cell_y] == MapCellStates.EMPTY)
	{
		var grid_alpha = 0.2*alpha;
		draw_set_alpha(grid_alpha);
		draw_rectangle_color(left, up , right, down, c_white, c_white, c_white, c_white, true);
	}
	draw_set_alpha(alpha);
	if(cell_x == x and cell_y == y)
	{
		var offset = sin(current_time*0.005)*3;
		draw_rectangle_color(left-offset, up-offset, right+offset, down+offset, c_white, c_white, c_white, c_white, true);
	}
	
	//borders
	if(MapCellBorders.UP & map_border_grid[# cell_x, cell_y])
	{
		draw_line_width(left, up , right, up, border_width);
	}
	if(MapCellBorders.DOWN & map_border_grid[# cell_x, cell_y])
	{
		draw_line_width(left, down , right, down, border_width);
	}
	if(MapCellBorders.LEFT & map_border_grid[# cell_x, cell_y])
	{
		draw_line_width(left, up , left, down, border_width);
	}
	if(MapCellBorders.RIGHT & map_border_grid[# cell_x, cell_y])
	{
		draw_line_width(right, up , right, down, border_width);
	}
	
	//door
	var left_tight = left+round(draw_cell_size*0.33);
	var right_tight = right-round(draw_cell_size*0.33);
	var up_tight = up+round(draw_cell_size*0.33);
	var down_tight = down-round(draw_cell_size*0.33);
	if(MapCellBorders.UP & map_door_grid[# cell_x, cell_y])
	{
		draw_line_width(left_tight, up , right_tight, up, door_width);
	}
	if(MapCellBorders.DOWN & map_door_grid[# cell_x, cell_y])
	{
		draw_line_width(left_tight, down , right_tight, down, door_width);
	}
	if(MapCellBorders.LEFT & map_door_grid[# cell_x, cell_y])
	{
		draw_line_width(left, up_tight , left, down_tight, door_width);
	}
	if(MapCellBorders.RIGHT & map_door_grid[# cell_x, cell_y])
	{
		draw_line_width(right, up_tight , right, down_tight, door_width);
	}
}

draw_set_alpha(1);