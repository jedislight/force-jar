{
    "id": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_breakable_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58833200-9a5e-43df-bf32-bed8779f31fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "compositeImage": {
                "id": "22fcb1b4-50aa-487a-b5a7-bca889e74f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58833200-9a5e-43df-bf32-bed8779f31fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11599988-ffba-4c6b-a14f-2fd7933a7376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58833200-9a5e-43df-bf32-bed8779f31fa",
                    "LayerId": "7fc14790-4cd9-4a66-b0d9-3d8ffec56df4"
                },
                {
                    "id": "97b2a04a-ec69-4481-96c2-50faee9cd0dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58833200-9a5e-43df-bf32-bed8779f31fa",
                    "LayerId": "bbef7fea-a75a-4a70-8142-a736876abe65"
                }
            ]
        },
        {
            "id": "6f5e3a8c-271a-4669-bc5a-ae190e9f7386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "compositeImage": {
                "id": "1151693c-5dd7-41c9-9689-ded774b0bfb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f5e3a8c-271a-4669-bc5a-ae190e9f7386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8429bd8-eadc-4918-a625-2520c2174bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f5e3a8c-271a-4669-bc5a-ae190e9f7386",
                    "LayerId": "bbef7fea-a75a-4a70-8142-a736876abe65"
                },
                {
                    "id": "9c8baf63-8434-41fe-ad5f-26029dc0c8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f5e3a8c-271a-4669-bc5a-ae190e9f7386",
                    "LayerId": "7fc14790-4cd9-4a66-b0d9-3d8ffec56df4"
                }
            ]
        },
        {
            "id": "e7c1b3df-a7ae-4c37-8aac-3e1c52659f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "compositeImage": {
                "id": "19eac48b-a1ca-484e-9a46-6e76401c1143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c1b3df-a7ae-4c37-8aac-3e1c52659f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47d985c-3b58-424c-bff5-56d9f17baf3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c1b3df-a7ae-4c37-8aac-3e1c52659f71",
                    "LayerId": "bbef7fea-a75a-4a70-8142-a736876abe65"
                },
                {
                    "id": "5e789c7e-7711-4e12-b188-f5b3fada97c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c1b3df-a7ae-4c37-8aac-3e1c52659f71",
                    "LayerId": "7fc14790-4cd9-4a66-b0d9-3d8ffec56df4"
                }
            ]
        },
        {
            "id": "6afea002-3635-473a-9983-def3112a4742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "compositeImage": {
                "id": "762fac41-f755-4704-b4e8-9a7dd3959918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6afea002-3635-473a-9983-def3112a4742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d24cfd56-0936-4d1d-82a9-0247ee1d8c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6afea002-3635-473a-9983-def3112a4742",
                    "LayerId": "bbef7fea-a75a-4a70-8142-a736876abe65"
                },
                {
                    "id": "3858c1cd-0224-400c-a0cb-153b1ab0f53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6afea002-3635-473a-9983-def3112a4742",
                    "LayerId": "7fc14790-4cd9-4a66-b0d9-3d8ffec56df4"
                }
            ]
        },
        {
            "id": "918915f2-1566-4e4f-9b05-6ba7b9096d2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "compositeImage": {
                "id": "62fac1d7-9cfb-49b4-86d1-b5dc34f05d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918915f2-1566-4e4f-9b05-6ba7b9096d2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb4bd14-8752-44cf-bed2-5984df7fb67d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918915f2-1566-4e4f-9b05-6ba7b9096d2b",
                    "LayerId": "bbef7fea-a75a-4a70-8142-a736876abe65"
                },
                {
                    "id": "9645fbc6-2126-467a-a749-68e5809089f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918915f2-1566-4e4f-9b05-6ba7b9096d2b",
                    "LayerId": "7fc14790-4cd9-4a66-b0d9-3d8ffec56df4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bbef7fea-a75a-4a70-8142-a736876abe65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "blendMode": 3,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7fc14790-4cd9-4a66-b0d9-3d8ffec56df4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6dc4197d-f982-4b9c-881e-53e6060e2e4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}