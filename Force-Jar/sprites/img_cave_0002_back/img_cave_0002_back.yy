{
    "id": "1a8b8908-06f8-4c93-b084-4a7c78f1cf41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_cave_0002_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3999,
    "bbox_left": 0,
    "bbox_right": 3999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "040ba1d8-2a7f-47c3-8b86-da64b07aa9dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8b8908-06f8-4c93-b084-4a7c78f1cf41",
            "compositeImage": {
                "id": "c91f5b9c-fbd3-45f3-ad2e-819dee8fc7d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "040ba1d8-2a7f-47c3-8b86-da64b07aa9dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0466b1f1-c530-45bf-8d27-96d403926ce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040ba1d8-2a7f-47c3-8b86-da64b07aa9dc",
                    "LayerId": "6d850fd8-ac92-4983-983c-af85c621ad65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4000,
    "layers": [
        {
            "id": "6d850fd8-ac92-4983-983c-af85c621ad65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a8b8908-06f8-4c93-b084-4a7c78f1cf41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4000,
    "xorig": 0,
    "yorig": 0
}