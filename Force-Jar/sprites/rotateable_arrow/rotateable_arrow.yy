{
    "id": "d12f0568-37dd-42b4-a679-42a2e1e16214",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rotateable_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 51,
    "bbox_right": 61,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed36cce9-6f2f-48f9-a0bb-51d3294685b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d12f0568-37dd-42b4-a679-42a2e1e16214",
            "compositeImage": {
                "id": "d4902d3c-89dc-4be1-b842-0bb84be1d97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed36cce9-6f2f-48f9-a0bb-51d3294685b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a95053-1837-4eab-86a9-0e3c432474c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed36cce9-6f2f-48f9-a0bb-51d3294685b3",
                    "LayerId": "61a1966f-6462-4c75-885e-5a50f5bea540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "61a1966f-6462-4c75-885e-5a50f5bea540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d12f0568-37dd-42b4-a679-42a2e1e16214",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}