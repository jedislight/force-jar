{
    "id": "cf109bd7-91cd-46b2-beaf-d738e2fbd0e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_hi_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fc2dcf4-8b1f-47fd-9136-570533fa93dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf109bd7-91cd-46b2-beaf-d738e2fbd0e2",
            "compositeImage": {
                "id": "8ecd321f-0ea7-421f-8f85-d8b46598444f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc2dcf4-8b1f-47fd-9136-570533fa93dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7344cb82-d838-49e0-9771-afb580ba1307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc2dcf4-8b1f-47fd-9136-570533fa93dc",
                    "LayerId": "a6112e78-85b7-4b1d-a837-ca1bfd3d01af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a6112e78-85b7-4b1d-a837-ca1bfd3d01af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf109bd7-91cd-46b2-beaf-d738e2fbd0e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}