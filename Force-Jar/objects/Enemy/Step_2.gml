/// @description Clean up the ignore map

if (not variable_instance_exists(id, "damage_object_ignore"))
{
	exit;
}

var current_ignore_key = ds_map_find_first(damage_object_ignore);

while (current_ignore_key != undefined)
{
	var frames_left = damage_object_ignore[? current_ignore_key] - 1;
	var next_key = ds_map_find_next(damage_object_ignore, current_ignore_key);
	
	if (frames_left < 0)
	{
		ds_map_delete(damage_object_ignore, current_ignore_key);	
	}
	
	current_ignore_key = next_key;
}