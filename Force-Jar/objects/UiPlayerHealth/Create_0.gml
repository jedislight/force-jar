/// @description Basic Values setup

owner = instance_find(PlayerStates, 0);

segment_x_offset = 18;
segment_y_offset = 18;
segment_row_max = 5;
segment_column_max = 2;