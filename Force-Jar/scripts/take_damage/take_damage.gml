/// @desc When handling damage objects, returns 0 if no damage.
/// @func take_damage(damage_object, instance_hit, damage_map_key)
/// @param {DamageObjectBase} damage_object the damage object to check.
/// @param {Instance} instance_hit the id of the GMInstance that was struck.
/// @param {string} damage_map_key the id of the global damage data to use

var damage_object = argument0;
var instance_hit = argument1;
var damage_map_key = argument2;

assert(object_is_ancestor(damage_object.object_index, DamageObjectBase),
	"Damage object assigned in take_damage was not a DamageObjectBase");
if (global.last_assert)
{
	return 0;	
}

assert(ds_map_exists(global.damage_data_map, damage_map_key),
	"Damage data key could not be found, treating target as universally struck");

if (global.last_assert)
{
	// If this throws that means damage_object isn't a DamageObject.  Or at least doesn't use `damage_amt`
	return damage_object.damage_amt;
}

var ddm_current = ds_map_find_value(global.damage_data_map, damage_map_key);

var ddm_faction = Factions.UNIVERSAL;
if (ds_map_exists(ddm_current, DM_FACTION))
{
	ddm_faction = ddm_current[? DM_FACTION];
	show_debug_message("Faction value [" + string(ddm_faction) + "] and is assigned");
}

if (ddm_faction == Factions.UNIVERSAL || ddm_faction != damage_object.faction)
{
	show_debug_message("But damage object's faction is [" + string(damage_object.faction) + "]");
	return damage_object.damage_amt;	
}

return 0;