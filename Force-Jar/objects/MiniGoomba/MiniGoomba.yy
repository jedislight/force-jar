{
    "id": "7cc25101-fd42-48c2-bdf0-02b4ffc015dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MiniGoomba",
    "eventList": [
        {
            "id": "4729c875-1034-4a50-b97f-e908f4b2f3e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7cc25101-fd42-48c2-bdf0-02b4ffc015dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "377eccde-9bd6-4653-9069-83569af1c46f",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
            "propertyId": "20aca9f3-ef56-4faf-bb9e-13400362483b",
            "value": "3"
        }
    ],
    "parentObjectId": "f8d813a5-3104-44b3-8995-9abfe6e54b80",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "35928798-b217-4f00-abd5-476266961574",
    "visible": true
}