/// @description Check Player Health

if (not instance_has_health(id))
{
	exit;	
}

if (health_current <= 0)
{
	instance_destroy();
	exit;
}