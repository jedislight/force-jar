/// @description Basic Variable Declaration for Damage Objects

// If overriden, call event_inherit()

// By default, all damage objects hurt everybody.
// Override from those that call for a weapon to be made.
faction = Factions.UNIVERSAL;
