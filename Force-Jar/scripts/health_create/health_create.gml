/// @function health_create()
/// @desc Applies health to whatever object calls this script
/// @param {Real} health_initial_max the starting max health

var health_initial_max = argument0;

assert(is_real(health_initial_max), "Cannot set health data, max initial health was not REAL");
assert_exit

if (instance_has_health(id) && not health_dirty)
{
	// Values already set, do not reset
	exit;
}

health_max = health_initial_max;
health_current = health_max;

health_dirty = false; // if not dirty, don't allow this to run again.