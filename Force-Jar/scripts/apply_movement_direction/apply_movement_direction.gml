/// @function apply_movement_direction(direction, amount)
var dir = argument0;
var amount = argument1;

var dx = lengthdir_x(amount, dir);
var dy = lengthdir_y(amount, dir);

return apply_movement(dx, dy);