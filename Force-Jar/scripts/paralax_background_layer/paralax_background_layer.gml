/// @function paralax_background_layer(layer_id, scalar)
/// @description sets a background's paralax x position
/// @param {layer_id} 
/// @param {scalar} 

var xx = camera_get_view_x(view_camera[0]);
var layer_id = argument0;
if(layer_exists(layer_id))
{
	layer_x(layer_id, xx * -argument1);
}