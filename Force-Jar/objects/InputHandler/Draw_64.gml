/// @description Insert description here
// You can write your code in this editor

var xx = 32;
var yy = 32 + (160 * target_gamepad);
var d_color = draw_get_color();
draw_set_color(c_white);
draw_set_font(ui_debug_font);
var line_spacing = font_get_size(ui_debug_font) + 5;
if gamepad_is_connected(target_gamepad)
{
    draw_text(xx, yy, "Gamepad Slot - " + string(target_gamepad));
    draw_text(xx, yy + line_spacing, "Gamepad Type - " + string(gamepad_get_description(target_gamepad)));
    draw_text(xx, yy + line_spacing * 2, "Left H Axis - " + string(gamepad_axis_value(target_gamepad, gp_axislh)));
    draw_text(xx, yy + line_spacing * 3, "Left V Axis - " + string(gamepad_axis_value(target_gamepad, gp_axislv)));
    draw_text(xx, yy + line_spacing * 4, "Right H Axis - " + string(gamepad_axis_value(target_gamepad, gp_axisrh)));
    draw_text(xx, yy + line_spacing * 5, "Right V Axis - " + string(gamepad_axis_value(target_gamepad, gp_axisrv)));   
    draw_text(xx, yy + line_spacing * 6, "Fire Rate - " + string(gamepad_button_value(target_gamepad, gp_shoulderrb)));
	draw_text(xx, yy + line_spacing * 7, "Face Button 1 - " + string(gamepad_button_value(target_gamepad, gp_face1)));
	draw_text(xx, yy + line_spacing * 8, "Face Button 2 - " + string(gamepad_button_value(target_gamepad, gp_face2)));
	draw_text(xx, yy + line_spacing * 9, "Face Button 3 - " + string(gamepad_button_value(target_gamepad, gp_face3)));
	draw_text(xx, yy + line_spacing * 10, "Face Button 4 - " + string(gamepad_button_value(target_gamepad, gp_face4)));
	draw_text(xx, yy + line_spacing * 11, "Start Button - " + string(gamepad_button_value(target_gamepad, gp_start)));
	draw_text(xx, yy + line_spacing * 12, "Select Button - " + string(gamepad_button_value(target_gamepad, gp_select)));
	draw_text(xx, yy + line_spacing * 13, "Left Bumper - " + string(gamepad_button_value(target_gamepad, gp_shoulderlb)));
	draw_text(xx, yy + line_spacing * 14, "Right Bumper - " + string(gamepad_button_value(target_gamepad, gp_shoulderrb)));
	draw_text(xx, yy + line_spacing * 15, "DPad Up - " + string(gamepad_button_value(target_gamepad, gp_padu)));
	draw_text(xx, yy + line_spacing * 16, "DPad Down - " + string(gamepad_button_value(target_gamepad, gp_padd)));
	draw_text(xx, yy + line_spacing * 17, "DPad Right - " + string(gamepad_button_value(target_gamepad, gp_padr)));
	draw_text(xx, yy + line_spacing * 18, "DPad Left - " + string(gamepad_button_value(target_gamepad, gp_padl)));
	
}
else
{
    draw_text(xx, yy, "Gamepad Slot - " + string(target_gamepad));
    draw_text(xx, yy + 20, "Gamepad not connected" + string(gamepad_get_description(target_gamepad)));
}
draw_set_color(d_color);
draw_set_font(-1);