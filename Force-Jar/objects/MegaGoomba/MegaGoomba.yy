{
    "id": "f8d813a5-3104-44b3-8995-9abfe6e54b80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MegaGoomba",
    "eventList": [
        {
            "id": "e00a859d-ce81-493e-8fcd-b39c0c180131",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f8d813a5-3104-44b3-8995-9abfe6e54b80"
        },
        {
            "id": "44f26d1b-55f7-4e57-89a9-bf98f790f80e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f8d813a5-3104-44b3-8995-9abfe6e54b80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "0f2f49b8-61eb-41fc-afa5-2066545713b5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
            "propertyId": "20aca9f3-ef56-4faf-bb9e-13400362483b",
            "value": "25"
        }
    ],
    "parentObjectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "0aacbf14-d271-40d8-8800-94b2efe1737e",
    "visible": true
}