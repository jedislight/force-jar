/// @function knock_back(instance, from_x, from_y)

var kb = instance_create_depth(0,0,0, KnockerBacker);
kb.target = argument0
var pd = point_direction(argument1, argument2, argument0.x, argument0.y);
kb.direction = pd;