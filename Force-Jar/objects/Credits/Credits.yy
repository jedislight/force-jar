{
    "id": "c5e1b4cd-5706-429f-af58-ceacbfd784bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Credits",
    "eventList": [
        {
            "id": "cc1e388a-a6cb-4bd5-ae50-657119cad331",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c5e1b4cd-5706-429f-af58-ceacbfd784bb"
        },
        {
            "id": "db31d858-b1d9-44f9-919e-322a86cd8b9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5e1b4cd-5706-429f-af58-ceacbfd784bb"
        },
        {
            "id": "a362f096-f1a9-49ce-8f2e-408de8500ee0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c5e1b4cd-5706-429f-af58-ceacbfd784bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}