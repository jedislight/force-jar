/// @description Insert description here
// You can write your code in this editor

pickup_bob();
image_alpha = min(1.0, frame*.01);
var player = instance_find(PlayerStates,0);
if(instance_exists(player))
{
	if(player.powerStates[? power_granted] != PowerStates.DISABLED)
	{
		instance_destroy();
	}
}