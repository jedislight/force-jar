/// @description Base (Create DS)

// This recreates the transition_data and transition_queue if they've been destroyed somehow.
// Anything that is declared in this create function must be checked before redefining them
// in case an inherited object does not override Create.

if (!variable_instance_exists(id, "transition_data"))
{
	// Transition data is where you should be memory related data you wish to manage in other systems.
	transition_data = ds_map_create();
}

if (!variable_instance_exists(id, "transition_queue"))
{
	// Set as a FIFO Queue as we go through each transition then dump it.
	transition_queue = ds_queue_create();
}

// Set to true if ever actually done.
transition_done = false;