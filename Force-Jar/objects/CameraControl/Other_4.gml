/// @description Insert description here
// You can write your code in this editor

var player_target = instance_find(PlayerStates, 0);

// Set the standard room camera.
view_visible[0] = true;
view_enabled = true;
camera_set_view_size(view_camera[0], camera_width, camera_height);
camera_set_view_target(view_camera[0], player_target);
camera_set_view_speed(view_camera[0], -1, -1);
camera_set_view_border(view_camera[0], 1534/2, 1280/2);