/// @description push apart

if(id < other.id)
{
	var wiggle = irandom_range(-15,15);
	var dir = point_direction(other.x, other.y, x,y) + wiggle;
	
	apply_movement_direction(dir, push_speed);
	with(other)
	{
		apply_movement_direction(dir+180, push_speed);
	}
}
