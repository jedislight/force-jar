assert_fatal(ds_list_size(info) >= 4, "malformed map info - bad number of paramaters left during apply");
var xx = ds_list_pop(info);
var yy = ds_list_pop(info);
var borders = ds_list_pop(info);
var doors = ds_list_pop(info);

if(Map.map_state_grid[# xx, yy] == MapCellStates.EMPTY)
{
	Map.map_border_grid[# xx, yy] = borders;
	Map.map_door_grid[# xx, yy] = doors;
	Map.map_state_grid[# xx, yy] = MapCellStates.UNEXPLORED;
}

return ds_list_empty(info);