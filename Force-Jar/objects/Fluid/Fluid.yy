{
    "id": "9e3303a1-3939-4114-b622-3ed1e1ddf147",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Fluid",
    "eventList": [
        {
            "id": "8fa27a6e-2335-4fcf-b985-bc9634768bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e3303a1-3939-4114-b622-3ed1e1ddf147"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b2d18c4-becd-4747-a1df-cac11b875a86",
    "visible": true
}