var value = argument0
var message = argument1
if(not value)
{
	show_debug_message("ASSERT:" + message);
}

global.last_assert = not value;