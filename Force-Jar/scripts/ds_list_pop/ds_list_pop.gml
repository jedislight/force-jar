result = undefined;

if(not ds_list_empty(argument0))
{
	result = argument0[|0];
	ds_list_delete(argument0, 0);
}

return result;