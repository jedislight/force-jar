/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(not locked)
{
	var v = velocity * ((frame % 5)/5);
	
	var px = x;
	var py = y;
	if(instance_exists(target))
	{
		px = target.x;
		py = target.y;
		
		if(target.object_index == Hive)
		{
			px = spawn_x;
			py = spawn_y;
		}
	}

	var path_viable = mp_grid_path(FlightPather.nav_grid, path, x, y, px, py, true);
	var tx = x;
	var ty = y-1;
	var tx = path_get_point_x(path, 1);
	var ty = path_get_point_y(path, 1);
	
	var dx = lengthdir_x(v, point_direction(x,y, tx, ty));
	var dy = lengthdir_y(v, point_direction(x,y, tx, ty)) + 3;

	if(instance_exists(nearest_other))
	{
		var away = point_direction(other.x, other.y, x, y);
		var away_velocity = velocity / sqrt(distance_to_object(nearest_other));
		var away_x = lengthdir_x(away_velocity, away);
		var away_y = lengthdir_y(away_velocity, away);
	}

	if(frames_since_last_turn > minimum_frame_to_turn)
	{
		apply_movement(dx,dy);
	}
}
else
{
	if(instance_number(Grappler) == 0)
	{
		locked = false;
	}
}

if(place_meeting(x,y, Fluid))
{
	health_change(id, -1);
}