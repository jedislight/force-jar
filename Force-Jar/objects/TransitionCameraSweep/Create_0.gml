/// @description Find Door In

if (!variable_instance_exists(id, player_instance))
{
	// May be previously set by TransitionPlayerStateToggle
	player_instance = instance_find(PlayerStates, 0); // always one.
}

// If none of this is set, the transition state will hard fail

var found_threshold = ds_map_create();
var ft_keys = ds_list_create();
	
var entry_side = (room_side + 180) % 360;
	
for (var inst_count = 0; inst_count < instance_number(TransitionThreshold); ++inst_count)
{
	var curr_inst = instance_find(TransitionThreshold, inst_count);
	if (curr_inst != noone && curr_inst.image_angle == entry_side)
	{
		var curr_key = 0;
		switch (entry_side)
		{
			case DoorDirection.EAST:
			case DoorDirection.WEST:
				curr_key = (curr_inst.y << 10) | curr_inst.x;
				break;
			case DoorDirection.NORTH:
			case DoorDirection.SOUTH:
				curr_key = (curr_inst.x << 10) | curr_inst.y;
				break;
		}
			
		// If this ever happens, that means the doors are literally on top of one another.
		while (ds_map_exists(found_threshold, curr_key))
		{
			curr_key += 10;
		}
			
		ds_map_add(found_threshold, curr_key, curr_inst);
		ds_list_add(ft_keys, curr_key);
	}
}
	
if (ds_map_empty(found_threshold))
{
	show_debug_message("No thresholds with the correct angle were found.");
	var backup_inst = instance_find(TransitionThreshold, 0);
	assert_fatal(backup_inst != noone, "There are no doors! What kind of nightmare room did you enter?");
		
	ds_map_add(found_threshold, 0, backup_inst);
	entry_side = backup_inst.image_angle;
}
	
ds_list_sort(ft_keys, true);
	
var key_index = min(door_offset, ds_list_size(ft_keys)-1);
	
if (key_index != door_offset)
{
	show_debug_message("Out of range exception caught on transitionThreshold instance " + string(instance_id));
}
	
var target_offset_key = ds_list_find_value(ft_keys, key_index);
	
threshold_inst = ds_map_find_value(found_threshold, target_offset_key);
	
switch (entry_side)
{
case DoorDirection.WEST:
	player_instance.x = threshold_inst.x + (threshold_inst.sprite_width - threshold_inst.sprite_xoffset) + 64;
	player_instance.y = threshold_inst.y - 64;
	Map.x+=1;
	break;
case DoorDirection.EAST:
	player_instance.x = threshold_inst.x - (threshold_inst.sprite_width - threshold_inst.sprite_xoffset) - 64;
	player_instance.y = threshold_inst.y - 64;
	Map.x-=1;
	break;
case DoorDirection.NORTH:
	player_instance.x = threshold_inst.x;
	player_instance.y = threshold_inst.y + (threshold_inst.sprite_width - threshold_inst.sprite_xoffset) + 64;
	Map.y+=1;
	break;
case DoorDirection.SOUTH:
	player_instance.x = threshold_inst.x;
	player_instance.y = threshold_inst.y - (threshold_inst.sprite_width - threshold_inst.sprite_xoffset) - (64 + player_instance.sprite_height);
	Map.y-=1;
	break;
}
Map.anchor_room = true;
	
player_instance.jump_start = player_instance.y - jump_offset;
	
ds_list_destroy(ft_keys);
ds_map_destroy(found_threshold);

sweep_objects = ds_list_create();
sweep_speed = 100;
var sweep_speed_create = sweep_speed;
var sweep_init_x = cos(degtorad(room_side)) * camera_get_view_width(view_camera[0]);
var sweep_init_y = sin(degtorad(room_side)) * camera_get_view_height(view_camera[0]);

for (var inst_index = 0; inst_index < instance_number(TransitionThreshold); ++inst_index)
{
	// For now, we'll work with just transition thresholds.
	var curr_inst = instance_find(TransitionThreshold, inst_index);
	
	ds_list_add(sweep_objects, curr_inst);
	with (curr_inst)
	{
		// these values will be set to undefined when we finish.
			
		var sweep_direction = point_direction(x + sweep_init_x, y - sweep_init_y, xstart, ystart);
			
		x += sweep_init_x;
		y -= sweep_init_y;
			
		motion_add(sweep_direction, sweep_speed_create);
	}
}