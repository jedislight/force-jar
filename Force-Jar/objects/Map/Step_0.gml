/// @description Insert description here
// You can write your code in this editor

player = instance_find(PlayerPlay, 0);

if(instance_exists(player))
{
	var local_cell_x = player.x div map_world_size_x;
	var local_cell_y = player.y div map_world_size_y;
	
	if(anchor_room)
	{
		room_origin_x = x - local_cell_x;
		room_origin_y = y - local_cell_y;
		room_max_x = (room_width-64) div map_world_size_x;
		room_max_y = (room_height-64) div map_world_size_y;
		anchor_room = false;
		exit;
	}
	
	x = room_origin_x + local_cell_x;
	y = room_origin_y + local_cell_y;
	
	if(debug_mode)
	{
		DebugController.map_x = x;
		DebugController.map_y = y;
	}
	
	map_state_grid[# x, y] = MapCellStates.EXPLORED;
	if(local_cell_x == 0)
	{
		map_border_grid[# x, y] = map_border_grid[# x, y] | MapCellBorders.LEFT;
	}
	if(local_cell_y == 0)
	{
		map_border_grid[# x, y] = map_border_grid[# x, y] | MapCellBorders.UP;
	}
	if(local_cell_x == room_max_x)
	{
		map_border_grid[# x, y] = map_border_grid[# x, y] | MapCellBorders.RIGHT;
	}
	if(local_cell_y == room_max_y)
	{
		map_border_grid[# x, y] = map_border_grid[# x, y] | MapCellBorders.DOWN;
	}
	
	with(TransitionThreshold)
	{
		var inner_x = x;
		var inner_y = y;
		switch(image_angle)
		{
			case DoorDirection.EAST:
				inner_x -= 0.5*sprite_width;
				break;
			case DoorDirection.WEST:
				inner_x -= 0.5*sprite_width;
				break;
			case DoorDirection.NORTH:
				inner_y += 0.5*sprite_width;
				break;
			case DoorDirection.SOUTH:
				inner_y -= 0.5*sprite_width;
				break;
			
		}
		var transition_local_x = inner_x div other.map_world_size_x;
		var transition_local_y = inner_y div other.map_world_size_y;
		
		if(transition_local_x == local_cell_x and transition_local_y == local_cell_y)
		{
			switch(image_angle)
			{
				case DoorDirection.EAST:
					other.map_door_grid[# other.x, other.y] = other.map_door_grid[# other.x, other.y] | MapCellBorders.RIGHT;
					break;
				case DoorDirection.WEST:
					other.map_door_grid[# other.x, other.y] = other.map_door_grid[# other.x, other.y] | MapCellBorders.LEFT;
					break;
				case DoorDirection.NORTH:
					other.map_door_grid[# other.x, other.y] = other.map_door_grid[# other.x, other.y] | MapCellBorders.UP;
					break;
				case DoorDirection.SOUTH:
					other.map_door_grid[# other.x, other.y] = other.map_door_grid[# other.x, other.y] | MapCellBorders.DOWN;
					break;	
			}
		}
	}
}