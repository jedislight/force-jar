/// @description Insert description here
// You can write your code in this editor

// PULL : TOATL HACK - pre draw so grappler can move during post-step


if (!variable_instance_exists(id, "pull"))
{
	return;
}

if(instance_exists(pull))
{
	x = xprevious;
	y = yprevious;
	
	var pull_dx = pull.x - pull.xprevious;
	var pull_dy = pull.y - pull.yprevious;
	x += pull_dx;
	y += pull_dy;
	
	if(not place_free(x,y))
	{
		x = xprevious;
		y = yprevious;
		instance_destroy(pull);
		pull = noone;
	}
}
else
{
	pull = noone;
}

if(not solid and !place_free(x,y))
{
	instance_destroy();
}