var menu_control_target = argument0;

with (menu_control_target)
{
	if (ds_list_size(menu_item_list) > 0)
	{
		with (ds_list_find_value(menu_item_list, selected_item))
		{
			item_selected = true;	
		}
	}	
}