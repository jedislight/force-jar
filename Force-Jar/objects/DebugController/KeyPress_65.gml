/// @description Grant all powers
// You can write your code in this editor

if(keyboard_check(vk_control))
{
	var player = instance_find(PlayerStates, 0);
	if(instance_exists(player))
	{
		for(var i = Powers.POWERS_START; i < Powers.POWERS_END; ++i)
		{
			player.powerStates[? i] = PowerStates.ON;
		}
	}
}