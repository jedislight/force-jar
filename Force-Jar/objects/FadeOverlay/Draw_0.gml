/// @description Draw Overlay

var new_merge_amt = merge_amt + (merge_delta * sign(merge_direction));
merge_amt = (merge_direction > 0) ? max(merge_amt, new_merge_amt) : min(merge_amt, new_merge_amt);

draw_set_color(c_black);
draw_set_alpha(merge_amt);
draw_rectangle(0, 0, room_width, room_height, false);
draw_set_alpha(1.0);

if (merge_direction < 0)
{
	merge_amt = max(0.0, merge_amt);	
}
else
{
	merge_amt = min(1.0, merge_amt);	
}
