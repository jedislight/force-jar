{
	var dx = abs(argument0);
	var dy = abs(argument1);
	var fluid = instance_place(x,y, Fluid);
	if(instance_exists(fluid))
	{
		dx *= fluid.motion_modifier;
		dy *= fluid.motion_modifier;
	}
	var signx = sign(argument0);
	var signy = sign(argument1);
	
	var collision = false;
	
	for(var i = max(dx,dy); i > 0 and (signx != 0 or signy != 0); --i)
	{
		if(i<=dx or i < 1 and signx != 0)
		{
			var step_x = signx * min(1, i);
			if(i < 1)
			{
				step_x = min(dx-int64(dx), step_x);
			}
			x += step_x;
			if(!place_free(x,y))
			{
				x -= step_x;
				signx = 0;
				collision = true;
			}
		}
		
		
		if(i<=dy or i < 1 and signy != 0)
		{
			var step_y = signy * min(1, i);
			if(i < 1)
			{
				step_y = min(dy-int64(dy), step_y);
			}
			y += step_y;
			if(!place_free(x,y))
			{
				y -= step_y;
				signy = 0;
				collision = true;
			}
		}
	}
	
	return collision;
}
