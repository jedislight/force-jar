/// @description Values for Attack

// Defining these things in the individual attack so the player can use each one separately.
// Without needing to know how it works internally.

event_inherited();

timeToLive = 3;
timeAlive = 0;
thrustDistance = 1.5;

yoffset = 0;
xoffset = 0;

frame = 0;

collision_last_frame = false;