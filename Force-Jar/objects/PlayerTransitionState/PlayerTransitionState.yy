{
    "id": "d87666ce-89d0-49b0-ac83-fa8a1ff5c2a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PlayerTransitionState",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "e0d08441-7a44-4210-a175-119da60f2fb5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "201d114f-1383-4886-9290-61266cbf78b0",
            "propertyId": "73d7c9b3-cf07-4c43-9166-ff5355980d55",
            "value": "DrawLayer.DRAW_ALWAYS"
        }
    ],
    "parentObjectId": "380edad1-487c-4192-bac0-65401ddf29b8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
    "visible": true
}