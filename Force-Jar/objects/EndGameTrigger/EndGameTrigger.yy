{
    "id": "4e29d1b6-590e-4819-8a0d-96cb32914283",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "EndGameTrigger",
    "eventList": [
        {
            "id": "8b5f6c5b-2d80-4f4e-b6db-f8035e49dc54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fce370f7-a038-4188-bfcb-126b004a9dfc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4e29d1b6-590e-4819-8a0d-96cb32914283"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0195675d-7d94-46f4-8583-c923ce6f2ce0",
    "visible": true
}