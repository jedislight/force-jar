/// @description Cycle Forward and Change

if (image_speed > 0 && image_index == image_number - 1)
{
	respawning = true;
	respawn_later_as(BreakableTerrain, respawn_timeout);
	return;
}
else if (image_speed < 0 && image_index == 0)
{
	if (collision_rectangle(x+1, y+1, x+sprite_get_width(sprite_index)-1, y+sprite_get_height(sprite_index)-1, all, false, true) != noone)
	{
		image_speed = 1;
		return;
	}
	
	image_speed = 0;
	solid = true;
}