/// @description Insert description here
// You can write your code in this editor

if (variable_instance_exists(id, "object_initialized"))
{
	exit;	
}

move_speed = 16;
jumping = false;
jump_height = 8;
jump_falloff_deceleration = 8;
jump_falloff_current = 0;
jump_start = 0;
facing = 0;
facing_locked = false;
coyote_time = 0;
coyote_time_max = 4;
grapler = noone;
grapler_max_extent = 16;
grapler_velocity = 64;
active_weapon = noone;
active_offhand_weapon = noone;
locked = false;
faction = Factions.PLAYER;
locked_by = ds_list_create();

// init powers to disabled
powerStates = ds_map_create();
for(var i = Powers.POWERS_START; i < Powers.POWERS_END; ++i)
{
	powerStates[? i] = PowerStates.DISABLED;
}

// init damage table
damage_table_key = "Player";
damage_data_map = noone;
if (!find_damage_table(damage_table_key))
{
	damage_data_map = global.damage_data_map[? damage_table_key];
	damage_data_map[? DM_FACTION] = faction;
}
else
{
	damage_data_map = global.damage_data_map[? damage_table_key];	
}

// initialize health
health_create(99);

// Later, we can update this using save data.
extension_map = ds_map_create();
map_map = ds_map_create();

// For initializing this on first object, to guarantee it's never not set.
last_player_state = object_index;

object_initialized = true;