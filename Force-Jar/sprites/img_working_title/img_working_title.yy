{
    "id": "f459084a-2ede-442c-8c65-4ca4227214db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_working_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 187,
    "bbox_left": 20,
    "bbox_right": 380,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "040dfecb-6346-4d2a-9d75-d34877636a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f459084a-2ede-442c-8c65-4ca4227214db",
            "compositeImage": {
                "id": "b842070f-ac6b-4ae8-b0d7-79b8d66b3e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "040dfecb-6346-4d2a-9d75-d34877636a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d26d60a-7b9f-44ae-974f-67563586b568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040dfecb-6346-4d2a-9d75-d34877636a70",
                    "LayerId": "0bc5ff5d-619d-47f1-8009-c0858a211dd3"
                },
                {
                    "id": "7d243ae8-5108-40cf-9d20-43b610c3d694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040dfecb-6346-4d2a-9d75-d34877636a70",
                    "LayerId": "cad30ab7-f94e-4ea7-951e-a9446e9a2508"
                },
                {
                    "id": "f687ec45-1818-4ad7-98be-5a3240d6d037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040dfecb-6346-4d2a-9d75-d34877636a70",
                    "LayerId": "a40c567c-948c-4178-b098-955621b2fae6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "a40c567c-948c-4178-b098-955621b2fae6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f459084a-2ede-442c-8c65-4ca4227214db",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "cad30ab7-f94e-4ea7-951e-a9446e9a2508",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f459084a-2ede-442c-8c65-4ca4227214db",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0bc5ff5d-619d-47f1-8009-c0858a211dd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f459084a-2ede-442c-8c65-4ca4227214db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}