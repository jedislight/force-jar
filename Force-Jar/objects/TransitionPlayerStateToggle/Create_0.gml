/// @description Toggle Player State

if (!variable_instance_exists(id, "player_state_data"))
{	
	player_instance = instance_find(PlayerStates, 0);
	var player_current_state;

	with (player_instance)
	{
		player_current_state = object_index;
		instance_change(PlayerTransitionState, false);
	}
	
	player_state = player_current_state;
	
	player_state_data = true;
}
else
{
	var player_original_state = player_state;
	
	with (player_instance)
	{
		instance_change(player_original_state, false);	
	}
}

event_user(0);