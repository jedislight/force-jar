{
    "id": "8d5b2706-44f9-46f2-bdce-4f07093d06e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_point",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0e07ccb-ff97-4cec-beb1-b9f10e0c5e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d5b2706-44f9-46f2-bdce-4f07093d06e3",
            "compositeImage": {
                "id": "7bac43b2-16e8-44a4-be18-6b252e9856dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0e07ccb-ff97-4cec-beb1-b9f10e0c5e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ee9131-b735-4a1f-97d6-04e2d19b1a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0e07ccb-ff97-4cec-beb1-b9f10e0c5e48",
                    "LayerId": "7948a9ca-805f-4ddf-9847-97247ffcf371"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "7948a9ca-805f-4ddf-9847-97247ffcf371",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d5b2706-44f9-46f2-bdce-4f07093d06e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}