var frames = timer_get();
var hours = floor(frames/30/60/60);
frames -= hours * 30 * 60 * 60
var minutes = floor(frames/30/60);
frames -= minutes * 30 * 60;
var seconds = floor(frames/30);
frames -= seconds * 30;
var miliseconds = floor(frames * (1000/30));
var text = string(hours)+":"+string(minutes)+":"+string(seconds)+"::"+string(miliseconds);
return text;