{
    "id": "2a798e7d-84ca-41f8-b9c3-69d1bdf4c680",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionFadeOut",
    "eventList": [
        {
            "id": "3c107093-b15a-4c94-b49d-b5e47ddf373c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a798e7d-84ca-41f8-b9c3-69d1bdf4c680"
        },
        {
            "id": "cc23d495-e68b-4dc9-abbe-b04ea0341f44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "2a798e7d-84ca-41f8-b9c3-69d1bdf4c680"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "dd1b089e-db02-48ac-9809-3db0559cd464",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}