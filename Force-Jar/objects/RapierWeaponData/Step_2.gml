/// @description Insert description here
// You can write your code in this editor
++frame;

draw_idle = instance_number(RapierAttack) == 0;

var charge_percent = charge / charge_max;

anchor_offset_x = lerp(anchor_offset_x_base, anchor_offset_x_base - 8, charge_percent);
anchor_offset_y = lerp(anchor_offset_y_base, anchor_offset_y_base + 4, charge_percent);

anchor_angle = -45;
if(charge_percent != 0)
{
	anchor_angle = owner.facing;
	if(anchor_angle > 90 and anchor_angle < 270)
	{
		//anchor_angle -= 180;	
		anchor_angle = 180 - anchor_angle ;
	}
}

image_blend = merge_color(c_white,c_red, min(abs(sin(frame*.25)*0.25+0.5),charge));