/// @description Shut down Animation

if (!variable_instance_exists(id, "respawning"))
{
	respawning = false;	
}

if (!respawning)
{
	image_index = 0;
	image_speed = 0;
}
else
{
	solid = false;
	image_index = image_number - 1;
	image_speed = -1;
	respawning = false;
}