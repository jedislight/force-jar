{
    "id": "b99af2a7-ce84-4313-acd4-2240c857cdac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionGotoRoom",
    "eventList": [
        {
            "id": "d2c892c7-10cf-4ee3-a924-1f4ed84ffeb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b99af2a7-ce84-4313-acd4-2240c857cdac"
        },
        {
            "id": "d5550b6c-058d-4127-bd8e-1f623efe291d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b99af2a7-ce84-4313-acd4-2240c857cdac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dd1b089e-db02-48ac-9809-3db0559cd464",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}