/// @description Insert description here
// You can write your code in this editor

if(keyboard_check_direct(vk_control))
{
	ApplicationRenderer.post_effect_shader_index = (ApplicationRenderer.post_effect_shader_index - 1) % ds_list_size(ApplicationRenderer.post_effect_shaders);
	if(ApplicationRenderer.post_effect_shader_index < 0)
	{
		ApplicationRenderer.post_effect_shader_index = ds_list_size(ApplicationRenderer.post_effect_shaders) -1;
	}
}
