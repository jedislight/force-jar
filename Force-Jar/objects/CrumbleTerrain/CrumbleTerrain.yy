{
    "id": "f47f78e9-11e7-425f-a6d6-691503bdc0b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CrumbleTerrain",
    "eventList": [
        {
            "id": "dd27f156-da7d-4af1-accf-1f2f4dd03600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f47f78e9-11e7-425f-a6d6-691503bdc0b3"
        },
        {
            "id": "3651ac1a-b163-471b-909f-e0d4d4ca514a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f47f78e9-11e7-425f-a6d6-691503bdc0b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a1e55b03-e373-4293-be98-a1325d58bb2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "ed68bff5-710a-4f87-91bc-f84b59208507",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "5",
            "varName": "respawn_timeout",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "615fc1c2-5b2e-48d1-a166-55cc48b73272",
    "visible": true
}