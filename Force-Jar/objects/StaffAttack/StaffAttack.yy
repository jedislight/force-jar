{
    "id": "76e0c264-d375-43a2-82bf-e3536e627e2e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "StaffAttack",
    "eventList": [
        {
            "id": "d12f809d-cebd-4f28-abdc-911992207184",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76e0c264-d375-43a2-82bf-e3536e627e2e"
        },
        {
            "id": "9cb8577a-59d3-4a96-9249-fbc88d55751d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76e0c264-d375-43a2-82bf-e3536e627e2e"
        },
        {
            "id": "6f2a515e-f226-4ea6-a3bd-b98f3e35fc01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "76e0c264-d375-43a2-82bf-e3536e627e2e"
        },
        {
            "id": "e8fd51f3-96e7-4663-a518-6405b87ec055",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "76e0c264-d375-43a2-82bf-e3536e627e2e"
        },
        {
            "id": "ca52b39c-02c4-4b28-80ac-06a48a9e6f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "76e0c264-d375-43a2-82bf-e3536e627e2e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "af028892-46ad-4d31-9506-7b8ce650b89f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "53d9ea50-fc5c-4878-8bb3-111cd6a2c91d",
    "visible": true
}