//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
	vec4 sampled = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 adjusted = sampled;
	float brightness = (sampled.r + sampled.g + sampled.b) / 3.0;
	if(sampled.r < brightness)
	{
		adjusted.r = clamp(adjusted.r * 1.25, 0.0, brightness);
	}

	if(sampled.g < brightness)
	{
		adjusted.g = clamp(adjusted.g * 1.25, 0.0, brightness);
	}

	if(sampled.b < brightness)
	{
		adjusted.b = clamp(adjusted.b * 1.25, 0.0, brightness);
	}
	
	adjusted *= 1.50;
    gl_FragColor = adjusted;
}
