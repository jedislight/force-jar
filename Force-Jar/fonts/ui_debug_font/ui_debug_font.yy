{
    "id": "56aa3ae0-f090-4f76-8e32-9a89e5422caa",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "ui_debug_font",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4eafeed9-cf14-4505-a791-278816ae10cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 269,
                "y": 146
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3f6ed686-38c9-4aaa-9231-8b47f359dbe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 404,
                "y": 146
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2c3d881b-0db1-4a1d-b7ea-b12dc854db83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 213,
                "y": 146
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "687739d0-7c09-45f2-af12-86856a729267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 283,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c224edcf-7974-4555-8371-a4063fba9196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "dada05fe-c959-42f1-af72-07642f42d8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "90f5fbfc-e683-4b67-a7b0-e086ed19aa02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "83fbad00-09e6-4dd4-b166-4eaabbac25be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 397,
                "y": 146
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "19930691-39b2-444b-aafc-890beb6fab32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 308,
                "y": 146
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1c380dff-fa8f-4337-bf4c-3197c49d132f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 320,
                "y": 146
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "66dcde84-60ac-4499-a18a-41bc3f402115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 139,
                "y": 146
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "01741a4c-8741-48ec-bb34-4ec31d815ced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 443,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e461b187-2fec-4211-93bb-d8e4876d2bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 376,
                "y": 146
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "22ad6bd2-9b4a-45a3-a93d-9150b5bb8218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 241,
                "y": 146
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c4031c36-7da9-4826-8598-3fb341e0611c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 432,
                "y": 146
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f20301c3-6cd4-4d41-a8b0-0eb51b821f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 255,
                "y": 146
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "792e700c-4ddf-4782-a448-de4477b3f9c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 487,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5c459836-4ea7-4618-978a-d6696fc7ac17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 295,
                "y": 146
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1cadd00b-6de0-4250-826a-abb7c130a6a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 24,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3fd217e6-dd91-4e0a-9536-2ec5c569c213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "39ff61a4-eb0d-418e-b958-36557cd86571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 354,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e1d7f5f9-e2fa-4821-bf4c-70a99e7a5113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 112,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6d02b7ee-163a-4b0b-8572-498661316c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 134,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fe6daa90-12ef-4060-9389-8147d4436f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8be50482-887c-4680-a143-de3e0a989c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 200,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4504aeba-83d7-4265-ab7a-4b77d800d7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 244,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d2ac24fd-1ea4-4d42-8c55-efd401fe272e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 418,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1abab73f-db11-4253-8605-a597a741321a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 425,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b71c07c6-db27-483a-b78a-ad271b80f492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 266,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "32572e54-e77c-4ac5-807f-fa764b3141f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 399,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "58741193-b079-48d6-8bcc-2bba1b6af2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 377,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "622b5ed0-d525-4496-ac99-3c450513c8b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 222,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1e3ae424-3157-49bc-b7c0-eeca24f30630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "293140db-05d6-4aa2-ac10-fb7cd9feb107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bd623b14-7fac-475d-af13-d704c577edfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 209,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4defe893-2349-4dba-b639-f8bcacb3f2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4f2d0f98-34fb-4584-be5f-e43e732dbb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 29,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3e0042dc-ed8e-4223-8e87-406903f1982d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 259,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2c621b68-9768-445c-bdd0-b745065e5539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 156,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "be84b22a-56dd-4d32-ae75-d1383fda499a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9b3e6152-eac7-48a7-9c6e-e3b144f06dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 159,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3e3d3a80-abcf-420f-ab4a-279db127107c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 411,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d6e1d58e-e51a-4882-b7de-93ada8c496a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 121,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1636c428-9615-4765-b99b-fb15d1df0eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "61745632-c650-48ca-8338-5cc01b4d501f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 438,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c37c4f54-1771-4cd8-b692-3f599d48c7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "748e22a6-1fb6-495b-9251-e4b8d966acff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 234,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0eb9cc95-38c5-41bf-98f8-7fed3c42d298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8a9feb9a-66e8-4cc8-909e-f9a91dea434a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 307,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b851995d-36ad-40cd-beaa-ebd3862e4457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c6e21651-47d6-4282-9b09-9048a51ea436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3ca6bf67-4f7e-49b2-a1c9-2ad50c4f2bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 55,
                "y": 50
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e3ae5fc8-02cb-4e8e-803c-f6fc78b25b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b1d7715a-7eb5-440d-a71c-0447ff16e4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 184,
                "y": 50
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a76a2a0c-a010-45be-9d70-57e370e044db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d74baf34-2850-4a03-807e-078c5a752028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "02cbe5b8-afe2-4289-9f47-b4376c4a8987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f1be4a8e-ff8c-49d9-8661-756b451ff9c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "08701e59-72f5-495d-8705-110f2812d0b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 133,
                "y": 50
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5440f7e0-2669-4ad7-ad78-263fa8f00386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 354,
                "y": 146
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4d4cc8e2-2027-4a68-92df-04911fdbf4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 185,
                "y": 146
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cf059c4a-f4d9-4c65-b1ad-5eee09e8ceb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 332,
                "y": 146
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7b47b2af-c66b-4e8a-883a-db89e3e7daf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 102,
                "y": 146
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a5cffc9c-1695-41af-8624-f7b4eff7991e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 81,
                "y": 50
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4d8afd52-097f-430d-9005-d2a4029a3ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 365,
                "y": 146
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "439fbb7c-902b-4c37-92ae-3c723a26828c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 90,
                "y": 98
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "13e64c68-c7af-473c-aa0c-711d455abf93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 459,
                "y": 98
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "17e06c32-84c5-44b0-a993-b9989a49a1ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 375,
                "y": 98
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4aed378a-6bac-4eb8-95ef-a66d14d6d390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 480,
                "y": 98
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4a990670-8c98-4b37-b289-be0c6e46b8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 421,
                "y": 50
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e2996fb6-8f74-48e0-8e0e-8eab3931603b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 155,
                "y": 146
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "12e6d5be-c106-4c6a-824c-2d5bc2e2e7e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 354,
                "y": 98
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cf3b54a2-8cfa-4c62-8763-16c33e0aae79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 62,
                "y": 146
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c7cd4fec-213f-41ac-a57e-96f7106d6788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 390,
                "y": 146
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "097e22fb-46d6-4606-b5bc-a560f50f8e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -2,
                "shift": 9,
                "w": 9,
                "x": 343,
                "y": 146
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "33c7e674-ce68-4ff9-9d88-f4a8feba2e1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 146
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d246c3bb-62d9-4fa1-86ae-f5da4dc24944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 383,
                "y": 146
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6bf52a7d-e6ea-4f98-a11f-fbe277a7c327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "35881a00-22c5-4861-a9bb-3152a215c15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "34b2faea-8016-42b0-b7ea-1f07ae8dfa69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 465,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e8003ce4-e83e-4d09-aebd-72c60751209c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "391e4260-f97f-43ef-892e-c9f2b7777113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 396,
                "y": 98
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8016ecf1-716a-4731-9e25-ca3b95a34a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 227,
                "y": 146
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7172b4b5-f289-4546-b92e-efa32610af6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 42,
                "y": 146
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c71f7d70-7650-46c8-9baa-5b10f85a9acd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 282,
                "y": 146
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7ffa0f7d-b52b-4561-8a91-3128e8bdb88c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 146
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fe706bb5-76db-433f-9713-f739fda1869d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 68,
                "y": 98
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "22380d7a-60e2-4db6-a3f8-ac6159add8ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c0e95c83-ce2a-4454-b129-b1e2f3f6a226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 310,
                "y": 98
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1e5ef4d0-9cdc-4d92-924e-64d3ec4473c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 288,
                "y": 98
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b58c98d7-646a-448d-98d4-30f71964e565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 332,
                "y": 98
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "508d428d-27d9-470c-a9b0-15a4cc607d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 199,
                "y": 146
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "06272173-3921-46b5-8cf4-962dc4a0326a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 439,
                "y": 146
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7ff96d95-609e-41a8-af39-09d523a4595f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 170,
                "y": 146
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1170b76b-ee9f-4311-9333-76b7c5b83b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 331,
                "y": 50
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "dd27b297-f501-4f70-b516-63195c9981ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "d3a85c89-fe8a-48d4-9f69-3e14133764bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "9b9cc3a5-0474-4c33-8c35-163f9008e165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "ca8a7da8-dafe-4162-a3f1-72be9fadfee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "38e89801-16e9-4ec4-a733-beafb238fa59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "bc5c72fb-cff6-4a66-900d-cda814747158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "094feb72-1387-4fe5-ab23-b98a63d3c326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "ed9dd460-0861-4897-a2a1-12145b9879d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "204f96d4-1f28-4ba2-9a4e-4987e9d2b711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "a2c63738-c1c2-443b-bed6-ba7a0f02593f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "b3f59d1a-f1c0-4f27-8811-e69d0feb5ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 49
        },
        {
            "id": "fb23e1da-11c6-4631-aec6-31780069fbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "16a47c29-6d0c-4230-8cdb-10a6a400ac4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "19c623ea-3a25-4b6e-a346-84573907f87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "e0680e6f-27d9-498a-83c3-1e229ad4c31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "c2d45cc3-cca9-488e-bdf9-63ce6d6e8ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "b7a4f36a-19aa-4192-82f2-9fb075251602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "0555edef-ab9b-4e24-a1e4-6c6b51c3f8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "f93bf813-9d6a-415e-87c2-8af80c25ddc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "de41689e-2056-4673-b2cb-8b98fe6a7e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "b638575a-16ac-449b-986f-cab7af724177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "6f36aaa9-bc09-4930-aef3-5e36229415de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "e022ec56-e959-463a-a693-1292bdada6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "0149c55c-1232-42b6-8cf3-7f79bdcb3212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "412cbe81-bbbc-4af6-b26b-d35e8df88e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "07d577bb-7f75-4a50-93ee-093f1944bde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "d3e83de5-ccc5-4136-8a67-9cf4900f306b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "cae564a5-77f8-4561-98ea-349e7ee4a7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "81de7811-740a-45ec-bbd1-28de56ef1a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "d3f8f3c4-1148-445e-8871-8fd60bd2fc5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "42855fb5-56bf-403d-b71f-80d5a127600a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "86612e66-16f5-45a8-a8cf-f66270afa29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "cbc774b7-385d-4519-b990-17351ac830c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "871636ed-c967-48a2-9af1-3841f6aa816a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "bc07de10-5589-4174-8962-362e9285c0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "292cfada-9b04-4ff6-a7f7-3817740206b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "f388625c-2a2b-4742-9859-fab67eecdc44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "f143314f-47f4-4df0-aa44-f36d30089c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "c732fcce-4013-4677-88bc-23e71ab793a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "eec82a77-149b-49cb-ae48-d2dc7ddb1668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "30ab6499-bdcc-49cb-befa-c62a665dfdc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "f414b526-abcc-40be-8d39-9d7c3d38c864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "385f4cb2-184f-41d9-a940-bd49e9fe2536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "93315139-8e91-4b9a-ba6f-87fcfb6d8908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "83591f45-d788-4343-af0c-93a4e8192c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "e97e1954-f9c6-4560-aa4b-af51675a117a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "b3516e53-95c9-4dff-8382-26d04f9c64f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "024b76f4-312a-486a-afdc-974240ca4eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "cfbbc272-8ef6-4cf6-b991-63e339ee7081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "29d4023f-49ca-4e0a-b03c-e293c3515422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "4e51f974-e7e1-48b8-89b0-97f6533fec59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "a4d3186a-2ced-4c20-999b-09e70441fe26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "e46b20ab-04ad-4608-ba56-cb050e33d8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "53521092-b59d-4e8a-82e5-2885d2b744df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "9ebdfe41-7684-47d5-9b57-7902cb38c958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "2b6b2981-5fb4-4c77-b1b0-e0944299c21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "6d20bba3-b3a7-4340-8c14-71a685ca7033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "9a70c6ad-62b9-49c9-847a-fcc61d75833b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "be37f8cf-f932-4a3e-95b8-d0c3c29d6d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "808ae7ff-f9d2-46f8-9dbe-7bd3a1c9358f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "d663da3d-d5be-4a62-b011-eb598ffc6163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "7dbc7396-4987-4fa9-8808-db4cc40ebab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "079ed86a-4e23-4f75-ade7-51b6ac86af3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "2909ba9c-79df-4782-a3d1-420b7afd1c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "85bd17e6-2105-4e9f-a231-ef50f9d46887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "1801e40f-a635-4e83-be24-574fca87c883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "e5cd3ec2-5821-4394-8f80-6933f55aedb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "36c32440-aeed-44dc-b5f4-a273c61543a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "3845e087-7859-4304-9701-ffd7daa3d130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "574da413-03fe-4023-804d-c0bf94e9ff4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "01a196a8-1902-4063-ad80-a86dbd0d38d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "1ede5ec0-fbee-42e7-808b-877aca4d19ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "e40e51a6-a006-45d3-96c9-74257d8c46a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "8032780a-32cf-4913-9d9a-abe475586bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "8628938a-35a8-4524-afa3-ac0f5f7b8031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "0ad0e172-0314-4425-a1c3-1d2cb4272ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "a8b22bd5-169e-4d6f-996d-b9b652bf90a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "038f2940-1067-417c-b405-f1adf284b1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "c5a3aec2-cda2-42d7-9af3-de118eba8994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "f5e96661-fc5e-4de1-b2bd-d9f03454cdb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "b80ea085-a1e6-4dfb-a690-991a35ba20a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "319a1ef6-bbc0-4243-a348-c11f6eb35d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "f7bb2088-3f03-4404-a036-acbfc685fb35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "757fec87-6c43-4523-9c5b-8fba5ca90d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "ea727f49-39d1-4968-85f8-2ac0aa12a265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "cb1ad978-c315-4be3-b897-3fd3b695d437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "d78bc275-cf25-4ed7-ae93-2cbd004077fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "5de8b305-35d7-49cb-8dc5-6f71e623e42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "afcbcb17-0e03-40bb-8252-f6a95accd078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "66bcb03f-864d-4e96-971c-58a14a0864f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "b30df9b6-efa3-4ead-aa7e-6abdc52dc54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "20384b66-60b8-4640-868b-ad5c49b2ce01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "c36ecd54-7bf8-4a06-b2b3-93b0343dbb19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "18c26a1a-a74d-42f9-9aa6-8569c851d16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "902778b3-2191-4d8e-9ad5-5c1d9e4b4528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "f9f7641f-de89-4db9-840c-25697ebf576c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "a7eb21a5-44fa-4b1a-801a-b4f86edb559b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "46ae3bcf-e2b9-464e-90ca-520cd19b7a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "d57282f0-e1d6-4e33-b9b5-b7ede4a4b7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "57dc9742-39f0-4e75-9fd7-b2826d38d1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "14e62081-1455-4ffc-a0e9-a253b1a706a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "6434916f-41ae-45ea-8c72-67b8bf55c491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "ce15558a-30de-4df1-b359-5444369666c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "7c88cf82-c552-46f5-aa83-6a98557cf5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "46da6ac3-308f-4e88-ab96-40e3a480c064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "07cbf0fc-5789-4da4-961e-3c9e428b9fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "bf227150-ab61-4399-9caf-cb65d2d95a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "7149afec-f40f-4a96-a1f5-bea5b5ea64fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "a998a82b-27a1-447b-bd85-36fe77b2055f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "2e1a7d40-2121-4bec-a5d9-fb0dc4b3973b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "8b23839a-82c4-463f-88d3-b21a3a3318d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "0cc7b74b-8869-48ee-a391-3c493ff73eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "34ac9b26-9db2-4a84-93f0-e3523c42db7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "3c8e1d60-8d9d-4a20-b79b-e05de444f9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "0de49c58-8242-4815-80ca-262b03521f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "6796c208-3c16-4b92-b114-34ce55b4a78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "363dd123-ba6a-4523-8e4e-efb87b4b8f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "5a59e56b-b6c2-4552-b17a-a4817f8ac29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "57bf4a89-c83e-4328-8413-eb9005d524b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}