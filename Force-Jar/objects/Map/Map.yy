{
    "id": "c4974b85-2b3c-4a12-8462-0efec402a584",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Map",
    "eventList": [
        {
            "id": "3cc45171-a783-4595-8479-b69b1b4817ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4974b85-2b3c-4a12-8462-0efec402a584"
        },
        {
            "id": "ec5404dc-bb26-4762-8de3-a115ad5b5cc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c4974b85-2b3c-4a12-8462-0efec402a584"
        },
        {
            "id": "4f31c8a7-2108-4b33-995e-379b72672701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c4974b85-2b3c-4a12-8462-0efec402a584"
        },
        {
            "id": "de20dbcc-44e2-445f-8eff-11fb2b42a26e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c4974b85-2b3c-4a12-8462-0efec402a584"
        },
        {
            "id": "5492b366-4e44-4969-8b1c-87914bb79442",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c4974b85-2b3c-4a12-8462-0efec402a584"
        },
        {
            "id": "8f9eb7e4-8a30-4fd9-8c7e-c2c2120315c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c4974b85-2b3c-4a12-8462-0efec402a584"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}