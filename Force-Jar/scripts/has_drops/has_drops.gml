/// @func has_drops(instance)
/// @desc Checks if the instance has the drops facet
/// @param {Real} instance the id of the instance

var instance = argument0;

return variable_instance_exists(instance, "has_facet_drops");