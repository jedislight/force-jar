// Standard Damage Tables should be defined here.

// Standard Enemy Table
find_damage_table(DM_DEFAULT_ENEMY);
var standard_enemy_map = global.damage_data_map[? DM_DEFAULT_ENEMY];
standard_enemy_map[? DM_FACTION] = Factions.ENEMIES;