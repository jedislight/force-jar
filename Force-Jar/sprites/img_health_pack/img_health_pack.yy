{
    "id": "bcdef3bf-238a-463c-8ee8-745a53c8f0cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_health_pack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "302e4a1d-2920-4710-bb53-e93b3131c141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcdef3bf-238a-463c-8ee8-745a53c8f0cd",
            "compositeImage": {
                "id": "b4d649d8-ae45-4d60-9b04-6185b5ad6a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302e4a1d-2920-4710-bb53-e93b3131c141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054e381f-fd32-4181-aa35-d0b1d97777c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302e4a1d-2920-4710-bb53-e93b3131c141",
                    "LayerId": "f60b9941-f957-4801-b26d-1fc7abb37d56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f60b9941-f957-4801-b26d-1fc7abb37d56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcdef3bf-238a-463c-8ee8-745a53c8f0cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}