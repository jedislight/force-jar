/// @function respawn_later_as(prev_object_id, countdown)
/// @desc Takes an object and turns it into a RegenerationTimeout object
/// @param {Real} prev_object_id object_id of the object to respawn as after countdown.
/// @param {Real} countdown frames the object is intended to remain inactive.

assert_fatal(object_exists(argument0),
	"Object id " + string(argument0) + " does not correspond to an object in the resource tree, cannot set to respawn");

prev_object_id = argument0;
countdown = argument1;

assert(is_real(countdown),
	string(countdown) + " is not a number.  Setting timer to default time.");

if (global.last_assert)
{
	// Set to one second of the game's target frame time
	// this is not precise so is not recommended as a fallback.
	countdown = game_get_speed(gamespeed_fps);
}

instance_change(RegenerationTimeout, false);