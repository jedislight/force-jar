{
    "id": "16a628eb-d621-48fc-b47e-7de758f634d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "StartGameMenuItem",
    "eventList": [
        {
            "id": "e93fc0b2-ff17-4284-8748-1dd0d8e2d8ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "16a628eb-d621-48fc-b47e-7de758f634d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ae82b777-fbe4-48cf-9766-8118f3f75409",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}