/// @description Disable render of application surface by engine

application_surface_draw_enable(false);

post_effect_shaders = ds_list_create();
ds_list_add(post_effect_shaders, shd_default);
ds_list_add(post_effect_shaders, shd_grayscale);
ds_list_add(post_effect_shaders, shd_gray_tone);
ds_list_add(post_effect_shaders, shd_toon);
ds_list_add(post_effect_shaders, shd_squint);
ds_list_add(post_effect_shaders, shd_overbright);

post_effect_shader_index = 0;