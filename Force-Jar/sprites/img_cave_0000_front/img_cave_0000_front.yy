{
    "id": "3d1a41ad-3399-473b-8506-83d1713d4d31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_cave_0000_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3999,
    "bbox_left": 0,
    "bbox_right": 3999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afc5760f-2be4-40b3-9d29-906c3ccc3562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d1a41ad-3399-473b-8506-83d1713d4d31",
            "compositeImage": {
                "id": "a784d0aa-c29d-4c11-b671-0000c4043841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afc5760f-2be4-40b3-9d29-906c3ccc3562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3f1f0e7-f253-4215-8a61-6fda6509cd63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc5760f-2be4-40b3-9d29-906c3ccc3562",
                    "LayerId": "5b0ef6be-9421-4734-86a1-63b1d5922fb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4000,
    "layers": [
        {
            "id": "5b0ef6be-9421-4734-86a1-63b1d5922fb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d1a41ad-3399-473b-8506-83d1713d4d31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4000,
    "xorig": 0,
    "yorig": 0
}