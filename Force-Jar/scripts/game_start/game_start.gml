/// @function game_start()
/// @desc Contains global/game level declarations.

gml_pragma("global", "game_start()");

// If not debugging, randomise to get different random numbers.
if (not debug_mode)
{
	randomise();	
}

// Enum Declarations ==============================================================================
// ================================================================================================

enum DoorDirection {
	NORTH = 90,
	SOUTH = 270,
	WEST = 180,
	EAST = 0,
};

// Values used with Renderer to differ objects to draw.
enum DrawLayer {
	DEFAULT,
	TRANSITION_EFFECT,
	DRAW_ALWAYS,
	END_LAYER, // Never use this, this should always be at the end.
};

// power ups
enum Powers {
	POWERS_START = 0,
	GRAPPLE = 0,
	RAPIER,
	LEVITATE_MASS,
	HI_JUMP,
	STAFF,
	POWERS_END,
}

enum Factions {
	UNIVERSAL = 0, // If no faction indicated, hits everything and is hit by everything.
	PLAYER,
	ENEMIES,
	TERRAIN,
}

// power up states
enum PowerStates{
	DISABLED,
	ON,
	OFF,
}

// ==============================================================================================
// Global Declarations
// ==============================================================================================
global.damage_data_map = ds_map_create();
global.active_menu_items = ds_map_create();
global.last_assert = false;
global.next_room_to_load = init;
global.input_manager = undefined;

// ==============================================================================================
// Constants Declarations
// ==============================================================================================

#macro DM_FACTION "Faction"
#macro DM_DEFAULT_ENEMY "StandardEnemy"

#macro LR_TILES "Tiles"
#macro LR_TILES_FRONT "Tiles_Front"
#macro LR_TRANSITION "Transitions"

#macro MI_EVENT 0

#macro assert_exit if (global.last_assert) exit;

define_damage_table();
define_drop_tables();