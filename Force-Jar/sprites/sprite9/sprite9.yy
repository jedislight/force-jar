{
    "id": "b95fc70f-0b83-44d7-957c-03a0302da005",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 860,
    "bbox_left": 64,
    "bbox_right": 701,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01fc2e48-58f2-44fc-a189-6251e395e5ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b95fc70f-0b83-44d7-957c-03a0302da005",
            "compositeImage": {
                "id": "635d5fb4-3fdb-4cbd-8d5b-c2c9f8393795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fc2e48-58f2-44fc-a189-6251e395e5ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2491657b-7312-4489-8832-f3f39b00d209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fc2e48-58f2-44fc-a189-6251e395e5ca",
                    "LayerId": "6c6afea1-5bf2-4e63-aab3-d36a06751a6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 896,
    "layers": [
        {
            "id": "6c6afea1-5bf2-4e63-aab3-d36a06751a6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b95fc70f-0b83-44d7-957c-03a0302da005",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 704,
    "xorig": 0,
    "yorig": 0
}