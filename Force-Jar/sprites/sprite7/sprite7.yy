{
    "id": "0195675d-7d94-46f4-8583-c923ce6f2ce0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0898089a-616a-4d4b-aa35-6ba9860b8aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0195675d-7d94-46f4-8583-c923ce6f2ce0",
            "compositeImage": {
                "id": "ea0f97bf-5d78-4114-9cd8-70e72d74bfaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0898089a-616a-4d4b-aa35-6ba9860b8aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63884bd1-bed4-421f-85c5-16a6cb2b35b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0898089a-616a-4d4b-aa35-6ba9860b8aeb",
                    "LayerId": "a8155ad5-e855-443d-b615-3050587911ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8155ad5-e855-443d-b615-3050587911ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0195675d-7d94-46f4-8583-c923ce6f2ce0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}