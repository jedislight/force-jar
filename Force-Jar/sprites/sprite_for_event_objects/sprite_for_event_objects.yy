{
    "id": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_for_event_objects",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d90970d8-589b-4728-9fa5-3c464c64d629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
            "compositeImage": {
                "id": "ea86764c-8732-4caf-8b91-4d07f55ceccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90970d8-589b-4728-9fa5-3c464c64d629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7e0009-b7e3-4b62-b8ee-de377b890790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90970d8-589b-4728-9fa5-3c464c64d629",
                    "LayerId": "eaffe58f-d142-4988-8d50-fde90675ae3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eaffe58f-d142-4988-8d50-fde90675ae3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}