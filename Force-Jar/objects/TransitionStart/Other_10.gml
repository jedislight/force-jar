/// @description State Forward

show_debug_message("Transition Changing State!");

// If not, start finding a new queue.
var next_transition_obj = undefined;
while (next_transition_obj == undefined)
{
	// If the queue is empty, destroy it.
	if (is_undefined(ds_queue_head(transition_queue)))
	{
		transition_done = true;
		instance_destroy();
		return;
	}
	
	next_transition_obj = ds_queue_dequeue(transition_queue);
	
	show_debug_message("Next state may be " + object_get_name(next_transition_obj));
	
	if (!object_is_ancestor(next_transition_obj, TransitionStart))
	{
		next_transition_obj = undefined;
	}
}

// Calls Create to allow this to be used by child objects, be mindful of this.
show_debug_message("Switching to state " + object_get_name(next_transition_obj));
instance_change(next_transition_obj, true);