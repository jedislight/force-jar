/// @description adjust ttl

--ttl;
if(ttl < 90)
{
	image_alpha = min(abs(sin(ttl*.2)), ttl/90);
}

if(ttl <= 0)
{
	instance_destroy();	
}
