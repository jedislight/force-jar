/// @function single_attack_on_press(weapon_data, attack_pressed, attack_held, attack_released, facing)
/// @description Attacks once with a weapon's damage object on key press.
/// @param {WeaponData} 

var weapon_data = argument0;
var attack_pressed = argument2;
var attack_held = argument1;
var attack_released = argument3;
var facing = argument4;

assert(object_is_ancestor(weapon_data.object_index, WeaponDataBase), "No weapon data supplied, arg zero is of type " + object_get_name(weapon_data.object_index));
assert_exit

if (not variable_instance_exists(weapon_data, "attack_object"))
{
	weapon_data.attack_object = noone;
}

if (not instance_exists(weapon_data.attack_object))
{
	weapon_data.attack_object = noone;
}

if (attack_pressed and weapon_data.attack_object == noone)
{
	weapon_data.attack_object = instance_create_layer(weapon_data.owner.x + weapon_data.xoffset, weapon_data.owner.y + weapon_data.yoffset, "Instances", weapon_data.damage_object_data);
	weapon_data.attack_object.owner = weapon_data.owner;
	weapon_data.attack_object.direction = facing;
	weapon_data.attack_object.xoffset = weapon_data.xoffset;
	weapon_data.attack_object.yoffset = weapon_data.yoffset;
	weapon_data.attack_object.faction = weapon_data.owner.faction;
}