{
    "id": "084d3b90-2ff0-4360-8e2c-282a32c55a32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "HealthDropSmall",
    "eventList": [
        {
            "id": "152c0d68-52da-48e7-88fd-fd41ff1fb73d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "380edad1-487c-4192-bac0-65401ddf29b8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "084d3b90-2ff0-4360-8e2c-282a32c55a32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "74568293-1815-43cb-b5b3-0475d48729ba",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "10",
            "varName": "health_value",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "ae101fb3-b9d0-4536-a355-c7333efb2843",
    "visible": true
}