
{
    "name": "game_over_room",
    "id": "533f7adf-4260-46d5-b071-a9652cf730d4",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "d94f4e90-8aaf-4985-a991-831c6627b98f",
        "0d8a3a6c-9120-4296-ab85-68550013d83c",
        "4dc36174-745c-47d3-a470-7b98c9b901af"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "TITLE",
            "id": "2c4c907b-3297-4bba-9dac-f449cf278d77",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_FF878D1","id": "b72f7046-e62c-4089-a47c-de5c82502714","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 4.2,"scaleY": 4.2,"mvc": "1.0","spriteId": "a92cec87-97d3-4789-a340-282ed40b28a2","userdefined_animFPS": false,"x": 1280,"y": 320}
            ],
            "depth": 0,
            "grid_x": 64,
            "grid_y": 64,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "94df1f5d-48ed-4ce2-9125-580d568f6f6e",
            "depth": 200,
            "grid_x": 64,
            "grid_y": 64,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "Restart_From_Checkpoint_Item","id": "d94f4e90-8aaf-4985-a991-831c6627b98f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "Restart_From_Checkpoint_Item","objId": "16a628eb-d621-48fc-b47e-7de758f634d2","properties": [{"id": "66409c42-ef75-4544-b22e-ab74cf040593","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "RESTART FROM CHECKPOINT"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1600,"y": 1344},
{"name": "Return_To_Menu_Item","id": "0d8a3a6c-9120-4296-ab85-68550013d83c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "Return_To_Menu_Item","objId": "b754f75d-1780-4643-9f8c-7e19d8eabfc1","properties": [{"id": "7a3502ff-9f12-419d-8553-37908e8451b2","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "RETURN TO MENU"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1600,"y": 1600},
{"name": "Exit_To_Desktop_Item","id": "4dc36174-745c-47d3-a470-7b98c9b901af","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "Exit_To_Desktop_Item","objId": "529bd6ae-171c-4a1f-866b-edc13f110737","properties": [{"id": "a93c7454-ac22-4212-905d-1a1ea24f95c2","modelName": "GMOverriddenProperty","objectId": "ae82b777-fbe4-48cf-9766-8118f3f75409","propertyId": "5f7935d0-6f95-44dc-a4a3-09662c20db83","mvc": "1.0","value": "EXIT TO DESKTOP"}],"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 1600,"y": 1856}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": true,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "29b36d8f-6a79-42b6-bad1-c1a42d9b664c",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 300,
            "grid_x": 64,
            "grid_y": 64,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "3f3ea47c-5e2a-41f0-9b1e-7b6b2767ad1f",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "4a989cc5-2064-4142-b47d-0345086022e4",
        "Height": 2160,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 3840
    },
    "mvc": "1.0",
    "views": [
{"id": "47c7aaba-4098-4be3-bb6b-24923237a6a4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5609b3b8-1429-4c25-84a4-d05d3fa60746","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f7a635da-a932-4ec1-8f00-4e957260fcf8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "77a8ad4e-1de0-4d1d-a11f-bae9e3a69c3f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fe138ddc-348e-439a-8f74-2bf85443a911","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e2916a8d-f915-487e-ae5f-5e5e8321a790","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ebc7c693-0bbb-418b-a532-6811b1bb2370","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e4e5f483-545f-41a5-af93-c12f9731f445","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "77c9589a-f577-4799-917d-45009c645841",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}