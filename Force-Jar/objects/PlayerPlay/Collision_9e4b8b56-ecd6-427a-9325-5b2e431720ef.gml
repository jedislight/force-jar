/// @description Check Damage Object

var damage_taken = take_damage(other.id, id, damage_table_key);

if (damage_taken == 0)
{
	exit;	
}

health_change(id, -damage_taken);

last_player_state = object_index;
iframe_countdown = 45;
current_alpha = 1.0;

var ox = other.x
var oy = other.y
knock_back(id, ox, oy);


instance_change(PlayerInvincibleFrames, false);