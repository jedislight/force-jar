{
    "id": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_staff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 2,
    "bbox_right": 188,
    "bbox_top": 76,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8b7a0fb-8ac8-45a8-a08a-5750a3b3c150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
            "compositeImage": {
                "id": "dec8c3f2-8494-47c4-9112-8f8e1e6ca453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b7a0fb-8ac8-45a8-a08a-5750a3b3c150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34a8315d-adde-494b-a55a-b8e0a2c333eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b7a0fb-8ac8-45a8-a08a-5750a3b3c150",
                    "LayerId": "acd97dfe-014e-48df-bfa7-9c2f72df6709"
                }
            ]
        },
        {
            "id": "8aa003ce-057e-4c70-90eb-1097ed27aaf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
            "compositeImage": {
                "id": "284a1064-94cf-4418-aabc-74ad0b70dd02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa003ce-057e-4c70-90eb-1097ed27aaf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a76137bb-605f-45c3-b8a7-d633ae6ec911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa003ce-057e-4c70-90eb-1097ed27aaf8",
                    "LayerId": "acd97dfe-014e-48df-bfa7-9c2f72df6709"
                }
            ]
        },
        {
            "id": "c0061b23-ec2c-4c8f-ada6-313bc7218095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
            "compositeImage": {
                "id": "d0b193c5-3d80-4053-9321-33e007c01175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0061b23-ec2c-4c8f-ada6-313bc7218095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba06d240-45ed-49c9-9c94-34928778b3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0061b23-ec2c-4c8f-ada6-313bc7218095",
                    "LayerId": "acd97dfe-014e-48df-bfa7-9c2f72df6709"
                }
            ]
        },
        {
            "id": "b7457108-babe-4b16-b08d-97a713542134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
            "compositeImage": {
                "id": "37dcd40c-b7cf-423b-b726-cfbe5ad70c54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7457108-babe-4b16-b08d-97a713542134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed938fae-e78b-4375-b4d0-639391324100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7457108-babe-4b16-b08d-97a713542134",
                    "LayerId": "acd97dfe-014e-48df-bfa7-9c2f72df6709"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "acd97dfe-014e-48df-bfa7-9c2f72df6709",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4281534720,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}