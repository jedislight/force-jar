/// @description Cycle and Change

if (image_speed > 0 && image_index == image_number - 1)
{
	respawning = true;
	respawn_later_as(CrumbleTerrain, respawn_timeout);
	return;
}
else if (image_speed < 0 && image_index == 0)
{
	if (collision_rectangle(x+1, y+1, x+sprite_get_width(sprite_index)-1, y+sprite_get_height(sprite_index)-1, all, false, true) != noone)
	{
		image_speed = 1;
		return;
	}
	
	image_speed = 0;
	solid = true;
}
else
{
	if (collision_rectangle(x+1, y-1, x + sprite_width - 1, y, PlayerStates, false, true) != noone)
	{
		image_speed = 1;
		tilemap_clear_tile(0);
		tilemap_clear_tile(-1);
		if (collision_rectangle(x+1, y+sprite_height, x + sprite_width - 1, y+sprite_height+1, all, false, true) == noone)
		{
			tilemap_clear_tile(1);		
		}
	}
}