/// @description Get the Keypresses

vertical_axis_pressed = global.input_manager.vertical_input_pressed;
horizontal_axis_pressed = global.input_manager.horizontal_input_pressed;

confirm_pressed = global.input_manager.confirm_input_pressed;
cancel_pressed = global.input_manager.cancel_input_pressed;