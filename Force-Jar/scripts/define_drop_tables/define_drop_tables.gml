// The first value in the Drop Table Grid to access a set of subjects.
enum DropTableKeys
{
	FIRST = 0, // FIRST and the actual first value should be the same.
	NONE = 0,
	DEFAULT,
	BOSS,
	HEALTH_ONLY,
	END // END must always be the last part of the Drop Table Keys
}

// The second value in the Drop Table Grid to get a real value.
enum DropTableOptions
{
	FIRST = 0, // FIRST and the actual first value should be the same.
	HEALTH = 0,
	END // END must always be the last part of the Drop Table Options
}

global.drop_tables = ds_grid_create(DropTableKeys.END, DropTableOptions.END);
global.drop_objects = ds_list_create();

// Define drop_objects
global.drop_objects[| DropTableOptions.HEALTH] = HealthDropSmall;

// Define NONE (no drops)
global.drop_tables[# DropTableKeys.NONE, DropTableOptions.HEALTH] = 0.0;

// Define DEFAULT (all drops at standard chance)
global.drop_tables[# DropTableKeys.DEFAULT, DropTableOptions.HEALTH] = 40.0;

// Define BOSS (for drops from bosses)
global.drop_tables[# DropTableKeys.BOSS, DropTableOptions.HEALTH] = 90.0;

// Define HEALTH_ONLY (only health)
global.drop_tables[# DropTableKeys.HEALTH_ONLY, DropTableOptions.HEALTH] = 60.0;