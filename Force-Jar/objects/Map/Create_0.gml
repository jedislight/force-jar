/// @description Insert description here
// You can write your code in this editor

enum MapCellStates
{
	EMPTY,
	UNEXPLORED,
	EXPLORED,
}
enum MapCellBorders
{
	NONE = 0,
	UP = 1,
	LEFT = 2,
	RIGHT = 4,
	DOWN = 8,
	ALL = 1 | 2 | 4 | 8
}

map_state_grid = ds_grid_create(100,100);
map_border_grid = ds_grid_create(100,100);
map_door_grid = ds_grid_create(100,100);

ds_grid_clear(map_state_grid, MapCellStates.EMPTY);
ds_grid_clear(map_border_grid, MapCellBorders.NONE);
ds_grid_clear(map_door_grid, MapCellBorders.NONE);

x = 50; //map starting cordinates
y = 50;

anchor_room=true;
mini = true;

map_world_size_x = 3840/2;
map_world_size_y = 2160/2;