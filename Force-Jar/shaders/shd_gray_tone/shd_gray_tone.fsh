//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
	vec4 sampled = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	float tone = min(floor(max(max(sampled.r, sampled.g), sampled.b) * 4.0)/4.0, 1.0);
	
	vec4 grayscale = vec4(tone, tone, tone, sampled.a);
    gl_FragColor = grayscale;
}
