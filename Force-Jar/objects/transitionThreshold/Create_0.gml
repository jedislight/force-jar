/// @description Sanity check instance

transition_start = noone;

if (door_offset > 8)
{
	show_debug_message("Threshold Instance " + instance_id + " has an offset greater than 8, why?")
}