/// @description Delete the weapon if needbe

if (timeAlive >= timeToLive)
{
	instance_destroy();	
}


timeAlive += 1;
if (owner != noone)
{
	var thrust_paramater = (timeAlive/timeToLive) * 2;
	depth = owner.depth - 1
	if(thrust_paramater > 1.0)
	{
		thrust_paramater = 2.0 - thrust_paramater;
		depth = owner.depth + 1
	}
	var x_thrust_length = lengthdir_x(thrustDistance * 64 * thrust_paramater, direction);
	var y_thrust_length = lengthdir_y(thrustDistance * 64 * thrust_paramater, direction);
	
	x = owner.x + xoffset + x_thrust_length;
	y = owner.y + yoffset + y_thrust_length;
}