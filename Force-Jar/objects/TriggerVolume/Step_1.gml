/// @description Trigger Detection
// You can write your code in this editor

var collision_state = instance_place(x,y, obj);

if(collision_state != previous_collision_state)
{
	if(previous_collision_state == noone)
	{
		event_user(0);
	}
	else
	{
		event_user(1);
	}
}

if(instance_exists(collision_state))
{
	event_user(2);
}

previous_collision_state = collision_state;