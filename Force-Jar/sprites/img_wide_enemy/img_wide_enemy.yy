{
    "id": "f581a497-5ecf-4ac7-8ce5-ef97f1979b56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_wide_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab6143fa-45cf-48e5-bb6f-fd237a864fe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f581a497-5ecf-4ac7-8ce5-ef97f1979b56",
            "compositeImage": {
                "id": "788a5878-0dff-4219-b879-5f0b27f5756d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab6143fa-45cf-48e5-bb6f-fd237a864fe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "203923e8-c076-45d5-94c6-6df449fb1065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab6143fa-45cf-48e5-bb6f-fd237a864fe9",
                    "LayerId": "0d172fb1-97c2-4071-9602-dd574735543d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0d172fb1-97c2-4071-9602-dd574735543d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f581a497-5ecf-4ac7-8ce5-ef97f1979b56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 0
}