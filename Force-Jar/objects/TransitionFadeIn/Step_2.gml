/// @description Conditional Call Forward

if (fade_overlay.merge_amt <= 0.0)
{
	if(music_transition)
	{
		music_manager_set_volume(1.0);
		music_manager_play(music);
	}
	
	instance_destroy(fade_overlay);
	
	// Call next transition.
	event_user(0);
}
else
{
	if(music_transition)
	{
		music_manager_set_volume(fade_overlay.merge_amt)
	}
}