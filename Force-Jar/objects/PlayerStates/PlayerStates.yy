{
    "id": "380edad1-487c-4192-bac0-65401ddf29b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PlayerStates",
    "eventList": [
        {
            "id": "a6f1161d-b211-4737-8310-750d9f343560",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "380edad1-487c-4192-bac0-65401ddf29b8"
        },
        {
            "id": "9df2599e-65fb-4d63-be11-f9902f87d2a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "380edad1-487c-4192-bac0-65401ddf29b8"
        },
        {
            "id": "d4df19e6-680a-485a-a10d-c391ea215ee5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "380edad1-487c-4192-bac0-65401ddf29b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "557d2127-79dc-4adf-b7cc-42c6001362c7",
    "visible": true
}