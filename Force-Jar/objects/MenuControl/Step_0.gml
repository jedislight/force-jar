/// @description Run Cycles

var control_axis_value = (use_horizontal) ? horizontal_axis_pressed : vertical_axis_pressed;

if (control_axis_value != 0)
{
	with (ds_list_find_value(menu_item_list, selected_item))
	{
		item_selected = false;	
	}
	
	selected_item = selected_item + control_axis_value;
	if (selected_item < 0)
	{
		selected_item = ds_list_size(menu_item_list) + control_axis_value;	
	}
	selected_item = abs(selected_item % ds_list_size(menu_item_list));
	
	with (ds_list_find_value(menu_item_list, selected_item))
	{
		item_selected = true;	
	}
	
	exit;
}

if (confirm_pressed)
{
	with (ds_list_find_value(menu_item_list, selected_item))
	{
		event_user(MI_EVENT);	
	}
}

if (cancel_pressed)
{
	// Set cancel code here for submenus.
}