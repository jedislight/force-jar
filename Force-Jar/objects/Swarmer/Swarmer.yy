{
    "id": "437abf02-e026-40dc-8efd-858aca6e5392",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Swarmer",
    "eventList": [
        {
            "id": "a84ec088-d5ba-4d5e-93d6-6aa32eac2958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "437abf02-e026-40dc-8efd-858aca6e5392"
        },
        {
            "id": "29d82c4e-c328-4cd1-ab56-84bdab33f62c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "437abf02-e026-40dc-8efd-858aca6e5392"
        },
        {
            "id": "e9ff5f65-59d4-43e8-a79e-266c26ea9350",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "437abf02-e026-40dc-8efd-858aca6e5392"
        },
        {
            "id": "9c4028b8-e0c0-430b-a278-caf7f0471a1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "437abf02-e026-40dc-8efd-858aca6e5392"
        },
        {
            "id": "92eed114-8a6a-43fd-9bf5-a54d255c59f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "437abf02-e026-40dc-8efd-858aca6e5392"
        },
        {
            "id": "d66e82df-66c2-4d4b-a2ef-53169a410d29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "437abf02-e026-40dc-8efd-858aca6e5392",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "437abf02-e026-40dc-8efd-858aca6e5392"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "5ba7beb1-1950-44a9-b97f-43b4fb853c5d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
            "propertyId": "20aca9f3-ef56-4faf-bb9e-13400362483b",
            "value": "1"
        },
        {
            "id": "5cb94a7a-de60-4302-ac09-5b13247c2165",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
            "propertyId": "e15d3949-d656-4428-956b-9ef580b20b9b",
            "value": "0"
        }
    ],
    "parentObjectId": "9d2d8481-2542-4e15-a7eb-b0925e3d196d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
    "visible": true
}