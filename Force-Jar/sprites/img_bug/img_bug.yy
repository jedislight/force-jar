{
    "id": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_bug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbef721e-a2f8-4ff8-b72f-fa95f22e17eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
            "compositeImage": {
                "id": "b6273d00-f5a4-4da8-91cd-1dc69136a5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbef721e-a2f8-4ff8-b72f-fa95f22e17eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0365d062-c424-41e9-9975-fd522a9ad1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbef721e-a2f8-4ff8-b72f-fa95f22e17eb",
                    "LayerId": "00bbd49f-bac3-4684-bfca-a46e3ca18745"
                }
            ]
        },
        {
            "id": "ef1b1fdd-0d26-4354-a7ca-6b93861a66bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
            "compositeImage": {
                "id": "a0a21e0d-b802-47a6-b283-f2dc1f16018f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef1b1fdd-0d26-4354-a7ca-6b93861a66bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd2e39d8-034e-4d54-869a-2613af9984ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef1b1fdd-0d26-4354-a7ca-6b93861a66bb",
                    "LayerId": "00bbd49f-bac3-4684-bfca-a46e3ca18745"
                }
            ]
        },
        {
            "id": "f48a4b45-d04b-4b6c-861a-381592f8d4eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
            "compositeImage": {
                "id": "e004bd12-68a3-4941-b43a-93f6222bd33f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f48a4b45-d04b-4b6c-861a-381592f8d4eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1294aad1-ad4d-442b-9a3c-febdfe5d452e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f48a4b45-d04b-4b6c-861a-381592f8d4eb",
                    "LayerId": "00bbd49f-bac3-4684-bfca-a46e3ca18745"
                }
            ]
        },
        {
            "id": "39fd62ac-2b83-4b96-b126-f163706f8ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
            "compositeImage": {
                "id": "747fb3fa-60de-44ec-ba53-bb23e52ef0a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39fd62ac-2b83-4b96-b126-f163706f8ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e2b863-d1e9-4a51-8d0a-80d4803f88f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39fd62ac-2b83-4b96-b126-f163706f8ccf",
                    "LayerId": "00bbd49f-bac3-4684-bfca-a46e3ca18745"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00bbd49f-bac3-4684-bfca-a46e3ca18745",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e4d3d4f-0e91-41eb-a512-77508f984a1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}