//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
	vec4 accumulator = vec4(0.0, 0.0, 0.0, 0.0);
	float count = 0.0;
	for(float yy = -0.01; yy <= 0.01; yy += 0.001)
	{
		vec4 sample = v_vColour * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x, max(0.0,min(1.0,v_vTexcoord.y+yy))) );
		accumulator += sample;
		++count;
	}
	
	gl_FragColor = accumulator / count;
	gl_FragColor.a = texture2D( gm_BaseTexture, v_vTexcoord ).a;
}
