{
    "id": "fd8c612e-4583-45d7-8d60-32e0513d9b93",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DebugController",
    "eventList": [
        {
            "id": "e50a9b4e-7408-47d1-baad-10c6eaec3745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "488c26fe-8514-4a0f-89b6-5c3ee3c54420",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 9,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "59107969-c4b2-4cd4-ae33-3ee90bbb8bbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "f3804911-9ef6-4f58-b77a-311ddf65a70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "af4f1f1c-c897-4b56-81f1-ce96f3d67af8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 112,
            "eventtype": 9,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "1db7d15b-04b4-4475-b568-4b344688b358",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "bcbb7fdf-b3c6-4079-a72f-e3f94e469208",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "74c419b7-0c67-4856-925e-559cd67b87a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 113,
            "eventtype": 9,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        },
        {
            "id": "14e40f78-9343-40d2-bae4-b75824b9fcdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "fd8c612e-4583-45d7-8d60-32e0513d9b93"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9da16325-a037-4a2b-a082-45d497f04bcb",
    "visible": true
}