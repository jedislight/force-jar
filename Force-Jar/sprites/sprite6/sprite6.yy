{
    "id": "818a0f22-fc15-447b-adb6-1977adddf408",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 31,
    "bbox_right": 32,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17e70a5c-92b3-49bb-bf2f-5fd123b13c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "818a0f22-fc15-447b-adb6-1977adddf408",
            "compositeImage": {
                "id": "65cc9c0a-d970-454b-a1bd-b7d96691e642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e70a5c-92b3-49bb-bf2f-5fd123b13c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7e9fc1-2ec6-4887-b66e-437e7d35da60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e70a5c-92b3-49bb-bf2f-5fd123b13c4c",
                    "LayerId": "8e7b39e0-fc2a-4efe-a843-05e8fd16d5b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8e7b39e0-fc2a-4efe-a843-05e8fd16d5b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "818a0f22-fc15-447b-adb6-1977adddf408",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}