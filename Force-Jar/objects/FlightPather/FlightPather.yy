{
    "id": "6b37da5a-4b80-4c6d-a4fb-cf2d43269246",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "FlightPather",
    "eventList": [
        {
            "id": "8891b53b-982c-4441-9dda-b59a57f54528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b37da5a-4b80-4c6d-a4fb-cf2d43269246"
        },
        {
            "id": "728ff5b3-2b56-4199-8990-88671e7361e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "6b37da5a-4b80-4c6d-a4fb-cf2d43269246"
        },
        {
            "id": "70821fc4-5ab8-49e4-b4ec-17c8dae53401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6b37da5a-4b80-4c6d-a4fb-cf2d43269246"
        },
        {
            "id": "01498aae-12ec-417b-a2fd-153dfb2d837a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6b37da5a-4b80-4c6d-a4fb-cf2d43269246"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}