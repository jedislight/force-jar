var weapon_data = argument0;
var attack_pressed = argument2;
var attack_held = argument1;
var attack_released = argument3;
var facing = argument4;

if(attack_pressed and instance_number(LevitationBurst) == 0)
{
	var caster = weapon_data.owner;
	instance_create_depth(caster.x+weapon_data.xoffset, caster.y+weapon_data.yoffset, caster.depth, LevitationBurst);
}