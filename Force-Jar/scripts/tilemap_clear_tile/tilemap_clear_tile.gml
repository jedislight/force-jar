/// @function tilemap_clear_tile(target_ytile_offset)
/// @desc Clears a tile on the map on the Tiles layer.
/// @param {Real} target_ytile_offset the offset position to clear a tile in grid coordinates.

var target_ytile_offset = argument0;

var tilemap_id = layer_tilemap_get_id(layer_get_id(LR_TILES));

assert(tilemap_id != noone, "No tilemap found");
assert_exit

var gx = tilemap_get_cell_x_at_pixel(tilemap_id, x, y);
var gy = tilemap_get_cell_y_at_pixel(tilemap_id, x, y) + target_ytile_offset;

var tiledata = tilemap_get(tilemap_id, gx, gy);

tiledata = tile_set_empty(tiledata);

tilemap_set(tilemap_id, tiledata, gx, gy);