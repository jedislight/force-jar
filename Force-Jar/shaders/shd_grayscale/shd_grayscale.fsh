//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
	vec4 sampled = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	float average = (sampled.r + sampled.g + sampled.b) / 3.0;
	vec4 grayscale = vec4(average, average, average, sampled.a);
    gl_FragColor = grayscale;
}
