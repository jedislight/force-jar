{
    "id": "dc23030b-a1d6-45d4-954d-04f49ffac3e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MapInfoPickup",
    "eventList": [
        {
            "id": "4c565529-1d42-461d-9566-c8796e0956aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc23030b-a1d6-45d4-954d-04f49ffac3e2"
        },
        {
            "id": "99b2d9bf-1320-4997-aadb-49e44aa24027",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "380edad1-487c-4192-bac0-65401ddf29b8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dc23030b-a1d6-45d4-954d-04f49ffac3e2"
        },
        {
            "id": "8730cb63-a0ba-469f-98db-8860bcf255bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "dc23030b-a1d6-45d4-954d-04f49ffac3e2"
        },
        {
            "id": "c69a52a2-c53f-45bb-aa11-e9f586994a0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc23030b-a1d6-45d4-954d-04f49ffac3e2"
        },
        {
            "id": "f1ef1dfd-0a5e-4237-a842-354f4b66d42d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "dc23030b-a1d6-45d4-954d-04f49ffac3e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "7b2e7559-9a18-4453-ad6f-f4c6b6364070",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"unset_map_name\"",
            "varName": "map_name",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "08964883-f522-42a9-9bbe-6ead8275de49",
    "visible": true
}