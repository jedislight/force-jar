/// @description Sweep in

if (ds_list_size(sweep_objects) == 0) 
{
	ds_list_destroy(sweep_objects);
	event_user(0);
	return;
}

for (var curr_inst = ds_list_size(sweep_objects) - 1; curr_inst > -1; --curr_inst)
{
	var curr_sweep_speed = sweep_speed;
	var curr_inst_id = ds_list_find_value(sweep_objects, curr_inst);
	with (curr_inst_id)
	{
		var sweep_distance = point_distance(x, y, xstart, ystart);
		
		if (sweep_distance < curr_sweep_speed)
		{
			curr_sweep_speed = sweep_distance;	
		}
		
		if (speed > curr_sweep_speed)
		{
			speed = curr_sweep_speed;
		}
		
		if (sweep_distance < 5)
		{
			x = xstart;
			y = ystart;
			speed = 0;
		}
	}
	
	if (point_distance(curr_inst_id.x, curr_inst_id.y, curr_inst_id.xstart, curr_inst_id.ystart) == 0)
	{
		ds_list_delete(sweep_objects, curr_inst);
	}
}