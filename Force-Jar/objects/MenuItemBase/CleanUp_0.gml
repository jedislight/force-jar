/// @description Remove menu item from active

while (not is_undefined(ds_map_find_value(global.active_menu_items, menu_key)))
{
	ds_map_delete(global.active_menu_items, menu_key);
	show_debug_message("Removing menu_key " + menu_key + " from active menu items");
}