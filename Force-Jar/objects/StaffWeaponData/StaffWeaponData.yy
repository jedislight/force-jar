{
    "id": "cfe40b55-7bf3-4c15-b156-14883df08252",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "StaffWeaponData",
    "eventList": [
        {
            "id": "f87b7a8b-e6c2-453f-b0a8-def4af13b17c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cfe40b55-7bf3-4c15-b156-14883df08252"
        },
        {
            "id": "247c3fd0-2f2f-49b2-84ce-c9d24efe596f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cfe40b55-7bf3-4c15-b156-14883df08252"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4485d142-534f-4381-8f6a-4017689b5e86",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6187398b-462f-412e-84e4-7b500f621f67",
            "propertyId": "9fb5d4cf-6c99-4d37-92f3-b8ba0c2e7774",
            "value": "StaffAttack"
        },
        {
            "id": "88bdf9a8-a375-4766-a4ac-d77dd2a89c83",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6187398b-462f-412e-84e4-7b500f621f67",
            "propertyId": "8d8e758a-cdfa-41ef-b0ca-27d0a13a833a",
            "value": "single_attack_on_press"
        }
    ],
    "parentObjectId": "6187398b-462f-412e-84e4-7b500f621f67",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b149eb55-8a76-48a6-84d7-8c3620ec5cb3",
    "visible": false
}