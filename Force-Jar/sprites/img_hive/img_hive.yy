{
    "id": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_hive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 6,
    "bbox_right": 118,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "892dde01-e2d7-472d-be9b-eb3616536293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "b42bb42f-6699-409b-b14f-8231a3c1f6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "892dde01-e2d7-472d-be9b-eb3616536293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd73a7c-b8df-4051-a23e-006086a1db04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "892dde01-e2d7-472d-be9b-eb3616536293",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "850a004d-8aac-45af-9bee-b2fe3c62100d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "4de57d59-383e-4b99-a3a8-2d26fcb537f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850a004d-8aac-45af-9bee-b2fe3c62100d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ed61d1-f641-4260-b003-372964734e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850a004d-8aac-45af-9bee-b2fe3c62100d",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "240581ad-e15b-4b46-bca0-198dd5bb1476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "d4cc8cd2-91d8-4bb0-8b7d-b3d4f8a67bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240581ad-e15b-4b46-bca0-198dd5bb1476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cce1922-134c-4574-be43-4634b6313929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240581ad-e15b-4b46-bca0-198dd5bb1476",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "16e793b8-d46f-43e0-9e5e-d1bc9c0118cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "f49a1899-b1d1-401a-940a-032d7f2df7b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16e793b8-d46f-43e0-9e5e-d1bc9c0118cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee9c9eaa-6464-4172-9b20-cf93b2fd229b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16e793b8-d46f-43e0-9e5e-d1bc9c0118cb",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "0884a708-7438-47a7-95f7-345139d1a769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "4536af88-c32a-4334-8134-cd32ab9ea1d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0884a708-7438-47a7-95f7-345139d1a769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00fbaba-374c-4dce-8e0d-f41d8c3a5061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0884a708-7438-47a7-95f7-345139d1a769",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "15f8b1e6-664b-4e2e-9821-66844dd2d270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "5cb2d29a-3f16-4fb7-add6-d103d42ce64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f8b1e6-664b-4e2e-9821-66844dd2d270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199ba7b6-86ae-4dda-ac8e-bf7f7336217b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f8b1e6-664b-4e2e-9821-66844dd2d270",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "8ca764a7-609a-483f-a428-a801c418a40b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "81be1171-10ed-4e67-8f78-77baaccc23bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca764a7-609a-483f-a428-a801c418a40b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4208539c-fd91-404a-a079-49b75a3fc9d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca764a7-609a-483f-a428-a801c418a40b",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        },
        {
            "id": "0706b415-0d0f-41ef-81ee-1d8698504c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "compositeImage": {
                "id": "44b6c36e-48e9-4c70-a650-2ccc6e96328e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0706b415-0d0f-41ef-81ee-1d8698504c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d739f6e-46ef-4781-b3d9-d60e77577135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0706b415-0d0f-41ef-81ee-1d8698504c48",
                    "LayerId": "4f0816a5-165d-4b05-99bf-456c1f85d475"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4f0816a5-165d-4b05-99bf-456c1f85d475",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20d9c960-dc95-414b-a334-b4a57d4bb0bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}