/// @description Prototype for Player Heath

if (owner == noone)
{
	owner = instance_find(PlayerStates, 0);
	exit;
}

if (not instance_has_health(owner))
{
	exit;	
}

if (not font_exists(ui_font))
{
	exit;
}

draw_set_color(c_yellow);
draw_set_font(ui_font);

var segments_count = floor(owner.health_max / 99) - 1;
var segments_filled = ceil(owner.health_current / 99) - 1;

draw_text(x, y, "HP: " + string(owner.health_current % 100));
var base_x = string_width("HP: 99") + 5;

for (var segment_index = 0; segment_index < segments_count; ++segment_index)
{
	var segment_color = c_gray;
	if (segment_index < segments_filled)
	{
		var segment_color = c_lime;
	}
	
	// If we get to this point we need to change how this displays.
	if (floor(segment_row_max / 4) >= segment_column_max)
	{
		break;	
	}
	
	draw_sprite_ext(img_health_pack, 0, x + base_x + segment_x_offset * (segment_index % segment_row_max), y - 5 + segment_y_offset * floor(segment_index / segment_row_max), 1, 1, 0, segment_color, 1);
}