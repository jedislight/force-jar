{
    "id": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DynamicPickup",
    "eventList": [
        {
            "id": "700f5991-8c06-4d10-a6d5-185713505d22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431"
        },
        {
            "id": "8c1c1d88-b6df-4f95-bc54-704628b1f5ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431"
        },
        {
            "id": "fb2986c1-3ff8-4495-bddf-c98f9a3dffb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431"
        },
        {
            "id": "fc02b5cc-70a0-4585-b9a1-ca750896ab9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cdfc828-6ef7-4a37-87c8-51e1f13bc431"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}