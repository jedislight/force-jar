{
    "id": "2dce82a6-5239-4396-9411-de5b0be9fef4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "img_energy_wave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 192,
    "bbox_left": 0,
    "bbox_right": 192,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6aec70ce-6d6d-492b-8165-7f059f090a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "b2dc40ff-0fb5-45ad-bd6b-10cb13ca6be1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aec70ce-6d6d-492b-8165-7f059f090a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d11b9d-4525-4788-ab11-6aeabc9db386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aec70ce-6d6d-492b-8165-7f059f090a21",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "4792834e-93b2-4d16-9f4a-a6c5d3a0a3c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "9860092a-4805-48be-af7a-9e6bac977fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4792834e-93b2-4d16-9f4a-a6c5d3a0a3c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f474c794-f7af-4d63-bf07-264a277f70cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4792834e-93b2-4d16-9f4a-a6c5d3a0a3c3",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "db6a42df-1fa5-450b-a223-e642441aefe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "af6d910c-1e5f-43dc-a033-11efac186616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6a42df-1fa5-450b-a223-e642441aefe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f3a8718-7df6-4a8d-af56-36f022741f9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6a42df-1fa5-450b-a223-e642441aefe1",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "edaba0ad-7809-41d2-9aff-e4a2d2cf56de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "4c93ef39-36d6-428e-9c41-41c5435e3ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edaba0ad-7809-41d2-9aff-e4a2d2cf56de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445ea794-66c3-4eea-8634-9676799b3d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edaba0ad-7809-41d2-9aff-e4a2d2cf56de",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "d0420b05-b9c4-4ba3-b90f-deb8b3fa2a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "f2aa9c09-769a-4f1b-8dd3-f7e4b27bce34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0420b05-b9c4-4ba3-b90f-deb8b3fa2a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdbef50e-cf7d-49c3-9045-26ed6bebb5b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0420b05-b9c4-4ba3-b90f-deb8b3fa2a6f",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "84845afb-a773-46b8-8f2f-39d1af631c99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "a15f4425-bea0-422d-b957-9f364936ac5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84845afb-a773-46b8-8f2f-39d1af631c99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6313d6af-6c07-49f1-9323-3b2cc9d77d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84845afb-a773-46b8-8f2f-39d1af631c99",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "2a60cb76-eb2f-4425-8fb1-3162e76163b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "ca05f97a-cdc2-4fe3-95ef-87097e1965ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a60cb76-eb2f-4425-8fb1-3162e76163b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c52ba011-1932-4688-b391-531eb37d8136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a60cb76-eb2f-4425-8fb1-3162e76163b0",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "ca27a1c9-b761-4363-8224-4a85f062f294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "ab3561c8-af37-4f6f-9e0a-bc0588955eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca27a1c9-b761-4363-8224-4a85f062f294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1258de45-8d2b-495a-b8ee-3001d208b892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca27a1c9-b761-4363-8224-4a85f062f294",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        },
        {
            "id": "82724a12-ea35-47d2-80ca-b677b8cb3940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "compositeImage": {
                "id": "4d96f0c3-0d81-4f74-aead-dbd62156c12a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82724a12-ea35-47d2-80ca-b677b8cb3940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee606dcd-ae56-4ba6-ba43-e692e938b0ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82724a12-ea35-47d2-80ca-b677b8cb3940",
                    "LayerId": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "cbbc31fb-a4a7-4898-bce5-d49bd66892a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dce82a6-5239-4396-9411-de5b0be9fef4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}