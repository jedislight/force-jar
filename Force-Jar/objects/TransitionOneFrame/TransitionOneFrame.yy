{
    "id": "53cbfcc1-5f6f-4b7a-9d44-c420acc631d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TransitionOneFrame",
    "eventList": [
        {
            "id": "2ac9d37d-e18a-477f-80b1-cc5793a82ad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "53cbfcc1-5f6f-4b7a-9d44-c420acc631d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dd1b089e-db02-48ac-9809-3db0559cd464",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}