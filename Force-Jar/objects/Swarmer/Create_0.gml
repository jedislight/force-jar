/// @description Insert description here
// You can write your code in this editor

event_inherited()
velocity = 32;
heavy = false;
frame = 0;

frames_since_last_turn = 1000;
minimum_frame_to_turn = 50;
path = path_add();
target = noone

if(!place_free(x,y))
{
	instance_destroy();	
}

spawn_x = x;
spawn_y = y;
draw_target_in_debug = false;