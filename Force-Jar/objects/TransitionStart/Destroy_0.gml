/// @description Base (Destroy DS)

if (transition_done)
{
	show_debug_message("Destroying transition");
	
	// NOTE: If you override this function on child object definitions, you must call event_inherited() before the end
	// or you will create a memory leak.
	ds_queue_destroy(transition_queue);

	// It is important that any lists added to transition_data that is intended to live only for the transition
	// be added via ds_map_add_list().
	// This causes the list to be destroyed when the map is destroyed.
	// If you don't intend for this to be the case, then add the list key via ds_map_add().
	ds_map_destroy(transition_data);
}